# Frutibouille

Chaques joueur possède une frutibouille le représentant.

- A la création de son frutiz le joueur peut choisir une famille et une couleur de peau.
  Il sera couvert d'une coiffe de profane.

- Au niveau 2 : Il pourra choisir une coupe de cheveux; de yeux et enlever sa coiffe.

- Au niveau 3 : Le joueur aura acces aux opérations de chirurgies esthetiques et pourra modifier sa frutibouille en payant une somme fixe en kikouz.

- Au niveau 4 : Le joueur aura acces a l'achat des incarnations.


## Familles

Chaques famille est contenu sur un swf différent.
Il existe 3 grands types de famille

- **Les Familles Principales : Frutibouilles basiques**
  Ce sont les familles que possederont tous les frutizs au départ.

- **Familles Secondaires : incarnations**
  Ce sont les familles payantes. Lorsqu'une incarnations et ses caractères sont choisi, l'incarnation devient unique et aucun joueur ne peut choisir une frutibouille de code similaire.

- **Familles Tertiaires :**
  Ce sont les familles qui serviront pour les "PNJ"

## Accessoires

Plusieurs accessoires sont vendus pour les frutibouilles basiques. Les joueurs peuvent en acheter autant qu'ils veulent mais ne peuvent en activer plus d'un en meme temps.
Ces accessoires ne fonctionneront pas sur les incarnations.

## Codage

Chaque frutibouille sera représentée par une string de 18 octets

```
ELEMENT :           NB d'octets
Famille             ( 2 )
Caractere 0         ( 2 )
Caractere 1         ( 2 )
Caractere 2         ( 2 )
Caractere 3         ( 2 )
Caractere 4         ( 2 )
Caractere 5         ( 2 )
Caractere 6         ( 2 )
Caractere 7         ( 2 )
```

Chacun de ses caracteres représentera un élément de la frutibouille ou une sous categorie de cet élément. Pour les frutibouilles basiques les caracteres sont toujours répartis de la façon suivante :

```
ELEMENT :           NB d'octets
Caractere 0         type d'yeux
Caractere 1         couleur de pupille
Caractere 2         type de cheveux
Caractere 3         sous categorie de cheveux
Caractere 4         type de bouche
Caractere 5         couleur du visage
Caractere 6         couleur 2
Caractere 7         couleur 3
```

## Expressions

Les frutiz peuvent donner a leur frutibouille une expression particulière et la changer a tout moment. Ces expressions sont caractérisées par les yeux et la bouche.
C'est une combinaison de ces deux éléments qui créera une expression.

Yeux :
0 - normaux
1 - mécontent
2 - triste
3 - content
4 - exorbités

Bouche :
0 - normale
1 - triste
2 - super Triste
3 - sourire
4 - grand Sourire
5 - tordu

## Actions

Les frutiz peuvent egalement lancer une animation sur leur frutibouille.
La plus courante est l'anim de dialogue mais il existe d'autres animations.

0 - parle
1 - Rigole un peu
2 - Rigole bcp
3 - Tire la langue

## Fonctions

Les fonctions suivantes seront présentes dans la frutibouille et permettront de la manipuler :

- **apply ( String )** :
  Permet d'appliquer une frutibouille grace a une string de 16 caracteres.

- **emote (  )** :
  Permet d'appliquer une expression fixe sur la frutibouille. Cette expression deviendra l'expression par defaut de la frutibouille.
  Le tableau des expression est disponible dans la section expressions.

- **anim( id,  time)**
  lance l'anim correspondante a l'id pendant une durée time
