/*
	$Id: GameDisc.as,v 1.6 2003/11/20 10:18:47 fnicaise Exp $
*/

import frusion.util.FrusionParams;

/*
	Class: GameDisc
	The GameDisc class stores game disc information
*/
class frusion.gamedisc.GameDisc
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = true;


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var discType : Number;		// disc_type 0=> black, 1=> white, etc...
	public var swfName : String;		
	public var swfId : String;			// swfid represntes the swf name of the game
	public var gameId : String;			// >> !!! the FD id		
	public var width : Number;			// width of the game to load
	public var height : Number;			// height of the game to load
	public var mode : String;			// open mode i=>internal e=> external
	public var files : Object;			// object containing the file information
	public var playMode : String;		// single, multi player 
	public var size : Number;			// game size
	
	 
/*------------------------------------------------------------------------------------
 * Cleanup methods
 *------------------------------------------------------------------------------------*/


    /*
        Function : finalize
        final cleanup
    */
    public function finalize() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.gamedisc::finalize" ) : undefined;

        delete this.swfName;
		this.swfName = null;
        
        delete this.swfId;
		this.swfName = null;
        
        delete this.gameId;
		this.gameId = null;
        
        delete this.mode;
		this.mode = null;
        
        delete this.files;
		this.files = null;

        delete this.playMode;
		this.files = null;
   }

    
/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: GameDisc
		Constructor
		
		Parameters:
		- discType : Number - represents the type of disc ( black (=0), white(=1), etc...)
		- gameName : String - name of the game
		- id : String - game id represented by an MD5 string
		- width : Number - width of the game to load
		- height : Number - height of the game to load
		- mode : String - open mode, internal (="i") for games loaded within frutiparc, external for loading in popup or external frames
	*/
	public function GameDisc( discType : Number, 
                                swfName : String, 
                                gameId : String, 
                                width : Number, 
                                height : Number, 
                                mode : String, 
                                files : Object, 
                                playMode : String, 
                                size : Number, 
                                swfId : String)
	{
        this.DEBUG ? _global.debug("frusion.gamedisc.GameDisc::GameDisc"  ) : undefined;
        
		this.discType = discType;
		this.swfName = swfName;
		this.swfId = swfId;
		this.gameId = gameId;
		this.width = width;
		this.height = height;
		this.mode = mode;
		this.files = files;
		this.playMode = playMode;
		this.size = size; 
	}


    /**
     *  Take encoded info and parse them
     */
    public static function parse( info : String ) : frusion.gamedisc.GameDisc
    {
        var mess : String = frusion.util.FrusionParams.decode( info );
        var params : Array = mess.split(":");
        
        /*
            discType : Number, 
            swfName : String, 
            gameId : String, 
            width : Number, 
            height : Number, 
            mode : String, 
            files : Object, 
            playMode : String, 
            size : Number, 
            swfId : String
        */

        return new frusion.gamedisc.GameDisc( params[0], params[1], params[2], parseInt( params[6], 10), parseInt(params[7], 10), "e", null, params[11],
parseInt( params[6], 10), params[2] );        
    }
    
}   // EOF

