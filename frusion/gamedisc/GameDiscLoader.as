/*
	$Id: GameDiscLoader.as,v 1.8 2003/11/20 10:18:47 fnicaise Exp $
*/


import frusion.gamedisc.GameDisc;
import frusion.gamedisc.GameFile;
import ext.util.Callback;
import frusion.Context;

/*
	Class: GameDiscLoader
*/
class frusion.gamedisc.GameDiscLoader
{


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var callback : Callback;
	public var errorCallback : Callback;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: GameDiscLoader
		Constructor
	*/
	public function GameDiscLoader() {}



	/*
		Function: loadGameDisc
		Loads a game disc. Gets its information from HTTP Server

		Parameters:
		- id: Number - game uid
		- callback: Callback - callback function to call when loeading is finished
	*/
	public function loadGameDisc( id : String, callback : Callback, errorCallback : Callback, sid: Number ) : Void
	{
		this.callback = callback;
		this.errorCallback = errorCallback;

		// ask for GameDisc information
		var loader = new HTTP(Context.DO_LD_PHP_SCRIPT, {u:id, sid:sid}, {type: Context.XML, obj: this, method:Context.ONLOADGAMEDISC} );
	}



/*------------------------------------------------------------------------------------
 * Private methodsn.attributes.k
 *------------------------------------------------------------------------------------*/


	/*
		Function: onLoadGameDisc
		CALLBACK method called once the gamedisc is loaded;
	*/
	private function onLoadGameDisc( success, node ) : Void
	{	
        // parse XML information
		var n = node.lastChild;

        ///*        
        _global.debug( "n="  + FEString.unHTML(n) );
        _global.debug( "n.attributes.k="  + n.attributes.k);
        _global.debug( "success" + success );
        //*/
		if( n.attributes.k != undefined || !success )
        {
            //_global.debug( "Problem while getting info" + success );
            this.errorCallback.execute(); 
            return;
        }

		// get disc properties : height, width and open mode
		var prop : Object = FEString.propParse(n.attributes.p);
		var discType : Number = parseInt( n.attributes.t, 10 );
		var swfName = n.attributes.n;
		var discId = n.attributes.u;
		var playMode = n.attributes.pm;		
		
		// getting swf size and id
		var id : String;
		var size : Number;
		var files : Object;
		files = new Object() ;
        var swfIndexName : String;
		for(var x=n.firstChild;x.nodeType>0;x=x.nextSibling)
		{
			if( x.nodeName != Context.S_NODE ) break;        

			if( x.attributes.n == undefined )
			{
				files[Context.INDEX]= new GameFile( x.attributes.u, x.attributes.s );
                //_global.debug("retrieved file 1 :" + name + " - " + x.attributes.u + " - " + x.attributes.s);
                swfIndexName = x.attributes.u;
			}
			else
			{
				// Code de remplacement (temporaire !!) du "." par "_"
				var name = x.attributes.n ;
				if ( name.indexOf(".") ) name = FEString.replace( name, ".", "_", 0 );
                //_global.debug("retrieved file 2 :" + name + " - " + x.attributes.u + " - " + x.attributes.s);
				files[name]= new GameFile( x.attributes.u, x.attributes.s );
			}
		}
		// create GameDisc and execute callback
		this.callback.execute( new GameDisc( discType, swfName, discId, prop.w, prop.h, prop.m, files, playMode, size, swfIndexName ) );
	}

}
