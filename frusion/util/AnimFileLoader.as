/*
	$Id: AnimFileLoader.as,v 1.1 2003/11/13 16:04:32 fnicaise Exp $
*/

/*
	Class: frusion.util.AnimFileLoader
*/
class frusion.util.AnimFileLoader extends FileLoader
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = false;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function : AnimFileLoader
		Constructor
		
		Parameters: 
			- url : animation file url
			- size: file size in bytes
	*/
	public function AnimFileLoader( url : String, size : Number )
	{
		super( url, size );
        
        this.DEBUG ? _global.debug("frusion.util.AnimFileLoader::AnimFileLoader" ) : undefined;
	}	
	

	/*
		Function : onLoadComplete
		Callback after file completion. Broadcast a new event to be listened to by client
	*/
	public function onLoadComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.util.AnimFileLoader::onLoadComplete" ) : undefined;
        
		if(this.size == undefined || this.size == this.bytesLoaded)
		{
			this.loaded = true;
			this.broadcastMessage("onAnimLoadComplete");
		}
		else
		{
			this.broadcastMessage("onAnimLoadError");
		}			

	}	
    

    /*
        Function : onLoadError
        An error occured during animation loading, broadcast to client
    */
    public function onLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.AnimFileLoader::onLoadError" ) : undefined;
        
		this.broadcastMessage("onAnimLoadError");
    }
	
}
