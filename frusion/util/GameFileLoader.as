/*
	$Id: GameFileLoader.as,v 1.3 2003/11/17 01:14:28 fnicaise Exp $
*/


/*
	Class: frusion.util.GameFileLoader
*/
class frusion.util.GameFileLoader extends FileLoader
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = false;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


    
	/*
		Function : GameFileLoader
		Constructor

		Parameters: 
			- url : animation file url
			- size: file size in bytes
	*/
	public function GameFileLoader( url : String, size : Number)
	{
		super( url, size );
        
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::GameFileLoader" ) : undefined;
	}
	

	/*
		Function : onLoadComplete
		Callback after file completion. Broadcast a new event to be listened to by client
	*/
	public function onLoadComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadComplete" ) : undefined;
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadComplete : this.mc.game._currentframe=" + this.mc._currentframe) : undefined;
		if(this.size == undefined || this.size == this.bytesLoaded)
		{
			this.loaded = true;
			this.broadcastMessage("onGameLoadComplete");
		}
		else
		{
			this.broadcastMessage("onGameFileNotFoundError");
		}			
	}	
	
	
	/*
		Function: onLoadStart
		CALLBACK. called when loading starts
	*/
	public function onLoadStart() : Void
	{
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadStart" ) : undefined;
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadStart : this.mc.game._currentframe=" + this.mc._currentframe) : undefined;
		this.broadcastMessage("onGameLoadStart");
	}
	
	
	/*
	 	Function: onLoadProgress
	 	CALLBACK. called during loading progress.
	 */
	public function onLoadProgress( mc,loadedBytes, totalBytes) : Void
	{
		// Update loading percent
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadProgress" ) : undefined;
		this.percent = Math.round( loadedBytes * 10000 / totalBytes ) / 100;						
		this.broadcastMessage( "onGameLoadProgress" );
	}
	

    /*
        Function : onLoadError
        An error occured during animation loading, broadcast to client
    */
    public function onLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadError" ) : undefined;
		this.broadcastMessage("onGameFileNotFoundError");
    }


    public function onFileNotFoundError() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onFileNotFoundError" ) : undefined;
		this.broadcastMessage("onGameFileNotFoundError");
    }


    public function onFalseSizeError() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onFalseSizeError" ) : undefined;
		this.broadcastMessage("onGameFalseSizeError");
    }


    public function onLoadInit() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadInit" ) : undefined;
        this.DEBUG ? _global.debug("frusion.util.GameFileLoader::onLoadInit : this.mc.game._currentframe=" + this.mc._currentframe) : undefined;
		this.broadcastMessage("onGameLoadInit");
    }
   
   
}   // EOF

