/*
	$Id: SkinFileLoader.as,v 1.1 2004/08/19 14:19:56 fnicaise Exp $
*/

/*
	Class: frusion.util.SkinFileLoader
*/
class frusion.util.SkinFileLoader extends FileLoader
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = false;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function : SkinFileLoader
		Constructor
		
		Parameters: 
			- url : animation file url
			- size: file size in bytes
	*/
	public function SkinFileLoader( url : String, size : Number )
	{
		super( url, size );
        
        this.DEBUG ? _global.debug("frusion.util.SkinFileLoader::SkinFileLoader" ) : undefined;
	}	
	

	/*
		Function : onLoadComplete
		Callback after file completion. Broadcast a new event to be listened to by client
	*/
	public function onLoadComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.util.SkinFileLoader::onLoadComplete" ) : undefined;
        
		if(this.size == undefined || this.size == this.bytesLoaded)
		{
			this.loaded = true;
			this.broadcastMessage("onSkinLoadComplete");
		}
		else
		{
			this.broadcastMessage("onSkinLoadError");
		}			

	}	
    

    /*
        Function : onLoadError
        An error occured during animation loading, broadcast to client
    */
    public function onLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.SkinFileLoader::onLoadError" ) : undefined;
        
		this.broadcastMessage("onSkinLoadError");
    }
	
}
