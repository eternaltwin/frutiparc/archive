//
//  $id$
//


class frusion.util.FrusionParams
{

    public static function encode( mess ) : String
    {
        return mess.toString( 36 );
    }

    public static function decode( mess ) : String
    {
        return mess.parseString( mess, 36 );
    }
}

// EOF
