/*
	$Id: FrutiConnectFileLoader.as,v 1.1 2003/11/13 16:04:32 fnicaise Exp $
*/

/*
	Class: frusion.util.FrutiConnectFileLoader
*/
class frusion.util.FrutiConnectFileLoader extends FileLoader
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = true;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function : FrutiConnectFileLoader
		Constructor
		
		Parameters: 
			- url : FrutiConnectation file url
			- size: file size in bytes
	*/
	public function FrutiConnectFileLoader( url : String, size : Number )
	{
		super( url, size );
        this.DEBUG ? _global.debug("frusion.util.FrutiConnectFileLoader::FrutiConnectFileLoader" ) : undefined;
	}	
	

	/*
		Function : onLoadComplete
		Callback after file completion. Broadcast a new event to be listened to by client
	*/
	public function onLoadComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.util.FrutiConnectFileLoader::onLoadComplete" ) : undefined;
		
		if(this.size == undefined || this.size == this.bytesLoaded)
		{
			this.loaded = true;
			this.broadcastMessage("onFrutiConnectLoadComplete");
		}
		else
		{
			this.broadcastMessage("onFrutiConnectLoadError");
		}
	}	

    public function onLoadInit() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.FrutiConnectFileLoader::onLoadInit" ) : undefined;
        this.broadcastMessage( "onFrutiConnectLoadInit" );    
    } 

    public function onLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.util.FrutiConnectFileLoader::onLoadError" ) : undefined;
        this.broadcastMessage( "onFrutiConnectLoadError" );    
    } 

    
}   // EOF

