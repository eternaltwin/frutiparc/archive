import ext.util.Callback;
import frusion.Context;

/**
 * The frusion.security.Citizen class make authorization requests to a security HQ
 */
class frusion.security.Citizen extends LocalConnection
{

    public static var ADDRESS : String = "_citizen";
    private static var GETPERMISSION : String = "getPermission";

    private var _permissionGrantedCallback : Callback;
    private var _permissionDeniedCallback : Callback;
    public var _greencard : Object;
    public var _id : String;


    public function Citizen( permissionGrantedCallback, permissionDeniedCallback )
    {
        //_global.debug( "frusion.security.Citizen" );
        _permissionGrantedCallback = permissionGrantedCallback;
        _permissionDeniedCallback = permissionDeniedCallback;

        allowDomain( Context.BASE_DOMAIN );
        allowDomain( Context.SENDING_DOMAIN );
        connect( ADDRESS );
    }

   
    public function getPermission() : Void
    {
        _global.debug( "frusion.security.Citizen::getPermission with " + _root.tourneboule );
        var securityHQ : LocalConnection = new LocalConnection();
        securityHQ.send( frusion.security.SecurityHQ.ADDRESS, GETPERMISSION, _root.tourneboule );
    }

 
    public function permissionGranted( greencard : Object, id : String ) : Void
    {
        _greencard = greencard;
        _id = id;
        /*
        _global.debug( "frusion.security.Citizen::greenCard:" + greencard.swfName );
        _global.debug( "frusion.security.Citizen::id:" + id );
        _global.debug( "frusion.security.Citizen::onPermissionGranted" );
        */
        _permissionGrantedCallback.execute();
        close();
    } 


    public function getGreenCard() : Object { return _greencard; }
    public function getId() : Object { return _id; }


    public function permissionDenied() : Void
    {
        _global.debug( "frusion.security.Citizen::onPermissionDenied" );
        _permissionDeniedCallback.execute();
		getURL( Context.CHARTE_URL );
        close();
    }


}

// EOF
