import frusion.Context;

/**
 * The frusion.security.SecurityHQ class check every permission request
 * from citizens
 */
class frusion.security.SecurityHQ extends LocalConnection
{

    private static var PERMISSIONGRANTED : String = "permissionGranted";
    private static var PERMISSIONDENIED : String = "permissionDenied";

    public static var ADDRESS : String = "_securityhq";

    private var _validAccessCount : Number;
    private var _permissionRequests : Number;
    private var _accessGranted : Boolean;

    private var _key : Number;
    private var _validDisc : String;
    private var _isValidDisc : Boolean;
    private var _greenCard : Object;

    public function SecurityHQ()
    {
        //_global.debug( "frusion.security.SecurityHQ" );
        _permissionRequests = 0;
        _validAccessCount = 0;
        _accessGranted = true;  
        _validDisc = "";
        _isValidDisc = true; 
        _key = 0; 

        allowDomain( Context.BASE_DOMAIN );
        allowDomain( Context.SENDING_DOMAIN );
        connect( ADDRESS );
        _global.debug( "ADDRESS=" + ADDRESS );
    }


    /**
     * Init the  security Manager
     */
    public function grantAccess() : Void
    {
        //_global.debug( "frusion.security.SecurityHQ::grantAccess" );
        _accessGranted = true;
        _permissionRequests = _validAccessCount;
    }


    public function accessGranted() : Boolean
    {
        //_global.debug( "frusion.security.SecurityHQ::accessGranted" );
        return _accessGranted;
    }

    
    public function getPermission( key ) : Void
    {
        _global.debug( "frusion.security.SecurityHQ::getPermission with internal key=" + _key );
        _global.debug( "frusion.security.SecurityHQ::getPermission with external key=" + key );
        _permissionRequests++;

        if( ( _permissionRequests > _validAccessCount ) || ( key != _key ) )
        {
        
            _global.debug( "frusion.security.SecurityHQ::getPermission DENIED" );

            _accessGranted = false;
            //_global.debug( "frusion.security.SecurityHQ::PermissionDenied" );

            // restore good state
            _permissionRequests = _validAccessCount;

            // inform citizen
            var citizen : LocalConnection = new LocalConnection();
            citizen.send( frusion.security.Citizen.ADDRESS, PERMISSIONDENIED );
            //citizen.close();
        }
        else
        {
            _global.debug( "frusion.security.SecurityHQ::getPermission OK" );

            //_global.debug( "frusion.security.SecurityHQ::PermissionGranted" );
            var citizen : LocalConnection = new LocalConnection();
            citizen.send( frusion.security.Citizen.ADDRESS, PERMISSIONGRANTED, _greenCard );
            //citizen.close();
        }
    }


    public function registerValidAccess() : Void
    {
        //_global.debug( "frusion.security.SecurityHQ::registerValidAccess" );
        _validAccessCount++;
    }


    public function registerValidDiscId( id : String ): Void
    {
        //_global.debug( "frusion.security.SecurityHQ::registerValidDiscId : " + id );
        _validDisc = id;
    }

    public function registerValidKey( key : Number ): Void
    {
        //_global.debug( "frusion.security.SecurityHQ::registerValidDiscId : " + id );
        _key = key;
    }


    public function checkDiscValidity( id : String ) : Void
    {
        /*
        _global.debug( "frusion.security.SecurityHQ::checkDiscValidity client : " + id );
        _global.debug( "frusion.security.SecurityHQ::checkDiscValidity server : " + _validDisc );
        */
        if( id != _validDisc ) _isValidDisc = false;
    }

    public function gotValidDisc() : Boolean
    {
        return _isValidDisc;
    }

    public function storeGreenCard( greencard )
    {
        _greenCard = greencard;
    } 

}

// EOF
