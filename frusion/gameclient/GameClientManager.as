/*
    Class: frusion.gameclient.GameClientManager

    Topic:
    Interface entre les jeux et le GameClient

    Version:
    $Id: GameClientManager.as,v 1.30 2004/12/15 12:19:06 ncannasse Exp $
 */


import ext.util.Callback;
import ext.util.MTSerialization;
//import frusion.gameclient.GameClient;


class frusion.gameclient.GameClientManager
{

/*------------------------------------------------------------------------------------
 * STATICS
 *------------------------------------------------------------------------------------*/


    public static var BLACK : Number = 0;
    public static var GREY : Number = 1;      // britain
    public static var GRAY : Number = GREY;          // britain
    public static var WHITE : Number = 2;
    public static var RED : Number = 3;


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


    public var gclient : Object;
    public var frusionClient : frusion.client.FrusionClient ;
    public var frutiScore : frusion.service.FrutiScore ;
    public var gameDisc : frusion.gamedisc.GameDisc ;
    public var slots : Array ;
    public var canSaveScore : Boolean;
    public var noSaveScore : Boolean;
    public var pauseStatus : Boolean;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


    private var scoreToSave : Number;
    private var dataToSave : Object;
    private var listeners : Array;
    private var scoreKey : String ; // cl� de codage des scores


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


    /*
        Function: GameClientManager
        Constructeur
    */
    public function GameClientManager( gclient )
    {
        _global.debug("frusion.gameclient.GameClientManager.GameClientManager");

        // Init FrusionClient
        this.frusionClient = new frusion.client.FrusionClient();
        this.frusionClient.registerReadyCallback( new Callback(this,onServiceConnect) ) ;
        this.frusionClient.registerPauseCallback( new Callback(this,onPause)) ;
        this.frusionClient.registerCloseCallback( new Callback(this,onGameClose)) ;
        this.frusionClient.registerResetCallback( new Callback(this,onGameReset)) ;
        this.frusionClient.onXMLHook = gclient.onXml;

        // Init FrutiScore service
        this.frutiScore = new frusion.service.FrutiScore( this.frusionClient ) ;
        this.frutiScore.addListener(this);

        // Init listener system
        this.listeners = new Array();

        this.noSaveScore = false;
        this.canSaveScore = false;

        // get client
        this.gclient = gclient;
        this.gclient.forcePause = false;
    }


    /*
        Function : isColor

        Returns tru if the disc color is same as asked
    */
    public function isColor( color : Number) : Boolean
    {
        return color == this.gameDisc.discType;
    }


/*------------------------------------------------------------------------------------
 * Error Handling
 *------------------------------------------------------------------------------------*/

    /*
        Function: logError

        simply log errors in database
    */
    public function logError(code, message) {
        this.frusionClient.logError( code, message ) ;
    }


    /*
        Function: fatalError

        log errors in database and ask to close frusion and quits
    */
    public function fatalError( msg : String, msgUsr : String )
    {
        this.frusionClient.fatalError( msg, msgUsr );
        this.finalize();
    }


    /*
        Function: getFileInfos
        Renvoie les infos d'un fichier (url et poids)

        Parameters:
        fileName (String) - nom "normal" du fichier � charger (ex: intro.swf)

        Returns:
        object - contient *name* (String) et *size* (int). Name est l'url
        compl�te du fichier
    */
    public function getFileInfos( fileName : String ) : Object
    {
        var fileInfos : Object ;
        var fullName : String ;

        fileName = FEString.replace( fileName, ".", "_" ) ;
        fileName = FEString.replace( fileName, "-", "_" ) ;

        fullName = _global.swfURL + this.gameDisc.files[fileName].id ;

        fileInfos = new Object() ;
        fileInfos.name = fullName ;
        fileInfos.size = parseInt(gameDisc.files[fileName].size, 10) ;
        return fileInfos ;
    }


    public function getUser() : String
    {
        return this.frusionClient.user ;
    }

/*------------------------------------------------------------------------------------
 * FrusionClient
 *------------------------------------------------------------------------------------*/


    /*
        Function: closeService
        Fermeture du jeu
    */
    public function closeService() : Void
    {
        debug("frusion.gameclient.GameClientManager.closeService");
        this.gclient.connected = false;
        this.frusionClient.closeService() ;
    }


    /*
        Function: serviceConnect
        Connexion au service de score (voir <onServiceConnect>)
    */
    public function getService() : Void
    {
        _global.debug("frusion.gameclient.GameClientManager.getService");
        this.frusionClient.getService() ;
    }


   public function debug( mess : String )
   {
      this.frusionClient.debug(mess);
   }

/*------------------------------------------------------------------------------------
 * FrutiScore
 *------------------------------------------------------------------------------------*/


    /*
        Function: saveSlot
        S�rialisation d'un slot et stockage sur le serveur (pas de callback);
        si le param�tre *slot* est omi, c'est le slot local de l'objet
        ( this.slots[slotId] ) qui est s�rialis�.

        Parameters:
        slotId (int) - id du slot � envoyer
        slot (Object) - *facultatif*, contenu du slot � serialiser
    */
    public function saveSlot( slotId : Number, slot : Object )
    {
        debug("frusion.gameclient.GameClientManager.saveSLot");
        if ( slot == undefined || slot == null )
            this.frusionClient.frutiCard.updateSlot( slotId, this.slots[slotId] ) ;
        else
            this.frusionClient.frutiCard.updateSlot( slotId, slot ) ;
    }



    /*
        Function: startGame
        D�marre une partie (voir <onStartGame>)

        Parameters:
        gameMode (int) - mode de jeu
    */
    public function startGame() : Void
    {
        debug("frusion.gameclient.GameClientManager.startGame");

        if( this.canSaveScore )
            this.frutiScore.startGame( this.gameDisc.gameId ) ;
        else
            this.gclient.onStartGame();
    }


    /*
        Function: endGame
        Termine la partie en cours (voir <onEndGame>)
    */
    public function endGame() : Void
    {
        debug("frusion.gameclient.GameClientManager.endGame");

        this.noSaveScore = true;
        this.gclient.noSaveScore = this.noSaveScore;
        this.gclient.gameRunning = false;
        this.frutiScore.endGame() ;
    }


    public function giveItem( item : String )
    {
        debug("frusion.gameclient.GameClientManager.giveItem");

        this.frutiScore.giveItem( item );
    }

    public function giveAccessory( item : String )
    {
        this.frutiScore.giveAccessory( item );
    }


    /*
        Function: saveScore
        Sauve des scores (voir <onSaveScore>)

        Parameters:
        scoreList (number) - score
        miscData (String) - information suppl�mentaire � stocker avec
        l'enregistrement
    */
    public function saveScore( score : Number, data : Object ) : Void
    {
        if( !this.canSaveScore )
        {
            var r = new frusion.gameclient.RankingResult( score, data  );
            this.gclient.ranking = r;
            this.gclient.onSaveScoreFruticard();
            this.gclient.onSaveScore();
        }
        else
        {
            debug( "saveScore server score=" + score);
            this.scoreToSave = score;
            this.dataToSave = data;
            this.endGame();
            this.noSaveScore = false;
        }
    }


/*------------------------------------------------------------------------------------
 * FrusionClient Callbacks
 *------------------------------------------------------------------------------------*/


    /*
        Function: onServiceConnect
        Callback <serviceConnect>
    */
    public function onServiceConnect() : Void
    {
        _global.debug( "onServiceConnect------------------------>GameClientManager" );
        this.gclient.dailyData = this.frusionClient.dailyData;
        this.canSaveScore = this.frusionClient.canSaveScore;

        this.gameDisc = this.frusionClient.gameDisc ;

		if( this.gclient.connected )
			return;

        this.slots = this.frusionClient.frutiCard.slots ;

        // manage gameclient
        _global.debug( "this.frusionClient.startTime=" + this.gclient.startTime );
        this.gclient.startTime = FEDate.newFromString( FEString.trim( this.frusionClient.startTime.toString() ) );
        //this.gclient.startTime = new Date();
        _global.debug( "this.frusionClient.startTime=" + this.gclient.startTime );
        _global.debug( "isdate=" + (this.gclient.startTime instanceof Date) );


        this.gclient.connected = true;
        this.gclient.slots = this.slots;
        this.gclient.canSaveScore = this.canSaveScore;
       	this.gclient.onServiceConnect();
    }



    /*
        Function: onPause

        Callback appel� quand la Frusion demande une pause (g�n�ralement
        quand le tab frusion perd le focus); le flag local
             *pauseStatus* vaut true.
    */
    public function onPause() : Void
    {
        this.pauseStatus = frusionClient.pauseStatus;
        this.gclient.forcePause = this.pauseStatus;
        if( this.pauseStatus ) this.gclient.onPause();
    }


    /*
        Function: onGameClose
         Callback d'interruption du jeu (depuis frutiparc); le flag local
         *forceClose* vaut true.
    */
    public function onGameClose() : Void
    {
        this.gclient.forceClose = true;
        this.gclient.forcePause = true;
        this.gclient.onGameClose();
    }



    /*
        Function: onGameReset
        Callback appel� quand la Frusion demande un reset du jeu
        (actuellement se comporte comme <onGameClose>)
    */
    public function onGameReset(): Void
    {
        this.gclient.reseting = true;
        this.gclient.onGameReset();
    }



/*------------------------------------------------------------------------------------
 * FrutiScore Callbacks
 *------------------------------------------------------------------------------------*/


    /*
        Function: onStartGame
        Callback <startGame>

        Parameters:
        nodeTxt (String) - node re�ue du serveur
    */
    public function onStartGame( nodeTxt : String )
    {
        var node : XML = new XML(nodeTxt) ;
        debug( "node.firstChild.attributes.d=" + node.firstChild.attributes.d );
        debug( "node.firstChild.attributes.s=" + node.firstChild.attributes.s );

        if ( node.firstChild.attributes.k != undefined || node.firstChild.attributes.d != this.gameDisc.discType )
        {
            debug( "ERRRRRRRRRORRR" );
            this.gclient.onError();
            return;
        }
        else
            this.gclient.gameRunning = true;

        this.gclient.onStartGame();

    }

    public function onGetTime( nodeTxt : String ) : Void
    {
        _global.debug( "onGetTime ++-----+++" );
    }


    /*
        Function: onEndGame
        Callback <endGame>; la cl� de codage des scores est re�ue ici

        Parameters:
        nodeTxt (String) - node re�ue du serveur
    */
    public function onEndGame( nodeTxt : String ) : Void
    {
        debug( "saveScore server onEndGame " + this.noSaveScore );

        if( this.noSaveScore )
        {
            this.gclient.onEndGame() ;
            return;
        }

        var node : XML = new XML(nodeTxt) ;
        this.scoreKey = node.firstChild.attributes.pi ;
        this.frutiScore.saveScore( this.scoreToSave, MTSerialization.serialize(this.dataToSave), this.scoreKey,
this.gameDisc.gameId ) ;
    }


    /*
        Function: onSaveScore

        Parameters:
        nodeTxt (String) - node re�ue du serveur
    */
    public function onSaveScore( nodeTxt : String ) : Void
    {
        // XXX ajouter score et data (qui est object )
        var node : XML = new XML(nodeTxt) ;
        var sub : XMLNode ;

        sub = node.firstChild ;
        debug( "onSaveScore sub = " + sub );

        // if any error occured... bye bye !
        if( sub.attributes.k ) onGameClose();

        var r = new frusion.gameclient.RankingResult(
                    this.scoreToSave,
                    this.dataToSave,
                    parseInt( sub.attributes.op, 10 ),
                    parseInt( sub.attributes.os, 10 ),
                    parseInt( sub.attributes.p, 10 ),
                    parseInt( sub.attributes.s, 10 ),
                    parseInt( sub.attributes.r, 10 ),
                    sub.attributes.rn
                    ) ;

        this.gclient.ranking = r;
        this.gclient.onSaveScore() ;
    }


/*------------------------------------------------------------------------------------
 * Cleanup methods
 *------------------------------------------------------------------------------------*/


    /*
       u know what... AS2 sucks !

       Reapeat after me:
       never use ASBrodcaster - LocalConnection is not safe - AS2 by far most time consuming language
       never use ASBrodcaster - LocalConnection is not safe - AS2 by far most moral consuming language
       never use ASBrodcaster - LocalConnection is not safe - AS2 by far most energy consuming language

       ...oh by the way, it has nothing to do with cleanup ;)
     */
    public function finalize() : Void
    {

        delete this.slots;
        this.slots = null;

        delete this.scoreKey;
        this.scoreKey = null;

        this.frusionClient.finalize();
        delete this.frusionClient;
        this.frusionClient = null;

        this.frutiScore.finalize();
        delete this.frutiScore;
        this.frutiScore = null;

        this.gameDisc.finalize();
        delete this.gameDisc;
        this.gameDisc = null;
    }


/*------------------------------------------------------------------------------------
 * Listener implementation
 *------------------------------------------------------------------------------------*/


    /*
	public function broadcastMessage( msg )
    {
        var i;
        for( i in this.listeners )
            this.listeners[i][msg]();
    }


	public function addListener( l )
    {
        this.listeners.push( l );
    }


	public function removeListener( l )
    {
        for( var i in this.listeners )
        {
            if( this.listeners[i] == l )
            {
                this.listeners.splice( i, 1 );
                return true;
            }
       }
       return false;
    }
    */

}


