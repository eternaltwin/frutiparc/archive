/*
	Class: frusion.gameclient.RankingResult

  Topic:
	Objet de stockage des r�sultats (classement) d'une partie.

	Se r�f�rer � la documentation du service FrutiScore pour plus d'informations (
	http://hq.motion-twin.com/~lbedubourg/cbee/commands/frutiscore.html )

	Version:
	$Id: RankingResult.as,v 1.6 2004/05/25 12:03:32 fnicaise Exp $

	SECTION: PROPRI�T�S
	- oldPos (int)
	- oldScore (int)
	- bestScorePos (int)
	- bestScore (int)
	- rankingName (String)
*/

class frusion.gameclient.RankingResult
{

/** PROPRI�T�S PUBLIQUES */
  public var oldPos : Number ;
  public var oldScore : Number ;
  public var bestScorePos : Number ;
  public var bestScore : Number ;
  public var rankingName : String ;
  public var rankingData : Object; 
  public var rankingScore : Number ;


/** SECTION: M�THODES */

/*------------------------------------------------------------------------
    Function: RankingResult
    Constructeur

    Parameters:
    op (int) - ancienne position (vaut 0 si inexistante)
    os (int) - ancien score
    p (int) - meilleure position
    s (int) - meilleur score
    rn (String) - nom du ranking concern�
 ------------------------------------------------------------------------*/
  public function RankingResult( score, data, op, os, p, s, rn ) 
  {
    this.oldPos = op ;
    this.oldScore = os ;
    this.bestScorePos = p ;
    this.bestScore = s ;
    this.rankingName = rn ;
    this.rankingScore = score;
    this.rankingData = data;
  }

}

