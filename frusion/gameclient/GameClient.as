/*
    Class: frusion.gameclient.GameClient

    Topic:
    Interface entre les jeux et la frusion

    Version:
    $Id: GameClient.as,v 1.52 2004/11/18 14:01:47 ncannasse Exp $
     */


import frusion.gameclient.RankingResult;

class frusion.gameclient.GameClient
{

    public static var BLACK : Number = 0;
    public static var GREY : Number = 1;      // britain
    public static var WHITE : Number = 2;
    public static var RED : Number = 3;
    public static var VERSION : String = "0.5";


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


    public var manager : Object;

    public var forceClose : Boolean ;           // flag indiquant une fermeture du jeu
    public var forcePause : Boolean ;           // flag indiquant une pause demand�e par le serveur
    public var reseting : Boolean ;             // flag indiquant un reset de la frusion
    public var connected : Boolean ;            // flag de l'op�ration serviceConnect
    public var gameRunning : Boolean ;          // indique si un startGame a �t� lanc� sans endGame
    public var slots : Array ;
    public var ranking : RankingResult;
    public var dailyData : String;
    public var startTime : Date;
    public var canSaveScore : Boolean;
    public var noSaveScore : Boolean;
    public var onXml : Function;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/

    private var debug : Function ;

/*------------------------------------------------------------------------------------
 * Cleanup functions
 *------------------------------------------------------------------------------------*/


    /*
       u know what... AS2 sucks !

       Reapeat after me:
       never use ASBrodcaster - LocalConnection is not safe - AS2 by far most time consuming language
       never use ASBrodcaster - LocalConnection is not safe - AS2 by far most moral consuming language
       never use ASBrodcaster - LocalConnection is not safe - AS2 by far most energy consuming language

       ...oh by the way, it has nothing to do with cleanup ;)
     */
    private function finalize() : Void
    {

        delete this.slots;
        this.slots = null;

        delete this.debug;
        this.debug = null;

        delete this.dailyData;
        this.dailyData = null;

        this.manager.finalize();
        delete this.manager;
        this.manager = null;
    }


    /*
        Function: GameClient
        Constructeur
    */
    public function GameClient( )
    {
        this.slots = new Array() ;
        this.forceClose = false;
        this.forcePause = false;
        this.reseting = false;
        this.connected = false;
        this.gameRunning = false;
        this.canSaveScore = false;
        this.dailyData="";
    }


	public function sendXml( x : XML ) {
		this.manager.frusionClient.sendXml(x.toString());
	}

    /*
        Function : isWhite

        Returns true if the disc color matches
    */
    public function isWhite() : Boolean
    {
        return this.manager.isColor( WHITE );
    }

    public function getUser() : String
    {
        return this.manager.getUser();
    }

    public function getVersion() : String
    {
        return VERSION;
    }

   public function debugMessage( mess : String )
   {
      this.manager.debug( mess );
   }

    /*
        Function : isBlack

        Returns true if the disc color matches
    */
    public function isBlack() : Boolean
    {
        return this.manager.isColor( BLACK );
    }


    /*
        Function : isGrey

        Returns true if the disc color matches
    */
    public function isGrey() : Boolean
    {
        return this.manager.isColor( GREY );
    }


    public function isGray() : Boolean
    {
        return this.manager.isColor( GREY );
    }

    /*
        Function : isRed

        Returns true if the disc color matches
    */
    public function isRed() : Boolean
    {
        return this.manager.isColor( RED );
    }


/*------------------------------------------------------------------------------------
 * Error Handling
 *------------------------------------------------------------------------------------*/


    /*
        Function: setDebugFunction
        D�fini la fonction de trace utilis�e par la classe (cette fonction
        recoit 1 param�tre de type *String*)

        Parameters:
        debugFunction (Function) - l'objet function � appeler
    */
    public function setDebugFunction( debugFunction : Function ) : Void
    {
        this.debug = debugFunction ;
    }


    /*
        Function: logError

        simply log errors in database
    */
    public function logError(code, message) {
        this.manager.logError( code, message ) ;
    }


    /*
        Function: fatalError

        log errors in database and ask to close frusion and quits
    */
    public function fatalError( msg : String, msgUsr : String )
    {
        this.manager.fatalError( msg, msgUsr );
        this.finalize();
    }


    /*
        Function: getFileInfos
        Renvoie les infos d'un fichier (url et poids)

        Parameters:
        fileName (String) - nom "normal" du fichier � charger (ex: intro.swf)

        Returns:
        object - contient *name* (String) et *size* (int). Name est l'url
        compl�te du fichier
    */
    public function getFileInfos( fileName : String ) : Object
    {
        return this.manager.getFileInfos( fileName );
    }


    /*
        Function: closeService
        Fermeture du jeu
    */
    public function closeService() : Void
    {
        this.debug("closeService")
        this.manager.closeService();

        // do some cleanup
        this.finalize();
    }


    /*------------------------------------------------------------------------
      FORCE LA FERMETURE DU JEU APR�S UN D�LAI (SI LE CLIENT NE SE FERME
      PAS ALORS QU'ON LE LUI DEMANDE)
      ------------------------------------------------------------------------*/
    private function forceCloseService() : Void {
        this.debug("forceCloseService") ;

        if ( this.connected )
            this.closeService() ;
    }


    /*
        Function: serviceConnect
        Connexion au service de score (voir <onServiceConnect>)
    */
    public function serviceConnect() : Void
    {
        this.manager = new frusion.gameclient.GameClientManager( this ) ;
        this.manager.getService();
    }


    /*
        Function: saveSlot
        S�rialisation d'un slot et stockage sur le serveur (pas de callback);
        si le param�tre *slot* est omi, c'est le slot local de l'objet
        ( this.slots[slotId] ) qui est s�rialis�.

        Parameters:
        slotId (int) - id du slot � envoyer
        slot (Object) - *facultatif*, contenu du slot � serialiser
    */
    public function saveSlot( slotId : Number, slot : Object )
    {
        this.manager.saveSlot( slotId, this.slots[slotId] );
    }



    /*
        Function: startGame
        D�marre une partie (voir <onStartGame>)
    */
    public function startGame() : Void
    {
        this.manager.startGame() ;
    }

    /*
        Function: giveItem
    */
    public function giveItem( item : String ) : Void
    {
        this.manager.giveItem( item ) ;
    }

    /*
        Function: giveAccessory
    */
    public function giveAccessory( accessory : String ) : Void
    {
        this.manager.giveAccessory( accessory ) ;
    }



    /*
        Function: endGame
        Termine la partie en cours (voir <onEndGame>)
    */
    public function endGame() : Void {
        this.manager.endGame() ;
    }


    /*
        Function: saveScore
        Sauve des scores (voir <onSaveScore>)

        Parameters:
        scoreList (number) - score
        miscData (String) - information suppl�mentaire � stocker avec
        l'enregistrement
    */
    public function saveScore( score : Number, miscData : Object ) : Void
    {
        this.manager.saveScore( score, miscData );
    }


/*------------------------------------------------------------------------------------
 * Manager Callbacks
 *------------------------------------------------------------------------------------*/


    public function onServiceConnect() : Void {}

    public function onPause() : Void {}

    public function onGameClose() : Void
    {
        if( this.gameRunning ) this.endGame();
        this.closeService();
    }

    public function onGameReset(): Void
    {
        if( this.gameRunning && !this.isWhite() )
        {
            this.endGame();
            this.closeService();
        }
    }


/*------------------------------------------------------------------------------------
 * FrutiScore Callbacks
 *------------------------------------------------------------------------------------*/

    public function onGetTime() : Void {}
    public function onStartGame() : Void {}
    public function onEndGame() : Void {}
    public function onSaveScoreFruticard() : Void {}
    public function onSaveScore() : Void {}
    public function onError() : Void { onGameClose(); }

}


