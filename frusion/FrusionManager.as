/*
	$Id: FrusionManager.as,v 1.1 2003/11/13 16:04:32 fnicaise Exp $
*/


import frusion.server.FrusionServer;
import frusion.gamedisc.GameDiscLoader;
import frusion.gamedisc.GameDisc;
import ext.util.Callback;
import ext.util.Observable;
import frusion.frutiparc.PopupFrusion;
import frusion.Context;
import frusion.frutiparc.FrameFrusion;
import frusion.frutiparc.InternalFrusion;
import frusion.frutiparc.FPFrusionSlot;

/*
	Class: FrusionManager
*/
class frusion.FrusionManager extends Observable
{

/*------------------------------------------------------------------------------------
 * STATICS
 *------------------------------------------------------------------------------------*/

    private static var LAUNCHCOUNT : Number = 0;
    private static var POPUPCOUNT : Number = 0;

/*------------------------------------------------------------------------------------
 * DEBUG
 *------------------------------------------------------------------------------------*/


    private var DEBUG : Boolean = true;


/*------------------------------------------------------------------------------------
 * Private members 
 *------------------------------------------------------------------------------------*/


	private var shut : Boolean = false;
	private var frusionX : Number;
	private var frusionY : Number;
	private var sent : Number;
	private var id : String;
    private var state : String;
    private var slot : FPFrusionSlot;
    private var localConnectionOk : Boolean;
    private var _breakFrusion : Boolean;

/*------------------------------------------------------------------------------------
 * Public members 
 *------------------------------------------------------------------------------------*/


	public var frusionServer : FrusionServer;
	public var gameDisc : GameDisc;


/*------------------------------------------------------------------------------------
 * cleanup
 *------------------------------------------------------------------------------------*/


	/*
		Function: finalize
	*/
    public function finalize() : Void
    {
        this.DEBUG ? _global.debug("frusion.FrusionManager::finalize" ) : undefined;
        
        this.frusionServer.finalize();
        delete this.frusionServer;
        this.frusionServer = null;
        
        this.gameDisc.finalize();
        delete this.gameDisc;
        this.gameDisc = null;
        
        delete this.slot;
        this.slot = null;
        
        delete this.state;
        this.state = null;
    
        delete this.id;
        this.id = null;
        
        this.sent = 0;
        this.frusionX = 0;
        this.frusionY = 0;
    }

    
/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: FrusionManager
		Constructor
	*/
	public function FrusionManager() 
    {
        this.DEBUG ? _global.debug("frusion.FrusionManager::FrusionManager" ) : undefined;
        
        this.sent = 0;
        this.frusionX = 0;
        this.frusionY = 0;
        _breakFrusion = false;

    }
	
		
	/*
		Function: launchGameDisc
		Get information for a game disc and creates a new frusion slot
	*/
	public function launchDisc( id : String )
	{	
        // cheater management ;)
        if( _breakFrusion )
        {
            _destroyFrusion();
            return
        }
			 
        this.DEBUG ? _global.debug("frusion.FrusionManager::launchDisc" ) : undefined;

            
		var gdl : GameDiscLoader = new GameDiscLoader();
		gdl.loadGameDisc( id, new Callback( this, onLaunchGameDisc ), new Callback( this, stopFrusion ), _root.sid );
		
		this.id = id;		
        this.localConnectionOk = false;
	}
	
	
	/*
		Function: onLaunchGameDisc
		CALLBACK. creates a new slot when the game disc information have been retrieved by the GameDiscLoader
	*/
	public function onLaunchGameDisc( gd: GameDisc )
	{
        this.DEBUG ? _global.debug("frusion.FrusionManager::onLaunchGameDisc" ) : undefined;

        delete _global[gd.swfName];
        _global[gd.swfName] = null;
        delete _global.asml;
        _global.asml = null;
        delete _global.Std;
        _global.Std = null;

        // ["153&"] > asml
        delete _global["153&"];
        _global["153&"] = null;
        // ["3&!$"] > Std
        delete _global["3&!$"];
        _global["3&!$"] = null;


        // generate key
        var x = random( 100000 );
        var b = random( 500 );
        var y = 5 * x + b;
        _root.tourneboule = y;
        
		// test if we can load the game
        this.gameDisc = gd;
		if( gd.playMode != undefined )
		{								
            var mode : Number = checkOpenMode();
            switch( mode )
            {
                case Context.FRUSION_OPENMODE_INTERNAL:
                {
                    this.slot = new InternalFrusion( this.gameDisc, x, b, y );
                    break;
                }
                case Context.FRUSION_OPENMODE_POPUP:
                {
                    // store how many discs we've inserted
                    this.slot = new PopupFrusion( this.gameDisc, x, b, y );
                    break;
                }
                case Context.FRUSION_OPENMODE_FRAME:
                {
                    this.slot = new FrameFrusion( this.gameDisc, x, b, y );
                    break;
                }
            }

			// Getting FrusionServer				
		    this.frusionServer = FrusionServer.getInstance();
		    this.frusionServer.init( gd, mode, y );
		    this.frusionServer.addListener( this );
 
		    // register FrusionServer as observer
		    this.addObserver( this.frusionServer ); 

            // create Slot and launch game
            this.launchGame();
		}
	}
	

    private function launchGame() : Void
    {
        _global.slotList.addSlot(this.slot, true);
    }


    private function _destroyFrusion()
    {
            // transform a little
            _global.main.frusion.breakFrusion();
            //_global.main.frusion._rotation += 5;
 
            // display gentle message
            _global.openErrorAlert( "Je ne sais pas ce que tu as bien pu bricoler, " + 
                                            "mais ta frusion semble bien mal en point.<br />" + 
                                            "Je pense que le mieux c'est encore de redémarrer frutiparc.");         
    }


    /*
        In case we've found a cheater, break frusion
    */
	public function breakFrusion() : Void
    {
        _breakFrusion = true;
    }
    
	
	/*
		Function : changeFrusionState
		This method is called when a frusion state is modified from the UI (FPFrusionSlot)
	*/
	public function changeFrusionState( state: String ) : Void
	{
        this.DEBUG ? _global.debug("frusion.FrusionManager::changeFrusionState" ) : undefined;
        
        // get new state
        this.state = state;
        this.sent = 1;
        
        // notify observers
		this.setChanged();
		this.notifyObservers( Context.FRUSIONMANAGER_CLASS );
		this.clearChanged();
	}


    
	/*
		Function : updateFrusionCoordinates
		This method is called on update size from the UI (FPFrusionSlot)
	*/
    public function updateFrusionCoordinates( x : Number, y : Number ) : Void
    {
        this.DEBUG ? _global.debug("frusion.FrusionManager::updateFrusionCoordinates" ) : undefined;
        
        this.frusionX = x;
        this.frusionY = y;
        this.sent = 0;

        // notify observers
		this.setChanged();
		this.notifyObservers( Context.FRUSIONMANAGER_CLASS );
		this.clearChanged();
    }

    
    /*
        Function: getState

        Public accessor for state
    */                    
    public function getState() : String 
    {
        return this.state;
    }


    public function getX() : Number { return this.frusionX; }
    public function getY() : Number { return this.frusionY; }
    public function getSent() : Number { return this.sent; }
    

/*------------------------------------------------------------------------------------
 * Callbacks from frusionServer
 *------------------------------------------------------------------------------------*/	


	/*
	 	Function: reset

        *Called by frusion when reset is called*
	*/	
    public function reset() : Void
    {
        this.DEBUG ? _global.debug("frusion.FrusionManager::reset" ) : undefined;
        
        this.changeFrusionState( Context.FRUSION_RESET_STATE );
    }


	/*
	 	Function: shutDown
        
        *Called by frusion when shut down is called*
	*/	
    public function eject() : Void
    {
        this.DEBUG ? _global.debug("frusion.FrusionManager::eject" ) : undefined;
        
        // test if we can communicate with client otherwise, kill everything
        if( this.localConnectionOk )
            this.changeFrusionState( Context.FRUSION_CLOSE_STATE );
        else
            this.onReadyToClose();
    }


	/*
	 	Function: registerStartGame
	 	
	 	*Inform the slot that the game has been created .*
	*/	
    public function registerStartGame() : Void
    {
        // give size before starting game
        this.slot.updateSize();
        this.slot.registerStartGame();
    }


	/*
	 	Function: registerConnection
	*/	
    public function registerConnection() : Void
    {
        this.slot.registerConnection();
    }


	/*
	 	Function: registerClientConnection
	 	
	 	*Tell the slot that the local connection has been opened .*
	*/	
    public function registerClientConnection() : Void
    {
        this.localConnectionOk = true;
    }


	/*
	 	Function: onReadyToClose
	 	
	 	*Tell the slot that it is safe to turn off everything .*
	*/	
    public function onReadyToClose() : Void
    {
        this.DEBUG ? _global.debug("frusion.FrusionManager::onReadyToClose" ) : undefined;
        
        this.shut = false;
        this.slot.onReadyToClose();
        this.finalize();
    }


	/*
	 	Function: createSlot
	 	
	 	*Turn slot created flag to true so we can start using the slot menu .*
	*/	
    public function createSlot() : Void
    {
        this.slot.createSlot();
    }


	/*
	 	Function: stopFrusion
	 	
	 	*When a game is not readable by the frusion, we just eject it.*
	*/	
    public function stopFrusion() : Void
    {
        this.DEBUG ? _global.debug("frusion.FrusionManager::stopFrusion" ) : undefined;
        
    	_global.main.frusion.stopDisc( Context.FRUSION_RELEASEDISC );
        _global.fileMng.frusionOff();		
        _global.openErrorAlert( Lang.fv( "error.frusion.fatalError" ) + Context.ERROR_GAME_UNREADABLE 
                + "<br>" + Lang.fv( "error.frusion.blur." + Context.ERROR_GAME_UNREADABLE )); 
    }


/*------------------------------------------------------------------------------------
 * Private methods 
 *------------------------------------------------------------------------------------*/	


	/*
	 	Function: checkOpenMode
	 	
	 	*determinates opn mode for the frusion*
	*/	
	private function checkOpenMode() : Number
	{
		// Check whether we have enough space to display game 
		var enoughSpace:Boolean;
		enoughSpace = (this.gameDisc.width <= _global.mcw-_global.main.cornerX) && (this.gameDisc.height <= _global.mch-_global.main.cornerY);
		
		// Check if navigator is internet explorer 
		var ieWin:Boolean = ( _root.cwm == Context.ONE );
		
		
		/* Choose open mode:
			 - popup
			 - in frutiparc swf
			 - in a a frame
		*/
        // XXX test :)
        ///*
        if( this.gameDisc.playMode == Context.FRUSION_MULTIPLAYER_LOADER )  return Context.FRUSION_OPENMODE_INTERNAL;
        if(!enoughSpace) 		                                    return Context.FRUSION_OPENMODE_POPUP;
		if(this.gameDisc.mode == Context.GAMEDISC_INTERNAL_MODE)   return Context.FRUSION_OPENMODE_INTERNAL;
        if(ieWin)			                                        return Context.FRUSION_OPENMODE_FRAME;
		return Context.FRUSION_OPENMODE_POPUP;
        //*/
        //_root._alpha = 50;
/*
        if( this.gameDisc.playMode == Context.FRUSION_MULTIPLAYER_LOADER )  return Context.FRUSION_OPENMODE_INTERNAL;
        if(!enoughSpace) 		                                            return Context.FRUSION_OPENMODE_FRAME;
		if(this.gameDisc.mode == Context.GAMEDISC_INTERNAL_MODE)            return Context.FRUSION_OPENMODE_INTERNAL;
        if(ieWin)			                                                return Context.FRUSION_OPENMODE_FRAME;
		                                                                    return Context.FRUSION_OPENMODE_FRAME;
                                                                        */
	}

}
