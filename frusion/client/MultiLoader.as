/*
 *	$Id$
 */


import frusion.gamedisc.GameDisc;
import frusion.util.FrutiConnectFileLoader;
import frusion.client.FrusionLoader;
import ext.util.XMLFileLoader;
import ext.util.Callback;
import frusion.Context;


/*
	Class: frusion.client.MultiLoader
	
	The MultiLoader class is responsible for loading the frusion logo anim 
	and the game and the multi player window
*/
class frusion.client.MultiLoader extends FrusionLoader
{


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


	private var frutiConnectLoadingCompleteState : Boolean = false;
	private var frutiConnectFileLoader : FrutiConnectFileLoader;
	private var frutiConnectDepth : Number;

	private var xmlPanelFileLoadingCompleteState : Boolean = false;
	private var xmlPanelLoader : XMLFileLoader;

	private var xmlPlayerPanelFileLoadingCompleteState : Boolean = false;
	private var xmlPlayerPanelLoader : XMLFileLoader;

    private var reloaded : Boolean = false;


/*------------------------------------------------------------------------------------
 * Cleanup methods
 *------------------------------------------------------------------------------------*/


    /*
        transitory cleanup before game starts
    */
    private function transitoryFinalize() : Void
    {
        super.transitoryFinalize();
        
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::transitoryfinalize" ) : undefined;
        
		this.frutiConnectFileLoader.removeListener(this);
        delete this.frutiConnectFileLoader;
        this.debugText = null;
        
        this.xmlPanelLoader.finalize();
        delete this.xmlPanelLoader;
        this.debugText = null;
        
        this.xmlPlayerPanelLoader.finalize();
        delete this.xmlPlayerPanelLoader;
        this.debugText = null;
	}
	

    private function _initVar()
    {
    }

/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function : MultiLoader
		Constructor

		Parameters:
			- mc : MovieClip - context for newly created movie clips ( game and anim )
			- gameDisc :  GameDisc - current game
	*/
	public function MultiLoader( baseURL : String, mc : MovieClip, debugFunction : Function )
	{
		super( baseURL, mc, debugFunction );
        
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::MultiLoader" ) : undefined;
		this.frutiConnectDepth = 7;

	}


/*------------------------------------------------------------------------------------
 * Public callback methods
 *------------------------------------------------------------------------------------*/

    public function _startLoading() : Void
    {
        super._startLoading();

		// creating new frutiConnect loader
		this.loadFrutiConnect( true );        

        // Loading XML Panel File
		// Try to load Xml File containing service ports
		// Note that we use _global.baseURL instead of baseURL, the later arguments.callee.context.being equal to _global.swfURL
	    this.xmlPanelLoader 
            = new XMLFileLoader( _global.baseURL + Context.XML_PATH + this.gameDisc.swfName + Context.XML_PANEL_SUFFIX);
        this.xmlPanelLoader.setErrorCallback( new Callback( this, onXMLPanelLoadError ) );
        this.xmlPanelLoader.load( new Callback( this, onXmlPanelFileLoadingComplete ) );

	    this.xmlPlayerPanelLoader 
            = new XMLFileLoader( _global.baseURL + Context.XML_PATH + this.gameDisc.swfName + Context.XML_PLAYERPARAM_PANEL_SUFFIX );
        this.xmlPlayerPanelLoader.setErrorCallback( new Callback( this, onXMLPlayerPanelLoadError ) );
        this.xmlPlayerPanelLoader.load( new Callback( this, onXmlPlayerPanelFileLoadingComplete ) );
    }

	public function onGameLoadStart() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onGameLoadStart" ) : undefined;
		this.mc.game._visible = false;
	}


    /*
        Function: onXMLPlayerPanelLoadError
        CALLABCK. called when it is impossible to load the file
    */
    public function onXMLPlayerPanelLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onXMLPlayerPanelLoadError" ) : undefined;
        this.stopLaunchingProcess( Context.ERROR_MISSING_PLAYERPARAM_PANEL_XML );
    }


    /*
        Function: onXMLPanelLoadError
        CALLABCK. called when it is impossible to load the file
    */
    public function onXMLPanelLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onXMLPanelLoadError" ) : undefined;
        this.stopLaunchingProcess( Context.ERROR_MISSING_PANEL_XML );
    }


    /*
        Function: onFrutiConnectLoadError
        CALLABCK. called when it is impossible to load the file
    */
    public function onFrutiConnectLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onFrutiConnectLoadError" ) : undefined;
        this.stopLaunchingProcess( Context.ERROR_MISSING_FRUTICONNECT_SWF );
    }


	/*
		Function: onXmlPanelFileLoadingComplete 
		CALLBACK. update state
	*/
	public function onXmlPanelFileLoadingComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onXmlPanelFileLoadingComplete" ) : undefined;
	    this.xmlPanelFileLoadingCompleteState = true;
	}


	/*
		Function: onXmlPlayerPanelFileLoadingComplete 
		CALLBACK. update state
	*/
	public function onXmlPlayerPanelFileLoadingComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onXmlPlayerPanelFileLoadingComplete" ) : undefined;
	    this.xmlPlayerPanelFileLoadingCompleteState = true;
	}


	/*
		Function: onfrutiConnectLoadInit
		CALLBACK. update state
	*/
	public function onFrutiConnectLoadInit() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onFrutiConnectLoadInit" ) : undefined;
        if( !this.frutiConnectLoadingCompleteState )
            this.mc.frutiConnect.stop();

	    this.tryToLaunchGame();
	}


	/*
		Function: onfrutiConnectLoadComplete
		CALLBACK. update state
	*/
	public function onFrutiConnectLoadComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::onFrutiConnectLoadComplete" ) : undefined;
        this.frutiConnectLoadingCompleteState = true;

	    this.tryToLaunchGame();
	}


    /*
        Function: breakFrutiConnect

        When playing remove frutiConnect UI in order to get some extra FPS        
    */
    public function breakFrutiConnect() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::breakFrutiConnect" ) : undefined;
	    this.mc.frutiConnect._visible = false;				
        this.mc.frutiConnect.removeMovieClip();
    }


    /*
        Function: loadFrutiConnect
    */    
    public function loadFrutiConnect( first : Boolean ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiLoader::loadFrutiConnect" ) : undefined;
        this.reloadFrutiConnect( first );
    }

    
    /*
        Function: reloadFrutiConnect

        When the player finishes a multiplayer game, we rebuild the frutiConnect UI
    */    
    public function reloadFrutiConnect( first : Boolean  ) : Void
    {
	    frutiConnectLoadingCompleteState = false;
        this.DEBUG ? _global.debug("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxxxfrusion.client.MultiLoader::reloadFrutiConnect" ) : undefined;
        this.mc.frutiConnect.removeMovieClip();
		this.mc.frutiConnect = this.mc.createEmptyMovieClip( Context.FRUTICONNECT, this.frutiConnectDepth);

		this.mc.frutiConnect._visible = false;

        _global.debug( "URL=" + baseURL + Context.FRUTICONNECT_SWF );
        _global.debug( "this.mc.frutiConnect=" + this.mc.frutiConnect );

		this.frutiConnectFileLoader = new FrutiConnectFileLoader( baseURL + Context.FRUTICONNECT_SWF, null );
		this.frutiConnectFileLoader.addListener(this);
		this.frutiConnectFileLoader.loadClip( this.mc.frutiConnect );

        if( !first ) this.reloaded = true;
    }


	/*
		Function: clean
		
		*Remove movie clips of no use.*
	*/
    public function clean() : Void
    {
        _global.debug( "clean()" );
    	delete this.xmlPlayerPanelLoader;
    	delete this.xmlPanelLoader;
		delete this.frutiConnectFileLoader;
        
        this.mc.fruticonnect.removeMovieClip();
        super.clean();
    }

    
/*------------------------------------------------------------------------------------
 * Private methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: tryToLaunchGame
		Verifies states and launch game is all conditions are met:
		- FrutiConnect is loaded
		- Anim is loaded
		- Anim has finished its play
		- Game is loaded
	*/
	private function tryToLaunchGame() : Void
	{
        _global.debug( "tryToLaunchGame()" );
        _global.debug( "this.gameReadyState" + this.gameReadyState );
		if( this.frutiConnectLoadingCompleteState
		    && this.animLoadingCompleteState
		        && this.animPlayingCompleteState 
    			    && this.gameLoadingCompleteState
	    			    && this.xmlPanelFileLoadingCompleteState
                           && this.xmlPlayerPanelFileLoadingCompleteState  
                                && this.gameReadyState
                                    && this.mc.frutiConnect.attachFrutiConnect != undefined )
		{	

            this.DEBUG ? _global.debug("frusion.client.MultiLoader::tryToLaunchGame" ) : undefined;
            
            if( this.mc.game.manager == undefined && this.mc.game.main.manager == undefined ) 
            {
                this.DEBUG ? _global.debug("frusion.client.MultiLoader::tryToLaunchGame : marche p� le manager" ) : undefined;
                this.mc.game.removeMovieClip();
    		    this.finalize();
    			this.stopLaunchingProcess( Context.ERROR_MISSING_GAMEMANAGER );
			    return;
		    }
    
            this.DEBUG ? _global.debug("frusion.client.MultiLoader::setting mediator" ) : undefined;

            // Give full powers to MultiLoader as a mediator
            this.mc.game.main.mediator = this;
            
		    // Get manager from the game with following prerequisites:
		    // - game must have a main movie clip where the manager will be located  
		    // - the manager class used by the game must implement interface MultiManager
		    // Now give our fruticonnect our ready to use manager 
            if( this.mc.game.manager != undefined)
            {
    		    this.mc.frutiConnect.manager = this.mc.game.manager;
      		    this.mc.frutiConnect.roomId = this.mc.game.manager.roomId;
            }
            else
            {
    		    this.mc.frutiConnect.manager = this.mc.game.main.manager;
      		    this.mc.frutiConnect.roomId = this.mc.game.main.manager.roomId;
            }
       		this.mc.frutiConnect.gameName = this.gameDisc.swfName;
            this.DEBUG ? _global.debug("frusion.client.MultiLoader::launching fruticonnect with game " + this.gameDisc.swfName) : undefined;
            
 		    this.mc.frutiConnect.play();
		    this.mc.frutiConnect._visible = true;				
            this.mc.frutiConnect.attachFrutiConnect();

            _global.debug( "1 ="  + this.mc  );
            _global.debug( "2 ="  + this.mc.frutiConnect  );
            _global.debug( "3 ="  + this.mc.frutiConnect.attachFrutiConnect  );

            // In case we reloaded, perform a new listGame to update fruticonnect UI 
            if( this.reloaded )
            {
                this.DEBUG ? _global.debug("frusion.client.MultiLoader::tryToLaunchGame : reloading fruticonnect" ) : undefined;
                this.mc.frutiConnect.manager.listGames();
                this.reloaded = false;
            }
            else
            {
                this.DEBUG ? _global.debug("frusion.client.MultiLoader::tryToLaunchGame : this.xmlPanelLoader" ) : undefined;
                if( this.mc.game.manager != undefined)
                {
                    this.DEBUG ? _global.debug("frusion.client.MultiLoader::laoding fruticonnect : ok" ) : undefined;
                    this.mc.game.manager.init( this.gameDisc.swfId, 
                                                this.xmlPanelLoader, 
                                                this.xmlPlayerPanelLoader );
                }
                else
                {
                    this.mc.game.main.manager.init( this.gameDisc.swfId, 
                                                    this.xmlPanelLoader, 
                                                    this.xmlPlayerPanelLoader );
                }

                this.transitoryFinalize();
            }
		}
	}


}   // EOF

