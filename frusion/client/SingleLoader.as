/*
 *	$Id$
*/


import frusion.client.FrusionLoader;
import frusion.Context;
import frusion.gamedisc.GameDisc;


/*
	Class: frusion.client.SingleLoader
	
	The SngleLoader class is responsible for loading a game
*/
class frusion.client.SingleLoader extends FrusionLoader
{


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function : FrusionLoader
		Constructor

		Parameters:
			- mc : MovieClip - context for neonLoadInitwly created movie clips ( game and anim )
			- gameDisc :  GameDisc - current game
	*/
	public function SingleLoader( baseURL : String, mc : MovieClip ) 
	{
        super( baseURL, mc );
        this.DEBUG ? _global.debug("frusion.client.SingleLoader::SingleLoader"  ) : undefined;
	}


/*------------------------------------------------------------------------------------
 * Private methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: tryToLaunchGame
		Verifies states and launch game is all conditions are met.
	*/
	private function tryToLaunchGame() : Void
	{
		if( this.gameLoadingCompleteState
				&& this.animLoadingCompleteState
					&& this.animPlayingCompleteState )
		{
            this.DEBUG ? _global.debug("frusion.client.SingleLoader::tryToLaunchGame : this.mc.game._currentframe="  
                + this.mc.game._currentframe ) : undefined;

            this.mc.game._visible = true;
            this.mc.game.play();
            this.transitoryFinalize();
		}
	}


}   // EOF

