/*
 *	$Id: PreviewLoader.as,v 1.2 2004/06/22 16:46:42 fnicaise Exp $
 */


import frusion.client.FrusionLoader;
import frusion.Context;
import frusion.gamedisc.GameDisc;


/*
	Class: frusion.client.PreviewLoader
	
	The SingleLoader class is responsible for loading a preview
*/
class frusion.client.PreviewLoader extends FrusionLoader
{


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function : FrusionLoader
		Constructor

		Parameters:
			- mc : MovieClip - context for neonLoadInitwly created movie clips ( preview and anim )
			- gameDisc :  GameDisc - current preview
	*/
	public function PreviewLoader( baseURL : String, mc : MovieClip )  
	{
        super( baseURL, mc );
        this.DEBUG ? _global.debug("frusion.client.PreviewLoader::PreviewLoader"  ) : undefined;
        _global.main.mainBar.toggleHalfHide(true);
	}


    public function previewComplete() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.PreviewLoader::previewComplete"  ) : undefined;
        
        var lc : LocalConnection = new LocalConnection();
        lc.send( Context.FRUSION_SERVER_LOCAL_CONNECTION, Context.FRUSIONSERVER_CLOSECONNECTION_METHOD );
        lc = null;
        delete lc;
    }
    

    public function finalize() : Void
    {
        super.finalize();
        _global.main.mainBar.toggleHalfHide(false);
    }
    
    
/*------------------------------------------------------------------------------------
 * Private methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: tryToLaunchGame
		Verifies states and launch preview is all conditions are met.
	*/
	private function tryToLaunchGame() : Void
	{
		if( this.gameLoadingCompleteState
				&& this.animLoadingCompleteState
					&& this.animPlayingCompleteState )
		{
            this.DEBUG ? _global.debug("frusion.client.PreviewLoader::tryToLaunchGame : this.mc.preview._currentframe="  
                + this.mc.game._currentframe ) : undefined;

            this.mc.game._visible = true;
            this.mc.game.play();
            this.transitoryFinalize();
		}
	}


}   // EOF

