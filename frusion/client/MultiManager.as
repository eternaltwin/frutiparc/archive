/*
 * $Id $
 */


import frusion.client.FrusionClient;
import frusion.client.multi.UIManager;
import frusion.client.multi.DataManager;
import frusion.client.multi.UIPlayer;
import frusion.client.multi.UIGame;
import ext.util.Callback;


/*
    Class: frusion.client.MultiManager

    MultiManager provides with methods to handle frutiConnect information needs
*/
class frusion.client.MultiManager
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = false;


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


    public var mc : MovieClip;
    public var gamePanel : XML;
    public var playerPanel : XML;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/

    private var gameIsStarted : Boolean;
    private var okToAsk : Boolean;
    private var lastChallengerSelected : String;
    private var currentOpponentsList : String;
    private var displayTimeMessage : Boolean = false;
    private var uiManager : UIManager;
    private var dataManager : DataManager;
    private var frusionClient : FrusionClient;
    private var discId : String;
    private var debug : Function;
    private var max : Number;
    private var pageIndex : Number;
    private var playerNumber : Number;
    private var gameMode : Number;
    private var roomId : String;
    
    // Game modes        
    private var FREE_MODE : Number = 0;
    private var CHALLENGE_MODE : Number = 1;
    private var CHAMPIONSHIP_MODE : Number = 2;
    
    
/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


    /*
        Function: MultiManager
        
        Constructor
    */
    public function MultiManager( debugFunction: Function )
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::MultiManager" ) : undefined;
        this.gameIsStarted = false;
        this.okToAsk = false;
        this.lastChallengerSelected = "";
        this.currentOpponentsList = "";
        this.debug = debugFunction;
        this.pageIndex = 0;
        this.max = 3;
        this.playerNumber = 0;
    }


    /*
        Function: initUI
        
        Inits our UIManager
    */
    public function initUI( mc : MovieClip ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::initUI" ) : undefined;
        this.uiManager = new UIManager( mc );
        this.mc = mc;
        
        _root._alpha += 20;
        //this.mc._alpha += 20; 
    }
 

    /*
        Function: hideUI
        
        Constructor
    */
    public function hideUI( mc : MovieClip ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::hideUI" ) : undefined;
        this.uiManager.hide();
    }
 
    
    public function getDiscID() : String
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::getDiscID" ) : undefined;
        return this.frusionClient.gameDisc.gameId;
    }


    public function peepShow( roomId : String, mode : Number ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::peepShow mode=" + mode ) : undefined;
        this.roomId = roomId;
        this.gameMode = mode;
        this.uiManager.initRoom();
    }
    

    public function showBoobs() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::showBoobs" ) : undefined;
        this.uiManager.initRoom();
    }


    /*  
        Function: init
        
        Inits the Manager, create connexion to service
    */
    public function init( discId : String, gamePanel : XML, playerPanel : XML )
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::init" ) : undefined;
        
        this.dataManager = new DataManager();
        this.frusionClient = new FrusionClient();
        this.frusionClient.registerReadyCallback( new Callback(this,onIdentFinished) );				
        this.frusionClient.registerPauseCallback( new Callback(this,onEventPause)) ;
        this.frusionClient.registerCloseCallback( new Callback(this,onEventClose)) ;
        this.frusionClient.registerResetCallback( new Callback(this,onEventReset)) ;
        this.frusionClient.getService();

        this.gamePanel = gamePanel;
        this.playerPanel = playerPanel;
        this.discId = discId;
    } 

    public function getUserName() : String
    {
        return this.frusionClient.getUserName();
    }

    public function registerGame() : Void
    {
        this.frusionClient.registerStartGame();
    }

    public function sendCommand( command : String, parameters : Array )
    {
        this.frusionClient.sendCommand( command, parameters );
    } 

	public function sendCommandWithText( command: String, parameters: Array, data : String ) : Void
    {
        this.frusionClient.sendCommandWithText( command, parameters, data );
    }

    public function registerCallbackList( callbackList : Object ) : Void
    {
        this.frusionClient.registerCallbackList( callbackList );
    }
 
/*------------------------------------------------------------------------------------
 * Events callbacks
 *------------------------------------------------------------------------------------*/


    /*
        Function: onEventPause
        
        Callback called when the frusion ask for a pause.
    */
    public function onEventPause() : Void 
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::onEventPause  status :"+ this.frusionClient.pauseStatus ) : undefined;
    }


    /*
        Function: onEventClose
        
        Callback called when the frusion ask for closing.
    */
    public function onEventClose() : Void 
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::onEventClose" ) : undefined;
        
        // XXX temporary
        this.frusionClient.closeService();
    }


    /*
        Function: onEventReset
        
        Callback called when the frusion ask for reset.
    */
    public function onEventReset() : Void 
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::onEventReset" ) : undefined;
    }


/*------------------------------------------------------------------------------------
 * Diffused information
 *------------------------------------------------------------------------------------*/


    public function onIdentFinished() : Void 
    {
        this.DEBUG ? _global.debug( "onIdentFinished" ) : undefined;

        this.frusionClient.registerConnection();
        this.onCbkIdentFinished();
    }
    
    public function onCbkIdentFinished() : Void {}


    /*
        Function: onCbkPlayerLeftGame
        
        Perform a list update when a player has left a game
    */
    public function onCbkPlayerLeftGame( nodeTxt : String ) : Void 
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame" ) : undefined;

        var node = new XML(nodeTxt) ;
        var usr : String = node.firstChild.attributes.u;
        
        // test if current user is the one who left
        if( usr == this.frusionClient.getUserName() )
        {
            // test if it was current user's game
            if( this.dataManager.leaveMyGame( usr ) )
            {
                // Current user leaves his game
                this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - current player leaves his game" ) :
                undefined;
                    
                this.dataManager.iLeaveMyGame( usr );
                this.uiManager.clearGame();
                this.playerNumber = 0;
            }
            else
            {
                this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - current player leaves a game" ) :
                undefined;
                    
                this.dataManager.iLeaveGame( usr );
                this.uiManager.clearGame();
                this.playerNumber = 0;
            }
        }
        else
        {
            this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - uLeaveGame" ) : undefined;
                
            // test if current player was in players'game
            if( this.dataManager.playerWasInGame( usr, this.frusionClient.getUserName() ) )
            {
                this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - I was in his Game" ) : undefined;
                
                // current was in the game
                // case for any other user
                this.currentOpponentsList ="";
                var players : Array = this.dataManager.getPlayerList( usr, this.playerNumber );
                var usrWasCreator : Boolean = this.dataManager.leaveGame( usr );
    
                // test cases : player was creator, player was ...  player ;)
                if( usrWasCreator ) 
                    this.uiManager.hidePlayersPanel();
                else
                    this.uiManager.displayPlayersPanel( players );
                    //this.gameJoined( false, players )
            }
            else
            {
                // Current player was not in the game
                // test if player was in current player's game
                if( this.dataManager.playerWasInGame( this.frusionClient.getUserName() , usr ) )
                {
                    // player is in current's player game
                    this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - he was in my Game" ) : undefined;
                        
                    // user who left was in current player's game
                    this.dataManager.playerLeavesMyGame( usr, this.frusionClient.getUserName() );

                    // get current players' game list
                    var players : Array = this.dataManager.getMyPlayersList( this.frusionClient.getUserName(), this.playerNumber );
                    this.uiManager.displayCreatorPanel( players );
                }
                else
                {
                    // neither player nor current player know each other ! This begins to feel weird...
                    // Strange things start to happen elsewhere....
                    this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - we were in neither's game" ) :
                    undefined;
                        
                    if( this.dataManager.wasCreator( usr ) )
                    {
                        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - lambda player leaving game" )
                        : undefined;
                            
                        // user who left was creator and currentPlayer was not in the game
                        this.dataManager.foreignCreatorLeavesGame( usr );
                    }
                    else
                    {
                        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftGame - lambda creator leaving game" )
                        : undefined;
                            
                        // user who left was a player in someone else's game
                        this.dataManager.playerLeavesGame( usr );
                    }                  
                }                    
            }
        }
        
        this.updateList();
    }

    
    /*
        Function: onCbkPlayerJoinedGame
        
        Perform a list update when a player has joined a game
    */
    public function onCbkPlayerJoinedGame( nodeTxt : String ) : Void 
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerJoinedGame" ) : undefined;
            
        var players : Array = this.dataManager.joinGame( nodeTxt );
        this.updateList();
        
        // display list only if current player is the game creator
        if( this.gameMode != this.CHALLENGE_MODE )
        {
            if( String (players[0].user.name) == this.frusionClient.getUserName() )
                this.uiManager.displayCreatorPanel( players );
        }    

        if( this.gameMode == this.CHALLENGE_MODE  )
        {
            var node = new XML(nodeTxt) ;
            var usr : String = node.firstChild.attributes.u;
        
            if( this.lastChallengerSelected == usr ) 
            {
                this.lastChallengerSelected = this.frusionClient.getUserName();
                this.getChallengerInfo( this.lastChallengerSelected );
            }
        }
    }


    /*
        Function: onCbkGameCreated
        
        Perform a list update when a game has been created
    */
    public function onCbkGameCreated( nodeTxt : String ) : Void 
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkGameCreated" ) : undefined;
            
        this.dataManager.addNewGame( nodeTxt );
        this.updateList();

        if( this.gameMode == this.CHALLENGE_MODE )
            this.iAmInGame( nodeTxt )
    }


    /*
        Function: onCbkPlayerJoinedRoom
        
        Perform a list update when a player joins a room
    */
    public function onCbkPlayerJoinedRoom( nodeTxt : String ) : Void 
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerJoinedRoom" ) : undefined;
            
        this.dataManager.addNewPlayer( nodeTxt );
        this.updateList();
    }

    
    /*
        Function: onCbkPlayerLeftRoom
        
        Perform a list update when a player leaves a room
    */
    public function onCbkPlayerLeftRoom( nodeTxt : String ) : Void 
    {
        var node = new XML(nodeTxt) ;
        var usr : String = node.firstChild.attributes.u;
        
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkPlayerLeftRoom usr = " + usr ) : undefined;
            
        this.dataManager.nukePlayer( usr );
        this.updateList();

        if( this.gameMode == this.CHALLENGE_MODE  )
        {
            if( this.lastChallengerSelected == usr ) 
            {
                this.lastChallengerSelected = this.frusionClient.getUserName();
                this.getChallengerInfo( this.lastChallengerSelected );
            }
        }
    }


    /*
        Function: onCbkReceiveMessage
        
        Messages diffused for all players
    */
    public function onCbkReceiveMessage( nodeTxt : String  ) : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkReceiveMessage" ) : undefined;
            
        var node = new XML(nodeTxt) ;
        var usr : String = node.firstChild.attributes.u;
        var str : String = node.firstChild.firstChild.nodeValue;
        this.uiManager.receiveMessage( usr + ">" + str );
    }


    /*
        Function: onCbkReceiveGameMessage
        this.cong
        Messages diffused for players of a given game
    */
    public function onCbkReceiveGameMessage( nodeTxt : String  ) : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkReceiveGameMessage" ) : undefined;
            
        var node = new XML(nodeTxt) ;
        var usr : String = node.firstChild.attributes.u;
        var str : String = node.firstChild.firstChild.nodeValue;
        this.uiManager.receiveMessage( usr + ">" + str );
    }


    /*
        Function: onCbkGameStarted

        *Transform new games into played games*
    */
    public function onCbkGameStarted( gameId : String ) : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkGameStarted" ) : undefined;
            
        this.dataManager.startGame( gameId );
        this.updateList();
    }


    /*
        Function: onCbkGameClosed

        *Remove players from the game and put players in the players's list*
    */
    public function onCbkGameClosed( gameId : String ) : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkGameClosed" ) : undefined;
            
        this.dataManager.closeGame( gameId );
        this.updateList();
    }


    /*
        Function: onCbkNewChallenge
    */
    public function onCbkNewChallenge( nodeTxt : String  ) : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCbkNewChallenge" ) : undefined;
    }
    

/*------------------------------------------------------------------------------------
 * Commands Callbacks
 *------------------------------------------------------------------------------------*/


    /*
        Function: onCmdJoinRoom
    */
    public function onCmdJoinRoom( nodeTxt : String ) : Void 
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCmdJoinRoom" ) : undefined;
    }

    
    /*
        Function: onCmdJoinGame
    */
    public function onCmdJoinGame( nodeTxt : String ) : Void 
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCmdJoinGame for player:" + this.frusionClient.getUserName() ) :
        undefined;
            
        var players : Array = this.dataManager.iJoinGame( nodeTxt, this.frusionClient.getUserName() );
        this.updateList();
        //this.gameJoined( false, players );
        this.uiManager.displayPlayersPanel( players );
    }


    /*
        Function: onListRooms
        CALLBACK. From ListRooms
    */
    public function onCmdListRooms( nodeTxt : String ) : Void
    {        
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCmdListRooms : " + FEString.unHTML( nodeTxt ) ) : undefined;
            
        this.dataManager.initRoomList( nodeTxt );
        this.uiManager.setRoomList( this.dataManager.roomList );
    }


    /*
        Function: onCmdListGames
        
        This callback has two responsibilities:
        - get the list of current games
        - ask for a player list
    */
    public function onCmdListGames( nodeTxt : String  ) : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCmdListGames" ) : undefined;
            
        this.dataManager.initGameList( nodeTxt );
        this.updateList();
        this.listPlayers();
        this.lastChallengerSelected = this.frusionClient.getUserName();
        this.gameIsStarted = false;
    }


    /*
        Function: onCmdChallengerInfo
    */
    public function onCmdChallengerInfo( nodeTxt : String  ) : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.multi.MultiManager::onCmdChallengerInfo for player" ) : undefined;
            
        this.uiManager.displayWaitList( this.dataManager.setChallengerInfo( nodeTxt ) );
        this.displayStartGame();
    }


    /*
        Function: onCmdListPlayers
        
        This callback has two responsibilities:
        - get the list of current players
        - display list
    */
    public function onCmdListPlayers( nodeTxt : String  ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::onCmdListPlayers" ) : undefined;
            
        this.dataManager.initPlayerList( nodeTxt );
        this.lastChallengerSelected = this.frusionClient.getUserName();
        this.updateList();
    }


    /*
        Function: onCmdCreateGame

       Callback when the game is created 
    */
    public function onCmdCreateGame( nodeTxt : String, playerNumber : Number  ) : Void
    {       
        if( this.DEBUG )
        {
            _global.debug("frusion.client.MultiManager::onCmdCreateGame : " + playerNumber );
            _global.debug( "this.frusionClient.getUserName()=" + this.frusionClient.getUserName() );
            _global.debug( "nodeTxt=" + nodeTxt );
        }
        
        //  sets playerNumber for currentGame
        this.playerNumber = playerNumber;
        this.updateList();
        //this.gameJoined( true, this.dataManager.createEmptyGame( this.frusionClient.getUserName(), playerNumber ) );
        if( this.gameMode == this.CHALLENGE_MODE )
        {
             var players : Array = this.dataManager.getPlayerList( this.frusionClient.getUserName(), this.playerNumber );
             //this.uiManager.displayPlayersPanel( players );
             this.uiManager.displayWaitList( this.dataManager.setChallengerInfo( nodeTxt ) );
        }
        else
        {
            this.uiManager.displayCreatorPanel( this.dataManager.createEmptyGame( this.frusionClient.getUserName(), playerNumber ) );
        }    
            
    }


    /*
        Function: onCmdStartGame

        Callback when the game is created 
    */
    public function onCmdStartGame( nodeTxt : String  ) : Void
    {
        this.DEBUG ? this.debug("frusion.client.MultiManager::onCmdStartGame" ) : undefined;
    }


/*------------------------------------------------------------------------------------veR
 * UI Calls
 *------------------------------------------------------------------------------------*/



    /*
        Function: initDefaultGamePanel
    */
    public function initDefaultGamePanel() : Void 
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::initDefaultGamePanel=" + this.gameMode ) : undefined;
            
        switch( this.gameMode )
        {
            case this.FREE_MODE :
            {
                this.DEBUG ? _global.debug("frusion.client.MultiManager::initDefaultGamePanel : FREE_MODE" ) : undefined;
                    
                this.uiManager.displayGameCreationPanel();
                this.uiManager.displayDoc( this.gamePanel );
                break;
            }
            case this.CHAMPIONSHIP_MODE :
            {
                this.DEBUG ? _global.debug("frusion.client.MultiManager::initDefaultGamePanel : CHAMPIONSHIP_MODE " ) : undefined;
                    
                this.uiManager.displayGameCreationPanel();
                break;
            }
            default : 
                break;
        }
    }


    /*
        Function: defyPlayer
    */
    public function defyPlayer( player : UIPlayer ) : Void 
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::defyPlayer :" + player.name ) : undefined;
            
        if( this.gameMode == this.CHALLENGE_MODE )
        {
            this.challengePlayer( player.name );
            //if( player.name != this.frusionClient.getUserName() )
            this.lastChallengerSelected = player.name;
            this.getChallengerInfo( player.name );
            this.okToAsk = true;
        }    
    }
    

    /*
        Function: selectPlayer
    */
    public function selectPlayer( player : UIPlayer ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::selectPlayer :" + player.name ) : undefined;
            
        if( this.gameMode == this.CHALLENGE_MODE )
        {
            //if( player.name != this.frusionClient.getUserName() )
            this.lastChallengerSelected = player.name;
            this.getChallengerInfo( player.name );
            this.okToAsk = true;
        }                
    }


    /*
        Function: dropIn
    */
    public function dropIn( obj : Object ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::dropIn :" + obj.gameId ) : undefined;
            
        this.joinGame( obj.gameId );
    }


    /*
        Function: listGreen

        *Display/hide games*
    */
    public function listGreen( hide : Boolean ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::listGreen=" + hide ) : undefined;
            
        this.dataManager.displayNewGames = hide;
        this.pageIndex = 0;
        this.dataManager.rebuildList();
        this.updateList();
    }
    

    /*
        Function: listYellow

        *Display/hide players*
    */
    public function listYellow( hide : Boolean ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::listYellow=" + hide ) : undefined ;
            
        this.dataManager.displayPlayers = hide;
        this.pageIndex = 0;
        this.dataManager.rebuildList();
        this.updateList();
    }


    /*
        Function: listRed

        *Display / hide all *
    */
    public function listRed( hide : Boolean ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.MultiManager::listRed=" + hide ) : undefined ;
            
        this.dataManager.displayCurrentGames = hide;
        this.pageIndex = 0;
        this.dataManager.rebuildList();
        this.updateList();
    }


    /*
        Function: requestGameInfo

        *give the UI all the information it needs*
    */
    public function requestList( slotMax : Number, cbk : Callback, pageIndex: Number ) : Void 
    {
        this.DEBUG ?  _global.debug("frusion.client.MultiManager::requestList" ) : undefined ;
            
        if( pageIndex != undefined ) 
            this.pageIndex = pageIndex;

        //this.max = slotMax;
        this.updateList();
    }


    /*
     *  Function : gameStarted   
     *
     *  Current game started, remove fruticonnect interface.
     */
    public function gameStarted() : Void
    {
        this.DEBUG ?  _global.debug("frusion.client.MultiManager::gameStarted" ) : undefined ;
        this.gameIsStarted = true;
    }


    /*
        Function: getChallengerInfo
    */
    public function getChallengerInfo( userId : String ) : Void {}


    /*
        Function: leaveGame
    */
    public function leaveGame() : Void {}
    

    /*
        Function: kickPlayer
    */
    public function kickPlayer( playerId : Number ) : Void {}


    /*
        Function: displayError
    */
    public function displayError( msg : String, cbk: Callback ) : Void
    {
        this.uiManager.displayError( msg );
        cbk.execute();
    }


    /*
        Function: displayWarning
    */
    public function displayWarning( msg : String, cbk: Callback ) : Void
    {
        this.uiManager.displayWarning( msg );
        cbk.execute();
    }

    
    
/*------------------------------------------------------------------------------------
 * Private methods
 *------------------------------------------------------------------------------------*/


    private function displayStartGame() : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.MultiManager::displayStartGame" ) : undefined;
            
        if( this.currentOpponentsList != "" )
        {
            this.uiManager.displayDoc( 
                new XML( "<p><l><t s='2' w='200'>Attention, votre prochaine partie (" 
                    + this.currentOpponentsList + ") va commencer dans une minute !</t></l></p>" ) );
        }    
    }


    private function iAmInGame( nodeTxt : String ) : Boolean
    {
        this.DEBUG ? _global.debug( "frusion.client.MultiManager::iAmInGame" ) : undefined;
            
        var me : String = this.frusionClient.getUserName() ;
        var node = new XML(nodeTxt) ;
        var tempList : String = node.firstChild.attributes.uc;

        this.DEBUG ? _global.debug( "frusion.client.MultiManager::iAmInGame list:" + tempList ) : undefined;
            
        var tempArray : Array = tempList.split(";");
        var l : Number = tempArray.length;
        for( var i : Number = 0; i < l ; i++ )
        {
            if( tempArray[ i ] == me )
            {
                this.currentOpponentsList = tempList;
                return;
            }    
        }

        this.currentOpponentsList = "";
    }


    private function updateList() : Void
    {
        this.DEBUG ? _global.debug( "frusion.client.MultiManager::updateList" ) : undefined;
            
        if( this.gameMode == this.CHALLENGE_MODE )
            this.dataManager.updateChallengeList();
        
        this.max = this.dataManager.setPageList( this.pageIndex );
        this.uiManager.refreshUserList( this.dataManager.displayList,this.pageIndex,this.max );
    }

    
/*------------------------------------------------------------------------------------
 * Public methods implementing Manager
 *------------------------------------------------------------------------------------*/


    public function challengePlayer( usr: String ) : Void {}
    public function startGame() : Void {}
    public function createGame( parms : Object ) : Void {}
    public function listRooms() : Void {}
    public function listGames() : Void {}
    public function listPlayers() : Void {}
    public function joinRoom( id : String ) : Void {}
    public function sendMessage( message : String ) : Void {}
    public function cancelGame() : Void {}
    public function joinGame( gameID : String ) : Void {}

}

