/*
	$Id: FrusionClient.as,v 1.1 2003/11/13 16:04:32 fnicaise Exp $
*/


import frusion.server.XMLCommand;
import frusion.gamedisc.GameDisc;
import ext.util.Callback;
import ext.util.Pair;
import frusion.service.FrutiCard;
import frusion.Context;
import ext.util.CardiogramPatient;
import ext.util.Cardiogram;


/*
	Class: frusion.client.FrusionClient
	Client for the singleton FrusionServer.
*/
class frusion.client.FrusionClient extends ext.util.ExtendedLocalConnection implements CardiogramPatient
{


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var alreadyConnected : Boolean = false;
	public var identified : Boolean = false;
	public var frutiCard : FrutiCard;
	public var pauseStatus : Boolean = false;
	public var gameDisc : GameDisc ;
    public var canSaveScore : Boolean;
    public var dailyData : String;
    public var startTime : String;

    /*
    public var frusionX : Number;
    public var frusionY : Number;
    */


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


    private var connectionAlreadyFailed : Boolean = false;
    private var cardiogram : Cardiogram;
	private var currentState : String;
	private var callbackList : Object;
	private var dispatchList : Object;
	public var user: String;
	private var onReadyCallback : Callback;
	private var onUpdateSizeCallback : Callback;
	public var onXMLHook : Function;


/*------------------------------------------------------------------------------------
 * Static
 *------------------------------------------------------------------------------------*/


    public static var id : Number = 0;


/*------------------------------------------------------------------------------------
 * Cleanup methods
 *------------------------------------------------------------------------------------*/

	/*
		Function: finalize
		Plopla! some cleanup :)
	*/
    public function finalize() : Void
    {
        super.finalize();

        delete this.frutiCard;
        this.frutiCard = null;

        this.gameDisc.finalize();
        delete this.gameDisc;
        this.gameDisc = null;

        this.cardiogram.finalize();
        delete this.cardiogram;
        this.cardiogram = null;

        delete this.callbackList;
        this.cardiogram = null;

        delete this.dispatchList;
        this.dispatchList = null;

        this.onReadyCallback.finalize();
        delete this.onReadyCallback;
        this.onReadyCallback = null;

        /*
        this.onUpdateSizeCallback.finalize();
        delete this.onUpdateSizeCallback;
        this.onUpdateSizeCallback = null;
        */
    }


/*------------------------------------------------------------------------------------
 * public methods for communication with client
 *------------------------------------------------------------------------------------*/


	/*
		Function: registerUpdateSizeCallback
		Set callback for client.
		This callback is called when frutiparc gets an updateSize signal
	*/
    /*
	public function registerUpdateSizeCallback( callback : Callback ) : Void
	{
		this.onUpdateSizeCallback = callback;
	}
    */


	/*
		Function: registerReadyCallback
		Set callback for client.
		This callback is called when the client is identified and fruticard information are get
	*/
	public function registerReadyCallback( callback : Callback ) : Void
	{
        debug( "FrusionClient::registerReadyCallback" );
		this.onReadyCallback = callback;
	}


	/*
		Function: registerPauseCallback
		Set callback for client.
		This callback is called when the game is put in pause by server
	*/
	public function registerPauseCallback( callback : Callback ) : Void
	{
        debug( "FrusionClient::registerPauseCallback" );
		this.dispatchList[ Context.FRUSION_PAUSE_ON_STATE ] = callback;
		this.dispatchList[ Context.FRUSION_PAUSE_OFF_STATE ] = callback;
	}


	/*
		Function: registerCloseCallback
		Set close callback for client.
		This callback is called when the game is closed by server
	*/
	public function registerCloseCallback( callback : Callback ) : Void
	{
        debug( "FrusionClient::registerCloseCallback" );
		this.dispatchList[ Context.FRUSION_CLOSE_STATE ] = callback;
	}


	/*
		Function: registerResetCallback
		Set reset callback for client.
		This callback is called when the game is reset by server
	*/
	public function registerResetCallback( callback : Callback ) : Void
	{
		this.dispatchList[ Context.FRUSION_RESET_STATE ] = callback;
	}


/*------------------------------------------------------------------------------------
 * public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: registerStartGame

		Tell frutiparc that a game was started and thus
        ensure we burn the gamedisc once used
	*/
    public function registerStartGame() : Void
    {
        this.sendRemote( Context.FRUSIONSERVER_REGISTERSTARTGAME_METHOD, null );
    }


	/*
		Function: closeService

		close current service connection
	*/
	public function closeService() : Void
	{
        this.sendRemote( Context.FRUSIONSERVER_CLOSECONNECTION_METHOD, null );
        this.close();
        this.finalize();
    }

	public function sendXml( node : XML ) : Void
	{
        this.sendRemote( Context.FRUSIONSERVER_SENDXML_METHOD, node );
    }


	/*
		Function: registerConnection

		Inform FP UI that a connection has been opened
	*/
	public function registerConnection() : Void
	{
        this.sendRemote( Context.FRUSIONSERVER_REGISTERCONNECTION_METHOD, null );
    }


	/*
		Function: FrusionClient

		Public constructor
	*/
	public function FrusionClient()
	{
		super( Context.FRUSION_CLIENT_LOCAL_CONNECTION + _root.sid, Context.FRUSION_SERVER_LOCAL_CONNECTION + _root.sid );
        this.addDomain( Context.BASE_DOMAIN );
        this.addDomain( Context.SENDING_DOMAIN );

		this.callbackList = new Object();
		this.dispatchList = new Object();

        this.connectionAlreadyFailed = false;

		// Inits connection to send info to server
        this.initRemote();
        this.enableConnectionFailureHandling( new Callback( this, manageLCFailure ) );

        this.cardiogram = new Cardiogram( 1000, 2000, 1, this );
        this.cardiogram.start();

		// Inits connection to get
        // information from the server
		// Note: use of underscore is important as connections
		// through different domains need this to find the shared LocalConnection
		// see flash doc (allow domain)
		this.connect();

		// Create listeners
		AsBroadcaster.initialize(this);

        // register connection
        FrusionClient.id++;

        // we send _root.testId to test if we got som info and so are in popup mode
        //this.sendRemote( Context.FRUSIONSERVER_REGISTERCLIENTCONNECTIONCONNECTION_METHOD, null, FrusionClient.id, _root.testId, _root.sid);
        this.sendRemote( Context.FRUSIONSERVER_REGISTERCLIENTCONNECTIONCONNECTION_METHOD, null, FrusionClient.id, _root.sid);
        this.canSaveScore = false;
	}


	/*
		Function: manageConnectionFailure
		When we lose the local connection handle failure
	*/
    public function manageLCFailure() : Void
    {
        //debug( "FrusionClient::manageConnectionFailure" );
        if( this.connectionAlreadyFailed ) return;
        this.connectionAlreadyFailed = true;
        //debug( "FrusionClient::manageConnectionFailure::OK" );
        this.cardiogram.stop();
        this.close();
        this.remote = null;
        getURL( Context.JS_SELFCLOSE, Context.SELF );
    }


	/*
		Function: getService
		Open a connection to the specified service

		Parameters:
		- serviceName : Number - the service name or port on host
	*/
	public function getService() : Void
	{
		// Note: use of underscore is important as connections
		// through different domains need this to find the shared LocalConnection
		// see flash doc (allow domain)
        _global.debug( "FrusionClient::getService (log is working?)" );
        this.sendRemote( Context.FRUSIONSERVER_GETSERVICE_METHOD, null, null );
	}


	/*
		Function: registerCallbackList
		Registers client callback functions.

		Parameters:
		- callback : Object - list of callback functions

	*/
	public function registerCallbackList( callbackList : Object ) : Void
	{
		// updating list with new callbacks
		for( var n in callbackList )
        {
            //_global.debug(" n=" + n);
            //_global.debug( callbackList[n] );
			this.callbackList[n] =  callbackList[n];
        }
	}


	/*
		Function: sendCommand
		Send an xml command to server

		Parameters:
		commandName - String : command name
		parameters - Array of CommandParameter(s) objects: all the command parameters
	*/
	public function sendCommand( commandName: String, parameters: Array ) : Void
	{
        _global.debug( "-----------------------FrusionClient::SendCommand------------");
        this.sendRemote( Context.FRUSIONSERVER_SENDCOMMAND_METHOD,
            XMLCommand.buildCommand( commandName, parameters, null ), FrusionClient.id );
	}


	/*
		Function: sendCommandWithText
		Send an xml command to server with an xml node

		Parameters:
		commandName - String : command name
		parameters - Array of CommandParameter(s) objects: all the command parameters
		data - Object
	*/
	public function sendCommandWithText( commandName: String, parameters: Array, data : String ) : Void
	{
        this.sendRemote( Context.FRUSIONSERVER_SENDCOMMAND_METHOD,
            XMLCommand.buildCommand( commandName, parameters, data ), FrusionClient.id );
	}


	/*
		Function fatalError
		Log errors and stop frusion
		Note: please encrypt data before using logError...
	*/
	public function fatalError( msg : String, msgUsr : String ) : Void
	{
        //debug( "frusionclient fatal error : " + code );
        this.sendRemote( Context.FRUSIONSERVER_CUSTOMFATALERROR_METHOD, msgUsr, msg );
        this.sendCommandWithText( "bf", new Array( new Pair("l", Context.ERROR_SECURITY ), new Pair("m", this.gameDisc.swfName) ), msg );
        this.close();
	}


	/*
		Function: LogError
		Log errors
		Note: please encrypt data before using logError
    */
	public function logError( code : String, message : String ) : Void
	{
        // save log on server
        this.sendCommandWithText( "bf", new Array( new Pair("l", code), new Pair("m", this.gameDisc.swfName) ), message );
	}


   public function debug( mess: String )
   {
        // append to custom debug if beta else to frusion server
        if( Context.BETA_DOMAIN != Context.BASE_DOMAIN )
            this.sendRemote( "debug", mess );
        else
            _global.debug( mess );//_global.customDebug( mess );
   }

/*------------------------------------------------------------------------------------
 * Public methods incoming from LocalConnection
 *------------------------------------------------------------------------------------*/


	/*
		Function : onXML
		Called by FrusionServer when XML information has arrived
		and are not to be treated by the server.
		The function parses xml and see if it finds the callback to call
		in its callback list

		Parameters:
		-node : XML - the node sent by the server
	*/
	public function onXML( received : Object ) : Void
	{

        var found : Boolean = false;
		var xmlReceived = new XML( received[1].toString());

		if( onXMLHook != null ) {
			if( xmlReceived.firstChild.nodeName == "ab" )
				this.onReadyCallback.execute();
			else
				onXMLHook(xmlReceived.firstChild);
			return;
		}

        _global.debug( "------> on XML !!!!!!");
        _global.debug( "------>" + xmlReceived.firstChild.nodeName );

        if( xmlReceived.firstChild.nodeName == "c" )
        {
            this.onGotTime( received );
        }

        if( xmlReceived.firstChild.nodeName == "ab" )
        {
            if( xmlReceived.firstChild.attributes.k != undefined
                && this.gameDisc.playMode == Context.FRUSION_SINGLEPLAYER_LOADER )
            {
                // XXX fatal error
                this.fatalError( "3", "received useDisc signal but game is multiplayer ??!!" );
                return;
            }
            else
                this.onUseDisc( received );
        }
        else
        {
            var event = this.callbackList[xmlReceived.firstChild.nodeName];
            this.broadcastMessage(event,xmlReceived);
        }
    }


	/*
		Function: onIdent
	*/
	public function onIdent() : Void
	{
        _global.debug( "FrusionClient:onIdent" );
		this.identified = true;

		// ask for gamedisc if needed
        if( this.alreadyConnected ) return;
        this.alreadyConnected = true;

    	this.getGameDisc();
	}


	/*
		Function: onReconnect
	*/
	public function onReconnect() : Void
	{
        debug( "FrusionClient:onReconnect" );
        this.sendCommand( "ab", new Array( new Pair("d", this.gameDisc.gameId) ) );
	}


    public function onGotTime( node : Object ) : Void
    {
		var xmlReceived = new XML( node[1].toString());
        this.startTime = xmlReceived.firstChild.firstChild.nodeValue.toString();

        _global.debug(FEString.unHTML( xmlReceived.firstChild ) );
        _global.debug(this.startTime);
        _global.debug("--------------------------------");

        //this.getUser();
    }

	/*
		Function: onUseDisc
	*/
	public function onUseDisc( node : Object ) : Void
	{
        debug( "FrusionClient.onUseDisc with playmode=" + this.gameDisc.playMode );
        debug( "FrusionClient.onUseDisc with playmode=" + this.gameDisc.playMode );
		var xmlReceived = new XML( node[1].toString());
        if( parseInt( String (xmlReceived.firstChild.attributes.s) , 10 ) == 1 )
        {
            this.canSaveScore = true;
        }
        this.dailyData = xmlReceived.firstChild.firstChild.toString();

		this.getUser();
	}


	/*
		Function: onGetGameDisc
	*/
	public function onGetGameDisc( received : Object) : Void
	{
		// got game disc
		this.gameDisc = received[1] ;

        if( Context.FRUSION_MULTIPLAYER_LOADER == this.gameDisc.playMode )
            debug( "--->multi on<---");
        else
            debug( "--->multi off<---");

        // identify
        _global.debug("------1");
        this.sendCommand( "c", new Array( new Pair("p�", "d�d�") ) );
        _global.debug("------2");
        this.sendCommand( "ab", new Array( new Pair("d", this.gameDisc.gameId) ) );
        _global.debug("------3");

		// ask for user info
		//this.getUser();
   	}


	/*
		Function: onGetUser
	*/
	public function onGetUser( received : Object ) : Void
	{
		// Got user
		this.user = received[1];
        debug("frusion.client.FrusionClient::GOT YOU:" + this.user);

		// Register Fruticard service
		this.frutiCard = new FrutiCard( this.gameDisc.swfName, this );
		this.frutiCard.addListener( this );

		// call Fruticard available slots
		this.frutiCard.listAvailableSlots( this.gameDisc.swfName );
	}


	/*
		Function: onChangeState
		Send from FrusionServer to tell that the state of the frusion has changed
	*/
	public function onChangeState( received : Object ) : Void
	{
		// save state
        debug("frusion.client.FrusionClient::onChangeState" )
        this.currentState = received[1];

		// if pause state encountered change state
		if( this.currentState == Context.FRUSION_PAUSE_ON_STATE )
			this.pauseStatus = true;
        else if( this.currentState == Context.FRUSION_PAUSE_OFF_STATE )
            this.pauseStatus = false;


		// Dispatch to callback

		//Callback (this.dispatchList[this.currentState]).execute();
		this.dispatchList[this.currentState].execute();
	}


    /*
        Function: onUpdateSize
        get size from server
    */
    /*
    public function onUpdateSize( received : Object ): Void
    {
        this.frusionX = received[1].x;
        this.frusionY = received[1].y;
        this.onUpdateSizeCallback.execute();
    }
    */


/*------------------------------------------------------------------------------------
 * Public methods incoming from FrutiCard
 *------------------------------------------------------------------------------------*/


	/*
		Function: onFrutiCardLoadSlot
		Callback from FrutiCard
	*/
	public function onFrutiCardSlotLoaded() : Void
	{
        _global.debug( "onFrutiCardSlotLoaded" );
        _global.debug( "------DISCTYPE" + this.gameDisc.discType );

		// Load process finished,
		// call the client method
		this.onReadyCallback.execute();
	}


/*------------------------------------------------------------------------------------
 * Private methods
 *------------------------------------------------------------------------------------*/


	 /*
		Function: getGameDisc
		Ask server for loaded game disc
	*/
	private function getGameDisc() : Void
	{
        this.sendRemote( Context.FRUSIONSERVER_GETGAMEDISC_METHOD );
	}


	/*
		Function: getUser
		Ask server for current user
	*/
	private function getUser() : Void
	{
        this.sendRemote( Context.FRUSIONSERVER_GETUSER_METHOD );
	}

    /*
        Function: getUserName
        Retrieve current user name.

        Note: this method can be used only after a getUser() call.
    */
    public function getUserName() : String
    {
        return this.user;
    }

/*------------------------------------------------------------------------------------
 * Intrinsic methods for AsBroadcaster
 *------------------------------------------------------------------------------------*/


	public function broadcastMessage(){}
	public function addListener(){}
	public function removeListener(){}


/*------------------------------------------------------------------------------------
 * Implementing CardiogramPatient
 *------------------------------------------------------------------------------------*/


    /*
        Function: monitorActivity

    */
    public function monitorActivity() : Void
    {
        this.sendRemote( Context.FRUSIONSERVER_PULSE_METHOD, null );
    }

    /*
        Function : manageCrisis

        What to do when a crisis situation arise...
    */
    public function manageCrisis() : Void {}


/*------------------------------------------------------------------------------------
 * Cardiogram activity management
 *------------------------------------------------------------------------------------*/


    /*
        Function: pulse

        Register pulse so we know the patient is still alive. This method is called by frusionClient
        and thus allow us to check it is still alive
    */
    public function pulse() : Void
    {
        this.cardiogram.pulse();
    }

}
