/*
	$Id$
*/


/*
	Class: frusion.client.multi.UIGameManager
	Simple class to store Game information for multiplayer games
*/
class frusion.client.multi.UIGameManager
{


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var gameId : String;
	public var players : Array;
	public var freeSlots : Number;
	public var totalSlots : Number;
	public var phase : Number;
	
	
/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: UIManager
		Constructor
	*/
	public function UIGameManager( gameId : String, players : Array, freeSlots : Number, phase : Number)
	{
		this.gameId = gameId;
		this.players = players;
		this.freeSlots = freeSlots;
		this.phase = phase;
		
		this.totalSlots = this.freeSlots + this.players.length;
	}
	
}
