/*
 *  $Id $
 */

import frusion.client.multi.UIGame;


/*
    Class: frusion.client.multi.UIPlayer
*/
class frusion.client.multi.UIPlayer
{
    public var name : String;
    public var status : Number;
    public var ranking : Number;
    public var game : UIGame;
    public var waitList : Array;


    public function UIPlayer( name : String, status : Number, game : UIGame, waitList : Array )
    {
        this.name = name;
        this.status = status;
        this.game = game;
        this.waitList = waitList;
        this.ranking = 0;
    }
}
 
