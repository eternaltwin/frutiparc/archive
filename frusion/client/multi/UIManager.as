/*
 *  $Id $    
 */


import frusion.Context;


/*
    Class: frusion.client.multi.UIManager
*/
class frusion.client.multi.UIManager
{



/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


    private var mc : MovieClip; 


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


    public function UIManager( mc : MovieClip )
    {       
        this.mc = mc;
        _global.debug( "frusion.client.multi.UIManager::UIManager=" + this.mc );
    }


    public function hide() : Void
    {
        this.mc._visible = false;
        this.mc._parent._visible = false;
    }

    
    public function initRoom() : Void
    {
        _global.debug( "frusion.client.multi.UIManager::initRoom" );
        this.mc.initRoom();
    }


    public function clearGame() : Void
    {
        this.mc.room.clearGame();
    }


    public function receiveMessage( msg : String ) : Void
    {
        this.mc.room.receiveMessage( msg );
    }
    

    public function setRoomList( list : Array ) : Void
    {
        this.mc.menu.setRoomList( list );
    }


    public function displayWaitList( list : Object ) : Void
    {
        this.mc.room.displayWaitList( list );
    }


    public function displayDoc( doc : XML ) : Void
    {
        this.mc.room.displayDoc( doc );
    }


    public function displayGameCreationPanel() : Void
    {
         this.mc.room.displayCreateGame();
    }

    public function hidePlayersPanel() : Void
    {
         this.mc.room.clearGame();
    }


    public function displayPlayersPanel( list : Array ) : Void
    {
        this.mc.room.joinGame(false, list);
    }

    public function displayCreatorPanel( list : Array  ) : Void
    {
        this.mc.room.joinGame(true, list);
    }

    public function refreshUserList( list : Array, pageIndex : Number, pageCount : Number  ) : Void
    {
        this.mc.room.setUserList( list, pageIndex, pageCount );
    }

    /*
        Function: displayError
    */
    public function displayError( msg : String) : Void
    {
        var obj : Object = new Object();
        obj.title="Erreur";
        obj.text=msg;
        obj.butList = new Array( {text:"ok"} );

        this.mc.displayError(obj);
    }


    /*
        Function: displayWarning
    */
    public function displayWarning( msg : String ) : Void
    {
        var obj : Object = new Object();
        obj.title="Pour info...";
        obj.text=msg;
        obj.butList = new Array( {text:"ok"} );

        this.mc.displayError(obj,0);
    }
    

}
