/*
	$Id$
*/


/*
	Class: frusion.client.multi.UIRoomManager
	Simple class to store room information
*/
class frusion.client.multi.UIRoomManager
{


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var roomId : String;
	public var roomMode : Number;
	public var userCount : Number;
	public var gameCount : Number;
    public var flActive  : Boolean;
	
	
/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: UIRoomManager
		Constructor
	*/
	public function GrapizUIRoomManager( roomId : String, roomMode : Number, userCount : Number, gameCount : Number, flActive : Boolean)
	{
		this.roomId = roomId;
		this.roomMode = roomMode;
		this.userCount = userCount;
		this.gameCount = gameCount;
        this.flActive  = flActive;
	}
	
}
