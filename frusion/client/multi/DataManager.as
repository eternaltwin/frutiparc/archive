/*
 *  $Id $    
 */


import frusion.client.multi.UIGameManager;
import frusion.client.multi.UIRoomManager;
import frusion.client.multi.UIPlayer;
import frusion.client.multi.UIGame;
import frusion.Context;


/*
    Class: frusion.client.multi.DataManager
*/
class frusion.client.multi.DataManager
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = true;


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


    public var list : Array;
    public var displayList : Array;
    public var roomList : Array;
    public var currentGame : String;
    public var displayCurrentGames : Boolean = true;
    public var displayNewGames : Boolean = true;
    public var displayPlayers : Boolean = true;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/
    
    
    private var isFirst : Boolean = true;
    private var games : Object;
    private var players : Object;
    private var virginGames : Array;
    private var gameCount : Number;
    private var playersCount : Number;
    private var currentGameCount : Number;
    private var maxList : Number;
    private var debugBox : box.Debug;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


    public function DataManager()
    {       
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::DataManager" ) : undefined;
        this.roomList = new Array();
        this.games = new Array();
        this.players = new Object();
        this.displayList = new Array();

        // define how many records we can display in the room 
        //this.maxList = 4;
        this.maxList = 20; // default value :)
    }



/*------------------------------------------------------------------------------------
 * Init Methods, called only once
 *------------------------------------------------------------------------------------*/


    /*
        Function: initRoomList
        
        Speaks for itself, na?
    */
    public function initRoomList( nodeTxt : String )
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::initRoomList" ) : undefined;
        this.roomList = new Array();
        var node = new XML(nodeTxt) ;
        node = node.firstChild;

        if( node.hasChildNodes() ) 
            node = node.firstChild;

        for(;node.nodeType>0;node=node.nextSibling)
        {
            var roomId    : String  = node.attributes.g;
            var name      : String  = node.firstChild.nodeValue;
            var roomMode  : Number  = parseInt( node.attributes.m,  10 );
            var userCount : Number  = parseInt( node.attributes.uc, 10 );
            var gameCount : Number  = parseInt( node.attributes.gc, 10 );
            var flActive  : Boolean = (parseInt( node.attributes.a, 10 ) == 1);
   
            this.roomList.push( {id:roomId, name:name, players:userCount, games:gameCount, flActive:flActive} );
        }
    }


    /*
        Function: getPlayersCount
        
        Speaks for itself, na?
    */
    public function getPlayersCount() : Number
    {   
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getPlayersCount" ) : undefined;
        return this.players.length;
    }

    
    /*
        Function: initGameList
        
        Speaks for itself, na?
    */
    public function initGameList( nodeTxt : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::setGameList" ) : undefined;

        this.games = new Array();
        var node = new XML(nodeTxt) ;
        node = node.firstChild;

        this.gameCount = parseInt( node.attributes.gc, 10 );
        if( this.gameCount < 1 ) return;
        
        this.playersCount = parseInt( node.attributes.uc, 10 );


        if( node.hasChildNodes() ) 
            node = node.firstChild;

        for(;node.nodeType>0;node=node.nextSibling)
        {
            var gameId : String = node.attributes.g;
            var players : String = node.attributes.uc;
            var freeSlots : Number = parseInt( node.attributes.f, 10);			
            var phase : Number = parseInt( node.attributes.p, 10);

            var poupoule : Array = players.split( ";");
            var l : Number = poupoule.length;
            for ( var i : Number = 0; i < l; i++ )
            {
                this.players[ poupoule[i] ] 
                    = new UIPlayer( poupoule[i], Context.NEW_GAME_SLOT_STATUS, new UIGame( gameId ), null );
            }                    
               
            this.games.push( new UIGameManager( gameId, poupoule, freeSlots, phase ) );	
        }
        this.updateList();
    }


    /*
        Function : initPlayerList

        Inits the list as it wll be displayed 
        when the player connects on a room for the first time.
        It represents the starting list we'll evolve at each diffuse signal
    */
    public function initPlayerList( nodeTxt : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::initPlayerList" ) : undefined;
        
        var arr: Array = new Array();
        var obj : Object = new Object();

        var node = new XML(nodeTxt) ;
        node = node.firstChild;

        if( node.hasChildNodes() ) 
            node = node.firstChild;

        for(;node.nodeType>0;node=node.nextSibling)
        {
            var playerId : String = node.attributes.u;
            var gameId : String = node.attributes.g;
            
            if( gameId == undefined )
            {
                this.DEBUG ? _global.debug ( "playerId =" + playerId  ) : undefined;
                this.players[ playerId ] = new UIPlayer( playerId, Context.STAND_ALONE_PLAYER_STATUS, null, null );
                this.playersCount++;
            }
        }
        this.updateList();
    }


    /*
        Function: rebuildList
    */
    public function rebuildList() : Void
    {
        this.updateList();
    }        


    /*
        Function: nukePlayer
    */
    public function nukePlayer( player : String ) : Void
    {
        delete this.players[ player ];
        this.updateList();
    }        


/*------------------------------------------------------------------------------------
 * Update list data methods
 *------------------------------------------------------------------------------------*/


    /*
        Function: addNewGame
        
        Speaks for itself, na?
    */
    public function addNewGame( nodeTxt : String ) : Void
    {            
        var node = new XML(nodeTxt) ;
        node = node.firstChild;
        var gameId : String = node.attributes.g;
        var players : String = node.attributes.uc;
        var freeSlots : Number = parseInt( node.attributes.f, 10);			
        var phase : Number = parseInt( node.attributes.p, 10);
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::addNewGame:" + gameId ) : undefined;
        
        var game : UIGameManager = new UIGameManager( gameId, players.split( ";"), freeSlots, phase );
        this.games.push( game );			
        
        // remove player from new players and update list
        this.DEBUG ? _global.debug ( "game.players[0]=" + game.players[0] ) : undefined;
        this.easyRemovePlayer( game.players[0] );
    }


    /*
        Function: setChallengerInfo
        
        Speaks for itself, na?
    */
    public function setChallengerInfo( nodeTxt : String ) : UIPlayer
    {
        var node = new XML(nodeTxt) ;
        var player : String = node.firstChild.attributes.u;
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::setChallengerInfo:" + player ) : undefined;
        var ranking : String = node.firstChild.attributes.s;
        var opponent : String = node.firstChild.attributes.o;
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::setChallengerInfo: opponent " + opponent ) : undefined;
        var tempList : String = node.firstChild.attributes.wl;
        //XXX
        if( tempList != "" || opponent != "")
        {
            var tempArray : Array = tempList.split(";");
            var waitList : Array = new Array();
            waitList.push( this.players[ opponent ])
            var l : Number = tempArray.length;
            if( tempList != "" )
            {
                for( var i : Number = 0; i < l ; i++ )
                {
                    this.DEBUG ? _global.debug ( "tempArray[ i ]=" + tempArray[ i ] ) : undefined;
                    waitList.push( this.players[ tempArray[ i ] ]);
                }
            }    
            
            this.players[ player ].waitList = waitList;
        }
        else if( tempList == "" && opponent == "" )
        {
            this.players[ player ].waitList = new Array();
        }
        
        this.players[ player ].ranking = ranking;
        
        return this.players[ player ];
    }


    /*
        Function: addNewPlayer
        
        Speaks for itself, na?
    */
    public function addNewPlayer( nodeTxt : String ) : Void
    {
        var node = new XML(nodeTxt) ;
        var player : String = node.firstChild.attributes.u;
        this.players[ player ] 
            = new UIPlayer( player, Context.STAND_ALONE_PLAYER_STATUS, 
                null, null );
                
        this.updateList();
    }


    /*
        Function: easyRemovePlayer
        
        Overloading in AS? what a joke!
    */
    public function easyRemovePlayer( usr : String ) : Void
    {
        ///!!!this.deletePlayer( usr );
        this.updateList();
    }


    /*
        Function: joinGame
        
        Speaks for itself, na?
    */
    public function joinGame( nodeTxt : String ) : Array
    {
        var node = new XML(nodeTxt) ;
        var gameId : String = node.firstChild.attributes.g;
        var usr : String = node.firstChild.attributes.u;

        // remove player from players' listnode.firstChild.attributes.u
        ///!!!this.deletePlayer( usr );

        // add player to the game
        var gameIndex : Number= this.getGameIndex(gameId);
        var uim : UIGameManager = UIGameManager (this.games[gameIndex]);
        uim.players.push( usr )
        this.games[gameIndex] = uim;
        
        // update display list
        this.updateList();

        // return players list to update panel
        return this.getAllPlayersFromGame( gameIndex );
    }


    /*
        Function: joinChallengeGame
        
        Speaks for itself, na?
    */
    public function joinChallengeGame( nodeTxt : String ) : Array
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::joinChallengeGame :" + usr ) : undefined;
        var node = new XML(nodeTxt) ;
        var gameId : String = node.firstChild.attributes.g;
        var usr : String = node.firstChild.attributes.u;

        // remove player from players' list
        ///!!!this.deletePlayer( usr );

        // add player to the game
        var gameIndex : Number= this.getGameIndex(gameId);
        var uim : UIGameManager = UIGameManager (this.games[gameIndex]);
        uim.players.push( usr );
        uim.totalSlots++;
        this.games[gameIndex] = uim;
        
        // update display list
        this.updateList();

        // return players list to update panel
        return this.getAllPlayersFromGame( gameIndex );
    }


    /*
        Function: iJoinGame
        
        Current player joins a game
    */
    public function iJoinGame( nodeTxt : String, usr : String ) : Array
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::iJoinGame:" + usr ) : undefined;
        
        var node = new XML(nodeTxt) ;
        var gameId : String = node.firstChild.attributes.g;
        this.currentGame = gameId;
        ///!!!this.deletePlayer( usr );
        this.updateList();
        return this.getAllPlayersFromGame( this.getGameIndex(gameId) );
    }


    /*
        Function: getMyPlayerList

        returns player's list for current player's game
    */
    public function getMyPlayersList( usr : String, playerNumber : Number ) : Array
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getMyPlayersList:" + usr ) : undefined;
        return this.getMyList( this.isGamePlayer( usr ), playerNumber, usr ); 
    }


    /*
        Function: getPlayerList
        
        return the list of player withour given user
    */
    public function getPlayerList( usr : String, playerNumber : Number ) : Array
    {
        return this.getPlayerListFromGame( this.isGamePlayer( usr ), usr, playerNumber ); 
    }
    

    /*
        Function: iLeaveGame 

        Remove current player from a game
    */
    public function iLeaveGame( usr : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::leaveGame:" + usr ) : undefined;
        // delete player from game
        this.removePlayerFromGame( this.isGamePlayer( usr ), usr ); 
        this.updateList();
    }


    /*
        Function: leaveGame 
    */
    public function leaveGame( usr : String, currentUser : String ) : Boolean
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::leaveGame:" + usr ) : undefined;

        // test if user is game creator
        var gameIndex : Number = this.isGameCreator( usr );
        if( gameIndex != -1 )
        {
            // user is game creator
            // put remaining players in list
            this.removeAllPlayersFromGame( gameIndex, usr );
            
            // remove game
            this.games.splice(gameIndex, 1);
            this.updateList();
            return true;

        }
        else
        {
            // user is not game creator
            // delete player from game
            this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::leaveGame - not creator:" + usr ) : undefined;
            this.removePlayerFromGame( this.isGamePlayer( usr ), usr ); 
        }
        
        // update display list
        this.updateList();

        // return true if the deleted game was a game where currentPlayer was a player ;)
        return ( gameIndex == this.currentGame );
    }


    /*
        Function: foreignCreatorLeavesGame 
    */
    public function foreignCreatorLeavesGame( usr : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::foreignCreatorLeavesGame:" + usr ) : undefined;

        // test if user is game creator
        var gameIndex : Number = this.isGameCreator( usr );
        if( gameIndex != -1 )
        {
            // user is game creator
            // put remaining players in list
            this.removeAllPlayersFromGame( gameIndex, usr );
            
            // remove game
            this.games.splice(gameIndex, 1);

            // put creator back in the playerlist
            //this.players.push( { player: usr, game: undefined } );
            this.players[ usr ]= new UIPlayer( usr, Context.STAND_ALONE_PLAYER_STATUS, null, null );
 
            // update display list
            this.updateList();
        }
    }


    /*
        Function: iLeaveMyGame
    */
    public function iLeaveMyGame( usr : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::iLeaveMyGame: " + usr ) : undefined;

        // test if user is game creator
        var gameIndex : Number = this.isGameCreator( usr );
        if( gameIndex != -1 )
        {
            // user is game creator
            // put remaining players in list
            this.removeAllPlayersFromGame( gameIndex, usr );
            
            // remove game
            this.games.splice(gameIndex, 1);

            // put creator back in the playerlist
            //this.players.push( { player: usr, game: undefined } );
            this.players[usr] = new UIPlayer( usr, Context.STAND_ALONE_PLAYER_STATUS, null, null );
            //this.playersLength++;
 
            // update display list
            this.updateList();
        }
    }


    /*
        Function: playerLeavesMyGame
    */
    public function playerLeavesMyGame( player : String, currentCreator : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::playerLeavesMyGame:" + player ) : undefined;

        // test if user is game creator
        var gameIndex : Number = this.isGameCreator( currentCreator );
        if( gameIndex != -1 )
        {
            // user is game creator
            // put remaining players in list
            this.removePlayerFromGame( gameIndex, player );

            // update display list
            this.updateList();
        }
    }


    /*
        Function: playerLeavesGame
    */
    public function playerLeavesGame( player : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::playerLeavesGame:" + player ) : undefined;

        // test if user is game creator
        var gameIndex : Number = this.isGamePlayer( player );
        if( gameIndex != -1 )
        {
            // user is game creator
            // put remaining players in list
            this.removePlayerFromGame( gameIndex, player );

            // update display list
            this.updateList();
        }
    }


    /*
        Function: playerWasInGame

        *Determines if player was in the game of the game creator who left the game*
    */
    public function playerWasInGame( creator : String, player : String ) : Boolean
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::playerWasInGame:" + player ) : undefined;
        var gameIndex : Number = this.isGameCreator( creator );
        if( gameIndex != -1 )
            return wasInGame( gameIndex, player );

        return false;
    }
    

    /*
        Function: wasCreator

        *Determines wether user is a creator*
    */
    public function wasCreator( user : String ) : Boolean
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::wasCreator:" + user ) : undefined;
        return ( this.isGameCreator( user ) != -1 );
    }


    /*
        Function: leaveMyGame

        *Determines if current player is leaving his own game*
    */
    public function leaveMyGame( creator : String ) : Boolean
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::leaveMyGame: " + creator ) : undefined;
        return ( this.isGameCreator( creator ) != -1 )
    }

    
    /*
        Function: removePlayer
        
        Speaks for itself, na?
    */
    public function removePlayer( usr : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::removePlayer:" + usr ) : undefined;

        // remove player from player list
        ///!!!this.deletePlayer( usr );
        this.updateList();
    }


    /*
        Function: setPageList
        
        Speaks for itself, na?
    */
    public function setPageList( pageIndex : Number ) : Number
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::setPageList"  ) : undefined;
        
        // calculate number of pages to display
        var max : Number = Math.ceil( this.list.length / this.maxList );

        // creating display list
        var tempList : Array = new Array();
        var startIndex : Number = pageIndex * this.maxList;
        
        if( max == pageIndex +1 )
            var endIndex : Number = this.list.length;
        else if( this.list.length > startIndex +  this.maxList )
            var endIndex : Number = startIndex +  this.maxList;
        else
            var endIndex : Number = startIndex +  this.list.length;
        
        for( var i : Number = startIndex; i < endIndex; i++ )
            tempList.push ( this.list[i] );


        // swap lists
        this.displayList = tempList;

        return max;
    }
    
    
    /*
        Function: createEmptyGame
    */
    public function createEmptyGame( gameCreator : String, playerNumber : Number) : Array
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::createEmptyGame: - gameCreator :" + gameCreator ) : undefined;
        var maraie : Array = new Array();
        this.players[ gameCreator ].status = Context.NEW_GAME_SLOT_STATUS; 
        maraie.push( this.createUser( this.players[ gameCreator ], 1, Context.USER_TYPE ) );
        for( var i : Number = 1; i < playerNumber; i++ )
            maraie.push( this.createUser( null, 1, Context.EMPTY_TYPE ) );

        return maraie;        
    }


    /*
        Function: startGame
    */
    public function startGame( gameId : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::startGame"  ) : undefined;
        // change game status
        var gameIndex : Number= this.getGameIndex(gameId);
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::gameIndex=" + gameIndex ) : undefined;
        var uim : UIGameManager = UIGameManager ( this.games[gameIndex] );
        uim.phase = 1;
        this.games[gameIndex] = uim;
        this.updateList();
    }



    /*
        Function: closeGame
    */
    public function closeGame( gameId : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::closeGame"  ) : undefined;
    }


/*------------------------------------------------------------------------------------
 * Create actual displayed list 
 *------------------------------------------------------------------------------------*/


    /*
        Function: updateList
        
        Do the washing up before display.
    */
    public function updateList() : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::updateList"  ) : undefined;
        this.list = new Array();

        // display recent created games
        if( this.displayNewGames )
        {
            // display games list
            var gamel : Number = this.games.length;
            for( var i : Number = 0; i < gamel; i++ )
            {
                var uim : UIGameManager = UIGameManager (this.games[i]);
                if( uim.phase == 0)
                {
                    var gameid : String = uim.gameId;
                    var temp = uim.players.length;
        
                    // manage current players
                    for( var j : Number = 0; j < temp; j++ )
                    {
                        //this.DEBUG ? _global.debug ( "uim.players[j]=" + uim.players[j] );
                        this.list.push( this.createNewGameSlot( gameid, uim.players[j], j==0 ? uim.totalSlots : 0 ) );
                    }    
                    
                    this.currentGameCount = uim.freeSlots + 1 - temp;
                    for( var j : Number = 0; j < this.currentGameCount; j++ )
                        this.list.push( this.createNewGameFreeSlot( gameid) );
                }                        
            }
        }


        // display started games
        if( this.displayCurrentGames )
        {
            var gamel : Number = this.games.length;
            this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::Games count =" + gamel ) : undefined;
            for( var i : Number = 0; i < gamel; i++ )
            {
                var uim : UIGameManager = UIGameManager (this.games[i]);
                //this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::uim.phase=" + uim.phase );
                if( uim.phase >= 1 )
                {
                    var gameid : String = uim.gameId;
                    this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::create gameId=" + gameid ) : undefined;
                    var temp = uim.players.length;
                    //this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::create uim.players.length=" + temp );
        
                    // manage current players
                    for( var j : Number = 0; j < temp; j++ )
                        this.list.push( this.createStartedGameSlot( gameid, uim.players[j], j==0 ? uim.totalSlots : 0 ) );
                }                        
            }
        }

        // display standalone user list
        if( this.displayPlayers )
        {
            for( var n in this.players )
                if( this.players[n].status == Context.STAND_ALONE_PLAYER_STATUS )
                    this.list.push( this.createPlayerSlot( this.players[n]) );
        }

    } 


    /*
        Function: updateChallengeList
        
        Do the washing up before display.
    */
    public function updateChallengeList() : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::updateList"  ) : undefined;
        this.list = new Array();


        // display started games
        if( this.displayCurrentGames )
        {
            // display games list
            var gamel : Number = this.games.length;
            var startCount : Number = 0;
            for( var i : Number = 0; i < gamel; i++ )
            {
                var uim : UIGameManager = UIGameManager (this.games[i]);
                if( uim.phase >= 1 ) startCount++
            }
            this.list.push( this.createStartedGamesCount( startCount * 2 ) );
        }
        else
        {
            var gamel : Number = this.games.length;
            this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::Games count =" + gamel ) : undefined;
            for( var i : Number = 0; i < gamel; i++ )
            {
                var uim : UIGameManager = UIGameManager (this.games[i]);
                if( uim.phase >= 1 )
                {
                    var gameid : String = uim.gameId;
                    this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::create gameId=" + gameid ) : undefined;
                    var temp = uim.players.length;
        
                    // manage current players
                    for( var j : Number = 0; j < temp; j++ )
                        this.list.push( this.createStartedGameSlot( gameid, uim.players[j], j==0 ? uim.totalSlots : 0 ) );
                }                        
            }
        }
        


        // display recent created games
        if( this.displayNewGames )
        {
            // display games list
            var gamel2 : Number = this.games.length;
            var newCount : Number = 0;
            for( var i : Number = 0; i < gamel2; i++ )
            {
                var uim : UIGameManager = UIGameManager (this.games[i]);
                if( uim.phase == 0 ) newCount++
            }
            this.list.push( this.createNewGamesCount( newCount * 2 ) );
        }
        else
        {
            // display games list
            var gamel : Number = this.games.length;
            for( var i : Number = 0; i < gamel; i++ )
            {
                var uim : UIGameManager = UIGameManager (this.games[i]);
                if( uim.phase == 0)
                {
                    var gameid : String = uim.gameId;
                    var temp = uim.players.length;
        
                    // manage current players
                    for( var j : Number = 0; j < temp; j++ )
                    {
                        //this.DEBUG ? _global.debug ( "uim.players[j]=" + uim.players[j] );
                        this.list.push( this.createNewGameSlot( gameid, uim.players[j], j==0 ? uim.totalSlots : 0 ) );
                    }    
                    
                    this.currentGameCount = uim.freeSlots + 1 - temp;
                    for( var j : Number = 0; j < this.currentGameCount; j++ )
                        this.list.push( this.createNewGameFreeSlot( gameid) );
                }                        
            }
        }
        


        // display standalone user list
        if( this.displayPlayers )
        {
            for( var n in this.players )
                if( this.players[n].status == Context.STAND_ALONE_PLAYER_STATUS )
                    this.list.push( this.createPlayerSlot( this.players[n]) );
        }

    } 


/*------------------------------------------------------------------------------------
 * Utility methods
 *------------------------------------------------------------------------------------*/


    /*
        Function: createStartedGameSlot
    */
    private function createStartedGameSlot( gameId : String, text : String, link : Number ) : Object
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::createStartedGameSlot=" + gameId ) : undefined;
        var p: UIPlayer = this.players[text];
        p.game = new UIGame( gameId );
        p.status = Context.STARTED_GAME_STATUS;
        return this.createUser( this.players[text], link, Context.USER_TYPE );
    }


    /*
        Function: createStartedGamesCount
    */
    private function createStartedGamesCount( count : Number) : Object
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::createStartedGamesCount=" + count ) : undefined;
        var p: UIPlayer = new UIPlayer( count + " qui jouent" , Context.STARTED_GAME_STATUS, null, null );
        return this.createUser( p, 1, Context.USER_TYPE );
    }


    /*
        Function: createNewGamesCount
    */
    private function createNewGamesCount( count : Number) : Object
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::createStartedGamesCount=" + count ) : undefined;
        var p: UIPlayer = new UIPlayer( count + " en attente" , Context.NEW_GAME_SLOT_STATUS, null, null );
        return this.createUser( p, 1, Context.USER_TYPE );
    }


    /*
        Function: createNewGameSlot
    */
    private function createNewGameSlot( gameId : String, text : String, link : Number ) : Object
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::createNewGameSlot - text=" + text ) : undefined;
        this.DEBUG ? _global.debug ( "this.players[text]=" + this.players[text] ) : undefined;
        var p: UIPlayer = this.players[text];
        p.game = new UIGame( gameId );
        p.status = Context.NEW_GAME_SLOT_STATUS;
        this.players[text] = p;
        return this.createUser( this.players[text], link, Context.USER_TYPE );
    }


    /*
        Function: createNewGameFreeSlot
    */
    private function createNewGameFreeSlot( gameId : String ) : Object
    {
        return { game : {gameId : gameId }, link: 0, type: Context.JOIN_TYPE };
    }


    /*
        Function: createPlayerSlot
    */
    private function createPlayerSlot( player : UIPlayer ) : Object
    {
        return this.createUser( player, 1, Context.USER_TYPE );
    }


    /*
        Function: createUser
    */
    private function createUser( user : UIPlayer, link : Number, type : Number ) : Object
    {
        return { user: user, link: link, type: type };
    }


    /*
        Function: deletePlayer
    */
    private function deletePlayer( usr : String ) : Void {} // seems it is not used anywhere...


    /*
        Function: isGamePlayer

        Returns the index of the game, otherwise returns -1
    */
    private function isGamePlayer( usr : String ) : Number
    {
        var l : Number= this.games.length;
        for( var i : Number = 0; i < l; i++)
        {
            var uim : UIGameManager = UIGameManager ( this.games[i] );
            var templ : Number = uim.players.length;
            for( var j : Number = 0; j < templ; j++ )
                if( usr == uim.players[j] )
                    return i;
        }
        
        return -1;
    }


    /*
        Function: getGameIndex

        Returns the index of the game, otherwise returns -1
    */
    private function getGameIndex( gameId : String ) : Number
    {
        var l : Number= this.games.length;
        for( var i : Number = 0; i < l; i++)
            if( gameId == UIGameManager ( this.games[i] ).gameId )
                return i;

        return -1;
    }


    /*
        Function: isGameCreator

        Returns the index of the game, otherwise returns -1
    */
    private function isGameCreator( usr : String ) : Number
    {
        var l : Number= this.games.length;
        for( var i : Number = 0; i < l; i++)
            if( usr == UIGameManager ( this.games[i] ).players[0] )
                return i;

        return -1;
    }


    /*
        Function: removeAllPlayersFromGame

        Removes all players except specified from a given game 
        and put them in the players list
    */
    private function removeAllPlayersFromGame( gameIndex : Number, gameCreator : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::removeAllPlayersFromGame - gameCreator=" + gameCreator ) : undefined;
        var uim2 : UIGameManager = UIGameManager ( this.games[gameIndex] );
        var temp : Number = uim2.players.length;
        for( var j : Number = 0; j < temp; j++ )
        {
            if( uim2.players[j] != gameCreator )
            {
                this.players[ uim2.players[j] ].status = Context.STAND_ALONE_PLAYER_STATUS;
                this.players[ uim2.players[j] ].game = null;
            }   
        }
    }


    /*
        Function: removeAllPlayersFromGame

        Removes all players except specified from a given game
    */
    private function removePlayerFromGame( gameIndex : Number, player : String ) : Void
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::removePlayerFromGame=" + player ) : undefined;
        var uim2 : UIGameManager = UIGameManager ( this.games[gameIndex] );
        var temp : Number = uim2.players.length;
        var index : Number = 0;
        for( var j : Number = 0; j < temp; j++ )
        {
            if( uim2.players[j] == player )
            {
                index = j;
                break;
            }    
        }
        this.games[gameIndex].players.splice( index, 1 );

        // put player back to list of alone players
        this.players[ player ].status = Context.STAND_ALONE_PLAYER_STATUS;
        this.players[ player ].game = null;
    }


    /*
        Function: getAllPlayersFromGame

        Get the player list from given game 
    */
    private function getAllPlayersFromGame( gameIndex : Number ) : Array
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getAllPlayersFromGame : " + gameIndex ) : undefined;
        var uim2 : UIGameManager = UIGameManager ( this.games[gameIndex] );
        var temp : Number = uim2.players.length;
        var values : Array = new Array;
        for( var j : Number = 0; j < temp; j++ )
        {   
            this.DEBUG ? _global.debug ( "got uim2.players[j]=" + uim2.players[j] ) : undefined; 
            values.push( this.createUser( this.players[ uim2.players[j] ], 1, Context.USER_TYPE ) );
        }          
        
        return values;
    }


    /*
        Function: getPlayerListFromGame

        Get the player list from given game without given user
    */
    private function getPlayerListFromGame( gameIndex : Number, player : String , playerNumber : Number ) : Array
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getPlayerListFromGame=" + player ) : undefined;
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getPlayerListFromGame - gameIndex =" + gameIndex ) : undefined;
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getPlayerListFromGame - playerNumber=" + playerNumber ) : undefined;
        var uim2 : UIGameManager = UIGameManager ( this.games[gameIndex] );
        var temp : Number = uim2.players.length;
        var index : Number = 0;
        var values : Array = new Array;
        for( var j : Number = 0; j < temp; j++ )
            if( uim2.players[j] != player )
                values.push( this.createUser( this.players[ uim2.players[j] ], 1, Context.USER_TYPE ) );


        // add remaining empty slots
        if( values.length < playerNumber )
        {
            var newL : Number = playerNumber - values.length;
            for( var j : Number = 0; j < newL; j++ )
                values.push( this.createUser( null, 1, Context.EMPTY_TYPE ) );
        }

        return values;
    }


    /*
        Function: getMyList

        Get the current player list from given game without given user
    */
    private function getMyList( gameIndex : Number, playerNumber : Number, me : String ) : Array
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getMyList : " + me) : undefined;
        var uim2 : UIGameManager = UIGameManager ( this.games[gameIndex] );
        var temp : Number = uim2.players.length;
        var index : Number = 0;
        var values : Array = new Array;
        for( var j : Number = 0; j < temp; j++ )
            values.push( this.createUser( this.players[ uim2.players[j] ], 1, Context.USER_TYPE ) );

        // add remaining empty slots
        if( values.length < playerNumber )
        {
            var newL : Number = playerNumber - values.length;
            for( var j : Number = 0; j < newL; j++ )
                values.push( this.createUser( null, 1, Context.EMPTY_TYPE ) );
        }
        
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::getMyList" + values.join() ) : undefined;
        return values;
    }


    /*
        Function: wasInGame
    */
    private function wasInGame( gameIndex : Number, player : String ) : Boolean
    {
        this.DEBUG ? _global.debug ( "frusion.client.multi.DataManager::wasInGame number=" + gameIndex ) : undefined;
        var players : Array = UIGameManager ( this.games[gameIndex] ).players;
        var temp : Number = players.length;
        for( var j : Number = 0; j < temp; j++ )
            if( players[j] == player )
                   return true;

        return false;
    }

 }
