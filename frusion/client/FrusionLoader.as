/*
	$Id: FrusionLoader.as,v 1.1 2003/11/13 16:04:32 fnicaise Exp $
*/


import frusion.util.GameFileLoader;
import frusion.util.AnimFileLoader;
import frusion.util.SkinFileLoader;
import frusion.gamedisc.GameDisc;
import frusion.gfx.LoadingBar;
import frusion.gfx.FrusionAnim;
import ext.util.Callback;
import frusion.Context;
import frusion.security.Citizen;

/*
	Class: frusion.client.FrusionLoader
	
	The FrusionLoader class is responsible for loading the frusion logo anim and the game
*/
class frusion.client.FrusionLoader 
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = false;
    public var LOADING_DEBUG : Boolean = false;   

/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


	private var gameReadyState : Boolean;
	private var animLoadingCompleteState : Boolean;
	private var gameLoadingCompleteState : Boolean;
	private var animPlayingCompleteState : Boolean;
    private var gameInitState : Boolean;
    
	private var gameRunningState : Boolean;

	//private var gameDisc : GameDisc;
	private var gameDisc : Object;
	private var mc : MovieClip;

	private var animFileLoader : AnimFileLoader;
	private var gameFileLoader : GameFileLoader;

  	private var animDepth : Number;
	private var loadDepth : Number;
	private var gameDepth : Number;

	private var frusionAnim: FrusionAnim;

    private var encounteredBug : String;
    private var debugText : String;
    private var baseURL : String;

    private var _citizen : Citizen;


/*------------------------------------------------------------------------------------
 * Cleanup methods
 *------------------------------------------------------------------------------------*/


    /*
        Function : finalize
        final cleanup
    */
    public function finalize() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::finalize" ) : undefined;
        
        // call transitory just in case... ;)
        this.transitoryFinalize();

        // finish cleanup
        this.gameDisc.finalize();
        delete this.gameDisc;
        this.gameDisc = null;

        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::transitoryfinalize : game " + this.mc.game ) : undefined;

        this.mc.game.removeMovieClip();
        delete this.mc.game;
        this.mc.game = null;

        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::transitoryfinalize : mc " + this.mc ) : undefined;

        this.mc.removeMovieClip();
        delete this.mc;
        this.mc = null;
        
        delete this.encounteredBug;
        this.mc = null;

        delete this.debugText;
        this.debugText = null;

        delete this.baseURL;
        this.debugText = null;
    }


    /*
        transitory cleanup before game starts
    */
    public function transitoryFinalize() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::transitoryfinalize" ) : undefined;
    
		this.animFileLoader.removeListener(this);
        this.animFileLoader = null;
        delete this.animFileLoader;
        
		this.gameFileLoader.removeListener(this);
        this.gameFileLoader = null;
        delete this.animFileLoader;
        
        this.frusionAnim = null;
        delete this.frusionAnim;
        
   		this.mc.anim_init.removeMovieClip();
        this.mc.anim_init = null;
        delete this.mc.anim_init;
    }

    
/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function : FrusionLoader
		Constructor

		Parameters:
			- mc : MovieClip - context for newly created movie clips ( game and anim )
			- gameDisc :  GameDisc - current game
	*/
	public function FrusionLoader( baseURL : String, mc : MovieClip )
	{
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::FrusionLoader" ) : undefined;
        
        this.debugText ="";
    	this.gameReadyState = false;
    	this.animLoadingCompleteState = false;
    	this.gameLoadingCompleteState = false;
    	this.animPlayingCompleteState = false;
        this.gameInitState = false;        
		this.mc = mc;
		this.animDepth = 10;
		this.gameDepth = 5;
		this.loadDepth = 20;
        
        this.baseURL = baseURL;
        
        // ask for permission before launching game
        _citizen = new Citizen( new Callback( this, _startLoading ) );
        _citizen.getPermission();     
	}


    /*
        Citizen tells it's ok to contine
    */
    private function _startLoading() : Void
    {
        //this.gameDisc = null;
        this.gameDisc = _citizen.getGreenCard();
        _global.debug( "got greencard from citizen: " + _citizen.getGreenCard().swfName );
        _global.debug( "got greencard from citizen: " + this.gameDisc.swfName );

		// creating new frusion anim loader
        // XXX add size to animFileLoader
        this.mc.createEmptyMovieClip( Context.ANIM_INIT, this.animDepth );
		this.animFileLoader = new AnimFileLoader( baseURL + Context.ANIMFRUSION_SWF, undefined );
		this.animFileLoader.addListener( this );
		this.frusionAnim = new FrusionAnim( this.mc.anim_init, this.gameDisc.width, this.gameDisc.height );
		this.animFileLoader.loadClip( this.mc.anim_init );
    }


    /*
        Function: loadGame
    */
    public function loadGame() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::loadGame" ) : undefined;

		// creating new game loader
	    this.mc.createEmptyMovieClip( Context.GAME, this.gameDepth);
		this.mc.game._visible = false;		
		this.gameFileLoader = new GameFileLoader( baseURL + gameDisc.swfId, undefined );
        
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::loadGame : this.mc.game._currentframe=" + this.mc.game._currentframe) : undefined;
        
        // XXX put size test again
		this.gameFileLoader.addListener(this);
		this.gameFileLoader.loadClip( this.mc.game );
    }
    

/*------------------------------------------------------------------------------------
 * Public callback methods
 *------------------------------------------------------------------------------------*/

    
    /*
        Function: onAnimLoadError
        CALLABCK. callaed when it is impossible to load the file
    */
    public function onAnimLoadError() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onAnimLoadError" ) : undefined;
        this.stopLaunchingProcess( Context.ERROR_MISSING_ANIM_SWF );
    }


    /*
        Function: onGameFileNotFoundError
    */
    public function onGameFileNotFoundError() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onGameFileNotFoundError" ) : undefined;
        this.stopLaunchingProcess( Context.ERROR_MISSING_GAME_SWF );
    }


    /*
        Function: onGameFalseSizeError
    */
    public function onGameFalseSizeError() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onGameFalseSizeError" ) : undefined;
        this.stopLaunchingProcess( Context.ERROR_FALSE_SIZE_GAME_SWF );
    }


	/*
		Function: onGameLoadStart
		CALLBACK. When game starts loading, creates loading bar
	*/
	public function onGameLoadStart() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onGameLoadStart" ) : undefined;
        this.mc.game.stop();
	}


	/*
		Function: onGameLoadProgress
		CALLBACK. Update current progressbar
	*/
	public function onGameLoadProgress() : Void
	{
        ( this.DEBUG && this.LOADING_DEBUG )
            ? _global.debug("frusion.client.FrusionLoader::onGameLoadProgress : percent=" + this.gameFileLoader.percent ) : undefined;
            
		this.frusionAnim.updateBar( this.gameFileLoader.percent );
	}


	/*
		Function: onGameLoadInit
		CALLBACK. update state
	*/
	public function onGameLoadInit() : Void
	{
        // add a flag to game in the first frame 
        // so you can test if you are in the frusion :)
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onGameLoadInit" ) : undefined;
        this.mc.game.inFrusion = true;
    }


	/*
		Function: onGameLoadComplete
		CALLBACK. update state
	*/
	public function onGameLoadComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onGameLoadComplete" ) : undefined;
        this.mc.game.loader = this;
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onGameLoadComplete game" + this.mc.game ) : undefined;
		this.frusionAnim.updateBar( 100 );
		this.gameLoadingCompleteState = true;
        this.tryToLaunchGame();
	}


    public function onGameReady() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onGameReady" ) : undefined;
		this.gameReadyState = true;
		this.tryToLaunchGame();
    }


	/*
		Function: onAnimLoadComplete
		CALLBACK. When animation is loaded, update gfx
	*/
	public function onAnimLoadComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::onAnimLoadComplete" ) : undefined;
        this.mc.anim_init.loader = this;
        this.frusionAnim.update();
		this.animFileLoader.removeListener(this);
		this.animFileLoader = null;
        this.loadGame();
		this.animLoadingCompleteState = true;
	}


	/*
		Function: animComplete
		Animation complete
	*/
	public function animComplete() : Void
	{
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::animComplete" ) : undefined;
		this.animPlayingCompleteState = true;
		this.tryToLaunchGame();
	}


	/*
		Function: clean
		
		*Remove movie clips of no use.*
	*/
    public function clean() : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::clean" ) : undefined;
        this.finalize();
        this.mc.game.removeMovieClip();
    }


/*------------------------------------------------------------------------------------
 * Private methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: tryToLaunchGame
		Verifies states and launch game is all conditions are met.
	*/
	private function tryToLaunchGame() : Void{ _global.debug("LHERITAGE EST MERDIQUE!!!!!!!!!!!!!!!" );  }

    
    /*
        Function: stopLaunchingProcess
        Handle all happened errors
    */
    private function stopLaunchingProcess( errorMessage : String ) : Void
    {
        this.DEBUG ? _global.debug("frusion.client.FrusionLoader::stopLaunchingProcess" ) : undefined;
        this.encounteredBug = errorMessage;

        var lc : LocalConnection = new LocalConnection();
        lc.send( Context.FRUSION_SERVER_LOCAL_CONNECTION, Context.FRUSIONSERVER_FATALERROR_METHOD, this.encounteredBug );
        lc = null;
        delete lc;

        // do some cleanup before going
        this.finalize();
    }


}   

// EOF
