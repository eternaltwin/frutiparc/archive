
import frusion.client.FrusionClient;
import ext.util.MTUnserializer ;
import ext.util.MTSerialization ;
import ext.util.Pair;
import frusion.Context;

/*
	Class: frusion.service.FrutiCard
	Fruticard Service handler
*/
class frusion.service.FrutiCard
{


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var gameName : String;
	public var slots : Array;
	public var currentSlotData : Array;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


	private var frusionClient : FrusionClient;
	private var callbackList : Object;
	private var commandList : Object;
	private var loadCount : Number = 0;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: FrutiCard
		Constructor
	*/
	public function FrutiCard( gameName : String, frusionClient : FrusionClient )
	{
		// Get game name
		this.gameName = gameName;

		// Prepare broadcasting interface
		AsBroadcaster.initialize(this);


		// Add listener to current service
		this.frusionClient = frusionClient ;
		this.frusionClient.addListener(this);

		// Registering callback list
		this.callbackList = new Object();
		this.callbackList.ea = "onGetPublicSlot";
		this.callbackList.eb = "onListAvailableSlots";
		this.callbackList.ec = "onLoadSlot";
		this.callbackList.ed = "onUpdateSlot";
		this.callbackList.ee = "onClearSlot";

        /*
		this.callbackList.onGetPublicSlot		= "ea";
		this.callbackList.onListAvailableSlots 	= "eb";
		this.callbackList.onLoadSlot 			= "ec";
		this.callbackList.onUpdateSlot 			= "ed";
		this.callbackList.onClearSlot			= "ee";
        */
		this.frusionClient.registerCallbackList( callbackList );


		// creating appropriate commands
		this.commandList = new Object();
		this.commandList.getPublicSlot		= "ea";
		this.commandList.listAvailableSlots = "eb";
		this.commandList.loadSlot 			= "ec";
		this.commandList.updateSlot			= "ed";
		this.commandList.clearSlot 			= "ee";

		this.slots = new Array();
		this.currentSlotData = new Array();

	}


/*------------------------------------------------------------------------------------
 * Public methods calling FrusionClient
 *------------------------------------------------------------------------------------*/


	/*
		Function: getPublicSlot
		Retrieve the public slot of specified user

		Parameters:
		slotOwner : String
	*/
	public function getPublicSlot( slotOwner : String ) : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.getPublicSlot,
				new Array(
					new Pair( "u", slotOwner ),
					new Pair( "g", this.gameName ) ) );
	}



	/*
		Function: listAvailableSlots
		This command returns a list of slot slots available for specified game
	*/
	public function listAvailableSlots() : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.listAvailableSlots,
				new Array(
					new Pair( "g", this.gameName ) ) );
	}


	/*
		Function: loadSlot
		Retrieve specified slot (create new one if not found)

		Parameters:
		slotId : Number : slot Id
	*/
	public function loadSlot( slotId : Number ) : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.loadSlot,
				new Array(
					new Pair( "s", slotId ),
					new Pair( "g", this.gameName ) ) );
	}


	/*
		Function: updateSlot
		Update slot content

		Parameters:
		slotId : Number : slot Id
	*/
	public function updateSlot( slotId : Number, data : Object ) : Void
	{
	  	//_global.debug("updateSlot: serialized="+MTSerialization.serialize(data)) ;
        var datain = MTSerialization.serialize(data);
        if( datain != this.currentSlotData[slotId] )
        {
            this.currentSlotData[slotId] = datain;
    		this.frusionClient.sendCommandWithText(
    				this.commandList.updateSlot,
    				new Array(
    					new Pair( "s", slotId ),
    					new Pair( "g", this.gameName ) ),
    			  datain );
        }
	}


	/*
		Function: clearSlot
		Delete a slot

		Parameters:
		slotId : Number : slot Id
	*/
	public function clearSlot( slotId : Number ) : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.clearSlot,
				new Array(
					new Pair( "s", slotId ),
					new Pair( "g", this.gameName ) ) );

        this.currentSlotData[ slotId ] = null;
	}


/*------------------------------------------------------------------------------------
 * Public callback methods from FrusionClient
 *------------------------------------------------------------------------------------*/


	/*
		Function: onListAvailableSlots
	*/
	public function onListAvailableSlots( nodeTxt : String ) : Void
	{
		var node = new XML(nodeTxt) ;
		var str : String = node.firstChild.firstChild.nodeValue;
		var splitString : Array =  str.split(":");

		// If there is no slot, node content is undefined
		if ( str == undefined )
		  this.loadCount = 0 ;
		else
		  this.loadCount = splitString.length;

		_global.debug("-------->frusion.service.Fruticard::onListAvailableSlots: str="+nodeTxt) ;
    	/*
		_global.debug("onListAvailableSlots: splitString="+splitString) ;
		_global.debug("onListAvailableSlots: count="+loadCount) ;
		*/

		// case where we didn't get anything, go next step
		if( this.loadCount == 0 )
			this.broadcastMessage( Context.FRUTICARD_SLOTLOADED_BROADCAST );
		else
		{
			// load all found slots
			for( var i : Number = 0; i < loadCount; i++ )
				this.loadSlot( splitString[i] );
		}
	}


	/*
		Function: onLoadSlot
	*/
	public function onLoadSlot( node : String ) : Void
	{
		//_global.debug("-------->frusion.service.Fruticard::onLoadSlot: node="+node) ;
		var nodeXML : XML ;
		var result : Object ;
		var slotId : Number ;

		nodeXML = new XML(node) ;


		// Get result
		result = MTSerialization.unserialize( nodeXML.firstChild.firstChild.nodeValue ) ;
		slotId = Number( nodeXML.firstChild.attributes.s ) ;

        this.currentSlotData[slotId] = nodeXML.firstChild.firstChild.nodeValue;
		this.slots[slotId] = result;
		this.loadCount--;


		// All slots are loaded finish FrusionClient startup process
		if( this.loadCount == 0 )
			this.broadcastMessage("onFrutiCardSlotLoaded");

	}


/*------------------------------------------------------------------------------------
 * Intrinsic methods for AsBroadcaster
 *------------------------------------------------------------------------------------*/


	function broadcastMessage(){}
	function addListener(){}
	function removeListener(){}


}
