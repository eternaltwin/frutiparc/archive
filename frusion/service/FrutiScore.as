import frusion.client.FrusionClient;
import frusion.service.ScoreParameter;
import ext.util.Pair;
import ext.util.MTCodec;
import frusion.Context;


/*
	Class: frusion.service.FrutiScore
	FrutiScore Service handler
*/
class frusion.service.FrutiScore
{


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


	private var frusionClient : FrusionClient;
	private var callbackList : Object;
	private var commandList : Object;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


    public function finalize() : Void
    {
        delete this.commandList;
        this.commandList = null;
    
        delete this.callbackList;
        this.callbackList = null;
        
        this.frusionClient.finalize();
        delete this.frusionClient;
        this.frusionClient = null;
    }
    

/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: FrutiScore
		Constructor
	*/
	public function FrutiScore( frusionClient : FrusionClient )
	{
		// Prepare broadcasting interface
		AsBroadcaster.initialize(this);


		// Add listener to current service
		this.frusionClient = frusionClient ;
		this.frusionClient.addListener(this);

		// Registering callback list
		this.callbackList = new Object();
		this.callbackList.c	 	= "onGetTime";
		this.callbackList.o		 	= "onStartGame";
		this.callbackList.p		 	= "onEndGame";
		this.callbackList.q		 	= "onSaveScore";
		this.callbackList.l		 	= "onListRankings";
		this.callbackList.m		 	= "onRankingResult";
		this.callbackList.n		 	= "onUserResult";
		this.callbackList.ac		= "onGiveAccessory";

        /*
		this.callbackList.onStartGame		 	= "o";
		this.callbackList.onEndGame 			= "p";
		this.callbackList.onSaveScore 			= "q";
		this.callbackList.onListRankings		= "l";
		this.callbackList.onRankingResult		= "m";
		this.callbackList.onUserResult			= "n";
        */
		this.frusionClient.registerCallbackList( callbackList );


		// creating appropriate commands
		this.commandList = new Object();
		this.commandList.startGame		 	= "o";
		this.commandList.endGame 			= "p";
		this.commandList.saveScore 			= "q";
		this.commandList.listRankings		= "l";
		this.commandList.rankingResult		= "m";
		this.commandList.userResult			= "n";
        this.commandList.giveItem           = "x";
        this.commandList.giveAccessory      = "ac";
		this.commandList.getTime 			= "c";
	}


/*------------------------------------------------------------------------------------
 * Public methods calling FrusionClient
 *------------------------------------------------------------------------------------*/


	/*
		Function: startGame
		Initialize a new game.
		The server will return the disc type if game successfully initialized (0,1,2,3).

		Parameters:
		d - disc id (md5)
	*/
	public function startGame( d : String ) : Void
	{
        this.frusionClient.registerStartGame();
		this.frusionClient.sendCommand(
				this.commandList.startGame,
				new Array(
					new Pair( "d", d ) ) );
	}


	/*
		Function: endGame
		End current play.
		This command requires no attribute as the server can retrieve
		the latest unfinished player game.
	*/
	public function endGame() : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.endGame,
				new Array() );
	}

	public function getTime() : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.getTime,
				new Array() );
	}


	/*
		Function: saveScore
		Save some scores for the current play.
		The server will provide a list of results depending of score progression
		on each available rankings.
	*/
	public function saveScore( score : Number, miscData : String, encodedScoreKey : String, discId : String ) : Void
	{
        var nodeStr : String ;
        var decodedKey : String ;

        // Security check
        if ( miscData == undefined )
	        miscData = "" ;

         // Decode the score key using session_id ( sid )
        decodedKey = MTCodec.decode( encodedScoreKey, _root.sid ) ;

        // Create a node for each score to save
        var scoreFinal : String = MTCodec.encode( String(score), decodedKey ) ;

        // Sending
		this.frusionClient.sendCommand(
				this.commandList.saveScore,
				new Array(
					new Pair( "d", miscData ),
					new Pair( "s", scoreFinal ),
                    new Pair( "i", discId )
				) ) ;
	}

    
    /*
        Function: giveItem
        Give a game item to the user.

        @param name String The item name.
    */
    public function giveItem( name:String ) 
    {
        var params : Array = [ new Pair("i", name) ];
        this.frusionClient.sendCommand( this.commandList.giveItem, params );
    }


    public function giveAccessory( name:String ) 
    {
        var params : Array = [ new Pair("i", name) ];
        this.frusionClient.sendCommand( this.commandList.giveAccessory, params );
    }


	/*
		Function: listRankings
		Retrieve a list of available rankings for later consultation.
		If no attribute is specified, the server will return a list of all current rankings.
	*/
	public function listRankings() : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.listRankings,
				new Array() );
	}


	/*
		Function: rankingResult
		Retrieve specified ranking results (scores and players).

		Parameters:
		- rk - ranking id
		- s - result start
		- l - result limit
	*/
	public function rankingResult( rk : Number, s: Number, l : Number ) : Void
	{
		this.frusionClient.sendCommand(
				this.commandList.listRankings,
				new Array(
					new Pair( "rk", rk ),
					new Pair( "s", s ),
					new Pair( "l", l ) ) );
	}


/*------------------------------------------------------------------------------------
 * Public callback methods from FrusionClient
 *------------------------------------------------------------------------------------*/


	public function onListModes( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_LISTMODES_BROADCAST, node);
	}


	public function onStartGame( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_STARTGAME_BROADCAST, node);
	}

	public function onGetTime( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_GETTIME_BROADCAST, node);
	}

	public function onEndGame( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_ENDGAME_BROADCAST, node);
	}


	public function onSaveScore( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_SAVESCORE_BROADCAST, node);
	}


	public function onListRankings( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_LISTRANKINGS_BROADCAST, node);
	}


	public function onRankingResult( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_RANKINGRESULT_BROADCAST, node);
	}


	public function onUserResult( node : String ) : Void
	{
		this.broadcastMessage(Context.FRUTISCORE_USERRESULT_BROADCAST, node);
	}


/*------------------------------------------------------------------------------------
 * Intrinsic methods for AsBroadcaster
 *------------------------------------------------------------------------------------*/


	function broadcastMessage(){}
	function addListener(){}
	function removeListener(){}


}
