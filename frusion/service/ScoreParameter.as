/*
	$Id: ScoreParameter.as,v 1.1 2003/11/25 17:09:58 sbenard Exp $
*/


/*
	Class: frusion.service.ScoreParameter
	Simple class to store a score subId and its value
*/
class frusion.service.ScoreParameter
{


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var subId : Number;
	public var value : Number;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: ScoreParameter
		Constructor
	*/
	public function ScoreParameter( subId : Number, value : Number )
	{
		this.subId = subId ;
		this.value = value ;
	}


}
