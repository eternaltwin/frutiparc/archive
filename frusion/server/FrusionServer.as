/*
	$Id: FrusionServer.as,v 1.1 2003/11/13 16:04:32 fnicaise Exp $
*/


import frusion.server.XMLServer;
import frusion.server.XMLCommand;
import frusion.gamedisc.GameDisc;
import frusion.Context;
import ext.util.Callback;
import ext.util.Observer;
import ext.util.Observable;
import ext.util.Pair;
import ext.util.CardiogramPatient;
import ext.util.Cardiogram;
import frusion.security.SecurityHQ;


/*
	Class: frusion.server.FrusionServer
	
    Server for all frusion clients. Must be initialized at project startup.
	This class implements the singleton Design Pattern. 
	To get the one and only one instance of the singleton, call FrusionServer.getInstance();

    *Please note that all base commands, such as ip, ping and time are not handled handled anymore by FrusionServer and
    FrutiCommons*
	
*/
class frusion.server.FrusionServer extends ext.util.ExtendedLocalConnection implements Observer, CardiogramPatient 
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = false;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


    private var frusionClientConnected : Boolean = false;
    private var connectionAlreadyFailed : Boolean = false;
    private var alreadyClosed : Boolean = false;
    private var alreadyFailed : Boolean = false;
    private var disconnected : Boolean = false;
    private var identDone : Boolean = false;
    private var logged : Boolean = false;
    private var closed : Boolean = false;
    private var cardiogram : Cardiogram;
	private var xmlsocket : XMLServer;
	private var callbackList : Object;
	private var gameDisc : GameDisc;
    private var port : Number;
    private var host : String;
    private var openMode : Number;
	private var closeInterval : Number;
    private var currentValidFrusionClient : Number;

    /*
    private var _securityProblem : Boolean = false;
    private var _securityError : String = "";
    */

    private var _securityHQ : SecurityHQ;


/*------------------------------------------------------------------------------------
 * Private static members
 *------------------------------------------------------------------------------------*/


	private static var instance : FrusionServer;
	private static var count : Number = 0;
    private static var CLIENTCOUNT : Number = 0;
		 

/*------------------------------------------------------------------------------------
 * cleanup
 *------------------------------------------------------------------------------------*/


	/*
		Function: finalize
	*/
    public function finalize() : Void
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::finalize" ) : undefined;
    }
    
    
/*------------------------------------------------------------------------------------
 * Private contructor
 *------------------------------------------------------------------------------------*/


	/*
		Function: FrusionServer
		Private constructor
	*/
	private function FrusionServer()
	{
		super( Context.FRUSION_SERVER_LOCAL_CONNECTION + _root.sid, Context.FRUSION_CLIENT_LOCAL_CONNECTION + _root.sid );
        
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::FrusionServer" ) : undefined;
        
        this.addDomain( Context.BASE_DOMAIN );
        this.addDomain( Context.SENDING_DOMAIN );

		// Inits connection to Server
    	this.xmlsocket = new XMLServer();
		
        _securityHQ = new SecurityHQ();

		// Create listeners
		AsBroadcaster.initialize(this);		
	}
	

/*------------------------------------------------------------------------------------
 * Instance methods enforcing Singleton Pattern
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: getInstance
		Singleton function to get the one and only one server instance
		
		Returns:
		FrusionServer - only one available instance fo the server 
	*/
	public static function getInstance() : FrusionServer
	{
        
		if( FrusionServer.instance == null )
			FrusionServer.instance = new FrusionServer();

        // already got an instance
		FrusionServer.count++;
		return FrusionServer.instance;	
	}
	
	
	/*
		Function: getInstanceCount
		Get the count of calls to FrusionServer.getInstance().

		Returns:
		count - count of calls to FrusionServer.getInstance().
	*/
	public static function getInstanceCount() : Number
	{
		return FrusionServer.count;
	}


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: connect
		Tries to connect to host.

	 	Parameters:
	 	- host : String - host we want to connect to 
	 	- port : Number - port on the host we want to connect to 
	*/
	private function XMLServerConnect( host : String, port : Number ) : Void
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::XMLServerConnect" ) : undefined;
		this.xmlsocket.connect( host, port );
	}			


	/*
		Function: closeConnection
		Close connection
	*/
	public function closeConnection() : Void
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::closeConnection" ) : undefined;
        // clear closeInterval which is set when the frutiparc close state is got from 
        // the Frusion manager via update method
        clearInterval( this.closeInterval );

        // close connection with server
        if( this.gameDisc.playMode == Context.FRUSION_MULTIPLAYER_LOADER )
    		this.xmlsocket.closeNow();
        else
    		this.xmlsocket.close();
        
        // tell everybody that the shop is closed ;)
        this.onXMLServerClose();
	}
	

	/*
	 	Function: init
	 	Init server information
	*/
	public function init( gameDisc : GameDisc, mode : Number, key : Number ) : Void
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::init with key =" + key + " and sid=" + _root.sid ) : undefined;

		// backup parameters ;)
		this.gameDisc = gameDisc;

        // init Security Manager
        _securityHQ.registerValidAccess();
        _securityHQ.registerValidDiscId( this.gameDisc.gameId );
        _securityHQ.registerValidKey( key );
        _securityHQ.storeGreenCard( this.gameDisc );

        this.openMode = mode;
        this.logged = false;
        this.disconnected = false;
        this.identDone = false;
        this.alreadyFailed = false;
        this.connectionAlreadyFailed = false;
        this.alreadyClosed = false;
        this.frusionClientConnected = false;
        this.disableConnectionFailureHandling();
        
        
		// Inits connection to get information from the client
		// Note: use of underscore is important as connections 
		// through different domains need this to find the shared LocalConnection 
		// see flash doc (allow domain)
        this.connect();
 	}


    /*
		Function: registerConnection
		
        Client tells it has connected with success, by the way gives it pointer so
	*/
    public function registerClientConnection( received : Object ) : Void
    {

        // test on gameDisc
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::registerClientConnection" ) : undefined;
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::registerClientConnection : client=" + Number( received[2] ) ) : undefined;

        /*
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::registerClientConnection : received swfid " + received[3] ) : undefined;
        var swfId : Number = received[3] == undefined ? 0 : 1;
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::registerClientConnection : local swfid " + this.gameDisc.gameId ) : undefined;
        if(swfId == 1) _securityHQ.checkDiscValidity( received[3] );
        */

        /*
        if( (swfId == 1) && ( String (received[3]) != this.gameDisc.gameId) )   
        {
            _securityError=" tried to fake swfid";
            _securityProblem = true;
        }
        */
        
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::registerClientConnection : OK " ) : undefined;
        this.currentValidFrusionClient = received[2];
        this.broadcastMessage( Context.FRUSIONSERVER_REGISTERCLIENTCONNECTIONCONNECTION_METHOD );
    }

    
/*------------------------------------------------------------------------------------	
* Public methods called by LocalConnection client
*------------------------------------------------------------------------------------*/
 

    public function sendXml( received : Object )
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::sendXml" + received ) : undefined;
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::sendXml" + FEString.unHTML( received[1] ) ) : undefined;
        this.xmlsocket.send( received[1] );        
    }
 
    /*
        Function: sendCommand
    */
    public function sendCommand( received : Object )
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::sendCommand" ) : undefined;
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::sendCommand : client=" + Number( received[2] ) ) : undefined;
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::sendCommand : this.currentValidFrusionClient=" + this.currentValidFrusionClient ) : undefined;
        
        if( this.currentValidFrusionClient == Number (received[2]) )
            this.xmlsocket.send( received[1] );
        else
            this.fatalError( Context.ERROR_SERVERCONNECTION_FAILURE );
    }


    public function debug( received : Object )
    {
         _global.debug( "Log from FrusionCLient : " + received[1] );
    }

    
	/*
	 	Function: getService
	 	Connect to a CBee service
	*/
	public function getService() : Void
	{				
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::getService" ) : undefined;
        
		// connect to the service
		this.xmlsocket.addListener( this );
		
        this.host = Context.BASE_DOMAIN;
        this.port = _global.cbeePort[ this.gameDisc.swfName ];

        this.DEBUG ? _global.debug("frusion.server.FrusionServer::getService :"  + this.port ) : undefined;
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::getService :"  + this.host ) : undefined;

        // Error Management
        if( this.port == undefined )
        {
            this.fatalError( Context.ERROR_PORT_UNKNOWN );
            return;
        }

        
        // connect and try to ident in the callback
		this.XMLServerConnect( this.host, this.port );
	}
	
	
	/*
		Function: getGameDisc()
		Returns current game disc to client 
	*/
	public function getGameDisc() : Void
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::getGameDisc" ) : undefined;
        this.sendRemote( Context.FRUSIONSERVER_ONGETGAMEDISC_METHOD, this.gameDisc );
	}
		
	
	/*
		Function: getUser
		Returns current user to client
	*/
	public function getUser() : Void
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::getUser" ) : undefined;
        this.sendRemote( Context.FRUSIONSERVER_ONGETUSER_METHOD, _global.me.name );
	}


 	/*
		Function: registerStartGame
		Tell frutiparc that a game was started and thus 
        ensure we burn the gamedisc once used
	*/
    public function registerStartGame() : Void
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::registerStartGame" ) : undefined;
        this.broadcastMessage( Context.FRUSIONSERVER_REGISTERSTARTGAME_METHOD );  
    }


	/*
		Function: fatalError
		For serious errors only
	*/
	private function fatalError( received : String ) : Void
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::fatalError: "+received ) : undefined;
        this.broadcastMessage( Context.FRUSIONSERVER_BREAKFRUSION_METHOD );  
        
        // display error message
        var code : String= received;
        this.DEBUG ? _global.debug(Lang.fv( "error.frusion.errorReport"  ) + code + " " + Lang.fv( "error.frusion." + code )  ) : undefined;
        _global.openErrorAlert( Lang.fv( "error.frusion.fatalError" ) + code + "<br>" + Lang.fv( "error.frusion.blur." + code )); 

        // close evrything whatever happens ;)
        this.onXMLServerClose();
	}

	/*
		Function: customFatalError
		For serious errors only
	*/
	private function customFatalError( received : String ) : Void
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::customFatalError" ) : undefined;
        
        // display error message
        var code : String= received[1];
        _global.openErrorAlert( "Une erreurs s'est produite:<br>" + code); 
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::customFatalError : " + received[2] ) : undefined;

        // close evrything whatever happens ;)
        this.onXMLServerClose();
	}


	/*
		Function: manageConnectionFailure
		When we lose the local connection handle failure
	*/
    public function manageLCFailure() : Void
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::manageLCFailure" ) : undefined;
        
        if( this.connectionAlreadyFailed ) return;
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::manageConnectionFailure::OK" ) : undefined;
        this.connectionAlreadyFailed = true;
        this.cardiogram.stop();
        this.closeConnection();
    }


/*------------------------------------------------------------------------------------
 * Ident callback
 *------------------------------------------------------------------------------------*/


	/*
		Function: onIdent
		CALLBACK. After ident command. This command is taken care of directly by FrusionServer, 
        not FrutiCommons as the latter never asks for ident.
	*/
	public function onIdent() : Void
	{  
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::onIdent" ) : undefined;
        // Did we get any swfid hack  attempt?
        /*
        if( !_securityHQ.gotValidDisc() )
        {
            this.xmlsocket.send( XMLCommand.buildCommand( "bf", 
                new Array( new Pair("l", "3"), new Pair("m",this.gameDisc.swfName) ), 
                "cheat:" + _global.me.name + " tried to fake disc id" ) );
            this.fatalError( Context.ERROR_SECURITY );
            _securityHQ.grantAccess();
            return;
        }
        */

        // Did we get any multi-windowing game attempt?
        if( !_securityHQ.accessGranted() )
        {
            this.xmlsocket.send( XMLCommand.buildCommand( "bf", 
                new Array( new Pair("l", "1"), new Pair("m",this.gameDisc.swfName) ), 
                "info:" + _global.me.name + " tried to launch disc in a new window" ) );
            _global.openErrorAlert( "D�sol�, le jeu n'a pu se lancer. Nous avons bien not� toutes les informations " 
                                        +"pour corriger le probl�me. Tu peux essayer de rejouer � pr�sent :)"); 
            _securityHQ.grantAccess();
            closeConnection();
        }
    

        if( !this.disconnected )
        {
            if( !this.identDone )
            {
                this.DEBUG ? _global.debug("frusion.server.FrusionServer::onIdent : Doing ident " ) : undefined;
                this.logged = true;
                this.identDone = true;
                
        		// Inits connection to send info to client
                this.initRemote();
        
                // start cardiogram to monitor network activity
                if( this.openMode == Context.FRUSION_OPENMODE_POPUP )
                {
                    _global.debug( "OPENING POPUP" );
                   this.enableConnectionFailureHandling( new Callback( this, manageLCFailure ) );
                   this.cardiogram = new Cardiogram( 1000, 2000, 1, this );
                   this.cardiogram.start();
                }

                // let the frusionManager tell the slot it is created so the user can begin to use it
                this.broadcastMessage( Context.FRUSIONSERVER_CREATESLOT_METHOD );  
    
                // let the frusionClient know the ident process is finished
                this.sendRemote( Context.FRUSIONSERVER_ONIDENT_METHOD, null );
            }
            else
            {
                // ident already done
                this.DEBUG ? _global.debug("frusion.server.FrusionServer::onIdent : Reconnection " ) : undefined;
                
                // try to send again commands
                this.xmlsocket.sendAfterReconnect();
                
                // inform client
                this.sendRemote( Context.FRUSIONSERVER_ONRECONNECT_METHOD, null );
            }
        }            
	}

	
/*------------------------------------------------------------------------------------
 * XMLServer callbacks
 *------------------------------------------------------------------------------------*/
		

	/*
		Function : onXML
		CALLBACK. called when any xml information is received in the XMLServer 
	*/
	public function onXML( node: XML ) : Void
	{
		// get k code value because all internal commands have
		// their code inferior to k		
		var kString : String = "k";
		var kIndex : Number = kString.charCodeAt(0);    

		// get nodeName and code				
		var xmlReceived = new XML( node.toString());
		xmlReceived = xmlReceived.lastChild;
		var nodeName : String = xmlReceived.nodeName;

        if( nodeName == "c" )
        {
            this.sendRemote( Context.FRUSIONSERVER_ONXML_METHOD, node.toString() );
            return;
        }

		var l : Number = nodeName.length;
		var nodeNameCode : Number = 0;
		if( l > 1 )
		{
			for( var i : Number = 0; i < l ; i++ )
				nodeNameCode += nodeName.charCodeAt(i);
		}
		else
		{
			nodeNameCode = nodeName.charCodeAt(0);
		}	


		// if commands code are inferior to k don't use base commands
		// else send node to client for further analysis		
		if( nodeNameCode < kIndex )
            return;

        if( nodeNameCode == kIndex && xmlReceived.attributes.l != undefined )
            this.onIdent();
		else 
            this.sendRemote( Context.FRUSIONSERVER_ONXML_METHOD, node.toString() );
	}


	/*
		Function: registerConnection
        
		Inform FP UI that a connection has been opened
	*/
    public function registerConnection() : Void
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::registerConnection" ) : undefined;
        
        this.broadcastMessage( Context.FRUSIONSERVER_REGISTERCONNECTION_METHOD );
    }


    /*
        Function : onXMLServerDisconnect
    */
    public function onXMLServerDisconnect() : Void
    {
        //this.DEBUG ? _global.debug("frusion.server.FrusionServer::onXMLServerDisconnect" ) : undefined;
        this.disconnected = true;
    }


    /*
        Function : onXMLServerConnect
    */
    public function onXMLServerConnect() : Void
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::onXMLServerConnect" ) : undefined;
        this.disconnected = false;
    }


    /*
        Function : onXMLServerClose
    */
    public function onXMLServerClose() : Void
    {
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::onXMLServerClose" ) : undefined;
        // xml socket should be closed now...
        // dispatching signal
        this.closed = true;
        
        // close localConnection with FrusionClient... I hate this!
        this.close();
 
        // Tell UI thet we'rre ready to close the shop
		this.broadcastMessage( Context.FRUSIONSERVER_ONREADYTOCLOSE_METHOD );
    }

    

/*------------------------------------------------------------------------------------
 * Public method implementing Observer
 *------------------------------------------------------------------------------------*/


	/*
		Function: update
		This method is called whenever the Observable 
		objects are changed. 
    */
	public function update( o: Observable , arg : Object )
	{
        this.DEBUG ? _global.debug("frusion.server.FrusionServer::update" ) : undefined;
        
        // Get observed object by its name (=arg)
    	var s : String = String (arg);
        switch( s )
        {
            case Context.FRUSIONMANAGER_CLASS:
            {
                var sent : Number = frusion.FrusionManager (o).getSent();
                if( sent == 0 )
                {
                    // In case we've changed frutiparc dimensions, send new coordinate system
                    var x : Number = frusion.FrusionManager (o).getX();
                    var y : Number = frusion.FrusionManager (o).getY();
                    this.sendRemote( Context.FRUSIONSERVER_ONUPDATESIZE_METHOD, new ext.geom.Point(x, y) );
                }
                else
                {
                    var state : String = frusion.FrusionManager (o).getState();
                    this.sendRemote( Context.FRUSIONSERVER_ONCHANGESTATE_METHOD, state );
    
                    // Put a timeout in case a close request was asked 
                    // and the frusion client is unreachable
                    // and thus unable to ask the FrusionServer to close
                    if( this.alreadyClosed ) return;
                    this.alreadyClosed = true;

                    if( state == Context.FRUSION_CLOSE_STATE && this.logged == true)
                   		this.closeInterval = setInterval(this, Context.FRUSIONSERVER_CLOSECONNECTION_METHOD, 3000);
                    /*    
                    else if( state == Context.FRUSION_CLOSE_STATE )
                        this.resetConnection();
                    */    
                }

                break;    
            }
            default:
                break;
        }
	}


/*------------------------------------------------------------------------------------
 * Intrinsic methods for AsBroadcaster
 *------------------------------------------------------------------------------------*/


	public function broadcastMessage(){}
	public function addListener(){}
	public function removeListener(){}


/*------------------------------------------------------------------------------------
 * Implementing CardiogramPatient        
 *------------------------------------------------------------------------------------*/

    
    /*
        Function: monitorActivity

    */
    public function monitorActivity() : Void
    {
        this.sendRemote( Context.FRUSIONSERVER_PULSE_METHOD, null );
    }

    /*
        Function : manageCrisis

        What to do when a crisis situation arise... close everything properly :)
    */    
    public function manageCrisis() : Void
    {}

    
/*------------------------------------------------------------------------------------
 * Cardiogram activity management
 *------------------------------------------------------------------------------------*/


    /*
        Function: pulse

        Register pulse so we know the patient is still alive. This method is called by frusionClient
        and thus allow us to check it is still alive
    */
    public function pulse() : Void
    {
        this.cardiogram.pulse();
    }

}
