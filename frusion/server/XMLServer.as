import frusion.server.XMLCommand;

/*
	Class: frusion.server.XMLServer
	XML Server for FrusionServer. 
*/
class frusion.server.XMLServer 
{


/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/

    
    public var connected : Boolean = false;
	public var logged : Boolean = false;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/

	
	private var commandBuffer : Array;
	private var host : String;
	private var port : Number;
	private var interval : Object;
	private var debugBox : box.Debug;	

    private var cbeeLocal : CBeeLC;   


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: XMLServer 
		Constructor
	*/
	public function XMLServer()
	{
		this.connected = false;
		this.logged = false;
        this.commandBuffer = new Array();
        
		XML.prototype.ignoreWhite = true;
		AsBroadcaster.initialize(this);

        this.debugBox = new box.Debug();
        if(_global.flDebug){
        	_global.desktop.addBox(this.debugBox);
        	this.debugBox.putInTab();
        }else{
        	_global.trashSlot.addBox(this.debugBox);
        }
    }
	



/*------------------------------------------------------------------------------------
 * Public methods redefining parent
 *------------------------------------------------------------------------------------*/
	
	/*
		Function: connect
		Attempts to connect to specified server on specified port		
	*/
	public function connect( host : String, port : Number )
	{
        _global.debug("frusion.server.XMLServer::Connect" );
		this.debug("Attempt to connect to "+host+" on port "+port);
        this.debugBox.setTitle("Debug "+port);

		this.host = host;
		this.port = port;
        this.cbeeLocal = _global.cbeeMng.addListener( this.port, this );

        // getting connection status
        var obj = _global.cbeeMng.getStatus(this.port);  
        this.connected = obj.connected;
        this.logged = obj.connected;
    }


	/*
		Function: send
		Send an xml command to server
		
		Parameters:
		xmlCommand - XML - command to send		
	*/
	public function send( xmlCommand : XML ) : Boolean
	{
		if(!this.connected || !this.logged)
		{
			this.debug("Must be connected to send data !");
            this.commandBuffer.push( xmlCommand );
			return false;
		}
		
		this.cbeeLocal.send( xmlCommand );
		this.debug("[S] "+FEString.unHTML(xmlCommand.toString()));
		return true;
	}


	/*
		Function: sendAfterReconnect
		Empty buffer after reconnection
    */        
    public function sendAfterReconnect() : Void
    {
		// send XML
        var l : Number = this.commandBuffer.length;
        if( l > 0 )
        {
            this.commandBuffer.reverse();
            var command : XML;
            for( var i : Number = 0; i<l; i++ )
            {
                command = XML (this.commandBuffer.pop());
                this.cbeeLocal.send( command );
    		    this.debug("[SR] "+FEString.unHTML(command.toString()));
            }    
        }
    }
    
    
/*------------------------------------------------------------------------------------
 * Callback methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: onConnect.
		CALLBACK
	*/
	public function onConnect( success : Boolean ) : Void
	{	
        _global.debug( "frusion.server.XMLServer::onConnect" );
		if( success )
		{
            this.connected = true;
   			this.debug("Connected to "+this.host+" on port "+this.port);
			this.broadcastMessage("onXMLServerConnect");
		}		
        else
        {
            this.connected = false;
			this.broadcastMessage("onXMLServerDisconnect");
        }    
	}
	
	
	/*
		Function: onClose.
		CALLBACK
	*/
	public function onClose() : Void
	{	
        _global.debug( "frusion.server.XMLServer::onClose" );
        this.connected = false;
        this.logged = false;
		this.broadcastMessage("onXMLServerDisconnect");
	}


	/*
		Function: onXML
		CALLBACK.
	*/
	public function onXML( node : XML ) : Void
	{
		// to get already good step
        this.debug("[R] "+FEString.unHTML(node.toString()));

        // add test on ident
        if( !this.logged )
        {
    		var xmlReceived = new XML( node.toString());
    		xmlReceived = xmlReceived.lastChild;
    		var nodeName : String = xmlReceived.nodeName;
            _global.debug( "nodeName =" + nodeName ); 
            _global.debug( "xmlReceived.attributes.l =" + xmlReceived.attributes.l ); 
            if( nodeName == "k" && xmlReceived.attributes.l != undefined )
                this.logged = true;
        }
        
		this.broadcastMessage("onXML", node);
    }


    /*
        Function : close

        Let the server perform some timeout before actually closing the connection
    */
    public function close() : Void
    {
        this.debug("closing connection to server " + this.host + " on port " + this.port );
        this.debug( "bye bye...");
        _global.cbeeMng.removeListener(this.port,this, false);
    }

    /*
        Function : closeNow

        Telle the server to close the connection to service immediately
    */
    public function closeNow() : Void
    {
        this.debug("closing connection to server now " + this.host + " on port " + this.port + " now");
        this.debug( "bye bye...");
        _global.cbeeMng.removeListener(this.port,this, true);
    }


/*------------------------------------------------------------------------------------
 * Private methods
 *------------------------------------------------------------------------------------*/


	/*
		Function: debug
		Appends message to _global.debug if it exists...  
	*/
	private function debug( message : String ) : Void
	{
        this.debugBox.addText(message);
	}


/*------------------------------------------------------------------------------------
 * Intrinsic methods for AsBroadcaster
 *------------------------------------------------------------------------------------*/


	function broadcastMessage(){}
	function addListener(){}
	function removeListener(){}
		

}
