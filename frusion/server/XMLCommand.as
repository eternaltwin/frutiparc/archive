/*
	$Id $
*/


import ext.util.Pair;


/*
	Class: frusion.server.XMLCommand
	Simple class to store a parameter name and its value
*/
class frusion.server.XMLCommand
{

	
/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: XMLCommand
		Constructor
	*/
	public function XMLCommand(){}
	

/*------------------------------------------------------------------------------------
 * Public static methods
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: buildCommand
		Send an xml command to server
		
		Parameters:
		commandName - String : command name		
		parameters - Array of Pair(s) objects: all the command parameters		
		information - String : information to pass in xml		
	*/	
	public static function buildCommand( commandName: String, parameters: Array, xml : String) : XML
	{
		// Creating xml with given command
		var x : Object = new XML();
		x.nodeName = commandName;
		
		// Apending parameters
		var l : Number = parameters.length;
		for( var i : Number = 0; i < l; i++)
		{
            _global.debug( "building command with name=" + parameters[i].name + " and value=" + parameters[i].value );
			//var currentParameter : Pair = Pair (parameters[i]);			
			x.attributes[parameters[i].name] = parameters[i].value;
		}
		
		// Append already existing xml to our new xml command 
		if(xml != null)
		{
			var xml : XML = new XML(xml);
			x.appendChild(xml);
		}
		
		return XML (x);
	}	

	
}
