/*
  $Id: FrusionAnim.as,v 1.2 2003/11/14 15:11:16 fnicaise Exp $
*/


import frusion.Context;


/*
  Class: frusion.gfx.FrusionAnim
  
  Frusion animation befor game startup
  
*/
class frusion.gfx.FrusionAnim
{


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


  private var mc: MovieClip;

  private var animScale : Number;
  private var animWidth : Number;
  private var animHeight : Number;
  private var gameWidth : Number;
  private var gameHeight : Number;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/


  /*
    Function: FrusionAnim
    Constructor

    Parameters:
    - mc : MovieClip - the frusionAnim MovieClip
  */
  public function FrusionAnim( mc : MovieClip, gameWidth : Number, gameHeight: Number )
  {
    this.mc = mc;
    this.gameWidth = gameWidth ;
    this.gameHeight = gameHeight ;

    this.animWidth = 400;
    this.animHeight = 300;
  }


/*
  anim = 1.33
  game = 1
*/

  /*
    Function: update
    Perform updates on the animation, change size, etc...
  */
  public function update() : Void
  {

    this.mc._xscale = 100 * ( this.gameWidth / Context.FRUSIONANIM_WIDTH );
    this.mc._yscale = 100 * ( this.gameHeight / Context.FRUSIONANIM_HEIGHT );
        
    /*
    var anim_ratio : Number = (this.animWidth / this.animHeight);
    var game_ratio : Number = (this.gameWidth / this.gameHeight);

    if ( game_ratio > anim_ratio ) 
    {
      // jeu plus large que l'anim
		this.animScale = this.gameWidth / this.animWidth;
    }
    else 
    {
      // Anim plus large que le jeu ou m�me proportions
		this.animScale = this.gameWidth / this.animWidth;
    }


    this.mc._xscale = this.animScale * 100;
    this.mc._yscale = this.animScale * 100;


    this.mc._x = (this.gameWidth - (this.animWidth * this.animScale)) / 2;
	this.mc._y = (this.gameHeight - (this.animHeight * this.animScale)) / 2;
    */
    
  }

  
  public function updateBar( percent : Number ) : Void
  {
    this.mc.updateBar( percent );
  }

}
