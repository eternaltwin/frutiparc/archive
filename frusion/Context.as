/*	
	$Id: Context.as,v 1.54 2005/08/04 16:06:03 pperidont Exp $
*/
/*
	Class: Context
	The Context stores global variables and constants for the frusion framework
*/
class frusion.Context
{	
	
/*------------------------------------------------------------------------------------
 * Public Static members
 *------------------------------------------------------------------------------------*/ 
    // Domains
    /*
    public static var BETA_DOMAIN : String = "www.beta.frutiparc.com";
    public static var BASE_DOMAIN : String = "www.beta.frutiparc.com";
    public static var SENDING_DOMAIN : String = "hq.motion-twin.com";
    /*/
    public static var BASE_DOMAIN : String = "swf.frutiparc.com";
    public static var BETA_DOMAIN : String = "";
    public static var SENDING_DOMAIN : String = "swf.frutiparc.com";
    //*/
    
    public static var LOG_CODE_DEBUG : String = "0"; 
    public static var LOG_CODE_INFO : String = "1"; 
    public static var LOG_CODE_WARNING : String = "2"; 
    public static var LOG_CODE_ERROR : String = "3"; 
    public static var LOG_CODE_FATAL : String = "4"; 
    public static var FRUSIONANIM_HEIGHT : Number = 300; 
    public static var FRUSIONANIM_WIDTH : Number = 400;
    public static var STAND_ALONE_PLAYER_STATUS : Number = 1;
    public static var NEW_GAME_SLOT_STATUS : Number = 0;
    public static var STARTED_GAME_STATUS : Number = 2;
    public static var JOIN_TYPE : Number = 0;
    public static var USER_TYPE : Number = 1;
    public static var EMPTY_TYPE : Number = 2;
    // Errors
    public static var ERROR_MISSING_SERVICES_XML : String = "0";
    public static var ERROR_PORT_UNKNOWN : String = "1";
    public static var ERROR_MISSING_GAME_SWF : String = "2";
    public static var ERROR_MISSING_ANIM_SWF : String = "3";
    public static var ERROR_FALSE_SIZE_GAME_SWF : String = "4";
    public static var ERROR_MISSING_PREVIEW_SWF : String = "5";
    public static var ERROR_FALSE_SIZE_PREVIEW_SWF : String = "6";
    public static var ERROR_MISSING_FRUTICONNECT_SWF : String = "100";
    public static var ERROR_MISSING_PANEL_XML : String = "101";
    public static var ERROR_MISSING_GAMEMANAGER : String = "102";
    public static var ERROR_MISSING_PLAYERPARAM_PANEL_XML : String = "103";
    public static var ERROR_LOCALCONNECTION_FAILURE : String = "201";
    public static var ERROR_SERVERCONNECTION_FAILURE : String = "202";
    public static var ERROR_CALLBACK_UNKNOWN : String = "203";
    public static var ERROR_GAME_UNREADABLE : String = "204";
    public static var ERROR_FILENOTFOUND : String = "300";
    public static var ERROR_SECURITY : String = "301";
    public static var ERROR_MISSINGDAILY : String = "302";
    public static var ERROR_TIMEOUT : String = "303";
    // Classes
    public static var XMLSERVER_CLASS : String = "XMLServer";
    public static var FRUSIONMANAGER_CLASS : String = "FrusionManager";
    
    // Methods
    public static var CHARTE_URL : String = "http://www.frutiparc.com/h/charte" ;
    public static var FRUSIONLOADER_INTERRUPTLOADING_METHOD : String = "_interruptLoading";
    public static var FRUSIONLOADER_CONTINUELOADING_METHOD : String = "_continueLoading";
    public static var FRUSIONSERVER_REGISTERLAUNCHING_METHOD : String = "registerLaunchingProcess";
    public static var FRUSIONSERVER_BREAKFRUSION_METHOD : String = "breakFrusion";
    public static var FRUSIONSERVER_CHECKCONNECTIONTOCLIENT_METHOD : String = "checkConnectionToClient";
    public static var FRUSIONSERVER_REGISTERCLIENTCONNECTIONCONNECTION_METHOD : String = "registerClientConnection";
    public static var FRUSIONSERVER_FATALERROR_METHOD : String = "fatalError";
    public static var FRUSIONSERVER_CUSTOMFATALERROR_METHOD : String = "customFatalError";
    public static var FRUSIONSERVER_LOGERROR_METHOD : String = "logError";
    public static var FRUSIONSERVER_CLOSECONNECTION_METHOD : String = "closeConnection";
    public static var FRUSIONSERVER_REGISTERCONNECTION_METHOD : String = "registerConnection";
    public static var FRUSIONSERVER_REGISTERSTARTGAME_METHOD : String = "registerStartGame";
    public static var FRUSIONSERVER_GETSERVICE_METHOD : String = "getService";
    public static var FRUSIONSERVER_SENDCOMMAND_METHOD : String = "sendCommand";
    public static var FRUSIONSERVER_GETGAMEDISC_METHOD : String = "getGameDisc";
    public static var FRUSIONSERVER_GETUSER_METHOD : String = "getUser";
    public static var FRUSIONSERVER_PULSE_METHOD : String = "pulse";
    public static var FRUSIONSERVER_ONGETGAMEDISC_METHOD : String = "onGetGameDisc";
    public static var FRUSIONSERVER_ONGETUSER_METHOD : String = "onGetUser";
    public static var FRUSIONSERVER_ONCHANGESTATE_METHOD : String = "onChangeState";
    public static var FRUSIONSERVER_ONUPDATESIZE_METHOD : String = "onUpdateSize";
    public static var FRUSIONSERVER_CREATESLOT_METHOD : String = "createSlot";
    public static var FRUSIONSERVER_ONRECONNECT_METHOD : String = "onReconnect";
    public static var FRUSIONSERVER_ONIDENT_METHOD : String = "onIdent";
    public static var FRUSIONSERVER_ONREADYTOCLOSE_METHOD : String = "onReadyToClose";
    public static var FRUSIONSERVER_SENDXML_METHOD : String = "sendXml";
    public static var FRUSIONSERVER_ONXML_METHOD : String = "onXML";
    // Broadcasts
    public static var FRUTICARD_SLOTLOADED_BROADCAST : String = "onFrutiCardSlotLoaded";
    public static var FRUTISCORE_LISTMODES_BROADCAST : String = "onListModes";
    public static var FRUTISCORE_STARTGAME_BROADCAST : String = "onStartGame";
    public static var FRUTISCORE_ENDGAME_BROADCAST : String = "onEndGame";
    public static var FRUTISCORE_GETTIME_BROADCAST : String = "onGetTime";
    public static var FRUTISCORE_SAVESCORE_BROADCAST : String = "onSaveScore";
    public static var FRUTISCORE_LISTRANKINGS_BROADCAST : String = "onListRankings";
    public static var FRUTISCORE_RANKINGRESULT_BROADCAST : String = "onRankingResult";
    public static var FRUTISCORE_USERRESULT_BROADCAST : String = "onUserResult";
    
    
    // Files
    public static var SERVICES_LIST_URL : String = "xml/services.xml";
    public static var FRUTICONNECT_SWF : String = "frutiConnect.swf";
    public static var ANIMFRUSION_SWF : String = "animfrusion.swf";
    public static var SKINFRUSION_SWF : String = "skinFrusion.swf";
    // Local connections
    public static var FRUSION_SERVER_LOCAL_CONNECTION : String = "_frusionserver";
    public static var FRUSION_CLIENT_LOCAL_CONNECTION : String = "_frusionclient";
    public static var FRUSION_LOADER_LOCAL_CONNECTION : String = "_frusionloader";
    // Frutiparc states
	public static var FRUSION_PAUSE_ON_STATE : String = "pauseOn";
	public static var FRUSION_PAUSE_OFF_STATE : String = "pauseOff";
	public static var FRUSION_CLOSE_STATE : String = "close";
	public static var FRUSION_RESET_STATE : String = "reset";
    // Frusion & gamedisc
	public static var FRUSION_MULTIPLAYER_LOADER : String			= "multi";
	public static var FRUSION_SINGLEPLAYER_LOADER : String			= "single";
	public static var FRUSION_PREVIEW_LOADER : String			= "preview";
	public static var FRUSION_LEVEL_LOADER : String			= "level";
	public static var FRUSION_OPENMODE_POPUP : Number		= 0;
	public static var FRUSION_OPENMODE_INTERNAL : Number	= 1;
	public static var FRUSION_OPENMODE_FRAME : Number		= 2;
    public static var GAMEDISC_INTERNAL_MODE : String = "i";
    public static var GAMEDISC_GREY : Number = 1;
    public static var GAMEDISC_WHITE : Number = 2;
    public static var GAMEDISC_BLACK : Number = 0;
    public static var GAMEDISC_RED : Number = 3;
    // movie clip linkage names
    public static var GAME : String = "game";
    public static var FRUSION : String = "frusion";
    public static var FRUSION_POPUP : String = "frusionPopUp";
    public static var FRUSION_SLOT : String = "frusionSlot";
    public static var FRUSION_MASK : String = "frusionMask";
    public static var FRUTICONNECT : String = "frutiConnect";
    public static var ANIM_INIT : String = "anim_init";    
    public static var PREVIEW : String = "preview";    
    public static var SKIN : String = "skinFrusion";    
    // xml
    public static var XML : String = "xml";
    public static var XML_PATH : String = "xml/";
    public static var XML_PANEL_SUFFIX : String = ".panel.xml"; 
    public static var XML_PLAYERPARAM_PANEL_SUFFIX : String = ".player.panel.xml"; 
    // Some garbage constants :( but still useful :)
    public static var ONE : String = "1";
    public static var S_NODE : String ="s";
    public static var GAMEDISC_INITMOVE : String = "initMove";
    public static var DO_LD_PHP_SCRIPT : String = "do/ld";
    public static var ONLOADGAMEDISC : String = "onLoadGameDisc";
    public static var INDEX : String = "index";
    public static var ERROR : String = "error";
    public static var SELF : String = "_self";
    public static var JS_SELFCLOSE : String = "javascript:self.close()";
    public static var FRUSION_BURSTDISC : String = "burstDisc";
    public static var FRUSION_RELEASEDISC : String = "releaseDisc";
    public static var FERMER : String = "close";
    public static var TRYTOCLOSE : String = "tryToClose";
    public static var SLOTFRUSION : String = "slotFrusion";
}	

