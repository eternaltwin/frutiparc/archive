/*
 *	$Id: InternalFrusion.as,v 1.19 2004/09/24 15:20:18 fnicaise Exp $
 */


import frusion.gamedisc.GameDisc;
import frusion.client.FrusionLoader;
import frusion.Context;


/*
	Class: InternalFrusion
*/
class frusion.frutiparc.InternalFrusion extends frusion.frutiparc.FPFrusionSlot
{


/*------------------------------------------------------------------------------------
 * DEBUG
 *------------------------------------------------------------------------------------*/


    private var DEBUG : Boolean = true;


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


	private var mcFrusion:MovieClip;
    private var mcMask:MovieClip;
    private var mcSkin:MovieClip;


/*------------------------------------------------------------------------------------
 * cleanup
 *------------------------------------------------------------------------------------*/


	/*
		Function: finalize
	*/
    public function finalize() : Void
    {
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::finalize" ) : undefined;

        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::finalize frusion: "  + this.mcFrusion) : undefined;

        this.mcSkin.finalize();
        this.mcSkin.removeMovieClip();
        delete this.mcSkin;

        this.mcFrusion.finalize();
        this.mcFrusion.removeMovieClip();
        delete this.mcFrusion;
        this.mcFrusion = null;
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::finalize frusion: "  + this.mcFrusion) : undefined;

        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::finalize mask : "  + this.mcMask) : undefined;

		this.mcMask.removeMovieClip();
        delete this.mcMask;
        this.mcMask = null;
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::finalize mask : "  + this.mcMask) : undefined;
    }
    

/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/
	
	
	/*
		Function: InternalFrusion
		Constructor.
	*/
	public function InternalFrusion(gd : GameDisc, x, b, z )
	{
        super( gd, x, b, z );
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::InternalFrusion" ) : undefined;
	}
	
	
	/*
		Function: init
		This method looks the game attributes and decides where the game shall be loaded
		either in a frame or in a popup or in the frutiparc swf.
		This method is called by a slotlist when the game is put in the frusion  
	*/
	public function init( slotList, depth : Number, flGo : Boolean ) : Void
	{
		super.init(slotList,depth,flGo);
        
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::init" ) : undefined;
		// The game will be loaded in frutiparc
		this.gd.size = this.gd.files[Context.INDEX].size; 

        /*
        var mcName = "SkinFrusion";
		this.slotList.mc.createEmptyMovieClip( mcName ,this.baseDepth + Depths.frusionSlot.skinFrusion);
		this.mcSkin = this.slotList.mc[mcName];
        FEMC.initDraw( this.mcSkin );
        */
     
		var mcName = Context.FRUSION_SLOT+FEString.uniqId();
		this.slotList.mc.attachMovie(Context.FRUSION_SLOT,mcName,this.baseDepth + Depths.frusionSlot.main);
		this.mcFrusion = this.slotList.mc[mcName];

        _global.debug( "this.mcFrusion.tourneboule=" + _root.tourneboule );
        this.mcFrusion.tourneboule = _root.tourneboule;
        
		mcName = Context.FRUSION_MASK +FEString.uniqId();		// SWF EXTERNE A FP2 ( frame )

		this.slotList.mc.createEmptyMovieClip(mcName,this.baseDepth + Depths.frusionSlot.mask);
		this.mcMask = this.slotList.mc[mcName];
		this.mcMask.initDraw();				
			
		// Create mask
		// I love AS2 so much...
		var width : String = String (this.gd.width);
		var height : String = String (this.gd.height);			
		var tempWidth : Number = parseInt( width, 10);
		var tempHeight : Number = parseInt( height, 10);
		var obj = {x:0,y:0,w:tempWidth,h:tempHeight};
		this.mcMask.drawSquare(obj,0xFF0000);
		this.mcFrusion.setMask(this.mcMask);
		this.mcFrusion.init( this.gd.playMode );
                    
		// update size
		this.updateSize();
	}


/*------------------------------------------------------------------------------------
 * Frusion UI Events
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: onActivate
	*/
	public function onActivate() : Void
	{
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::onActivate" ) : undefined;
        
		super.onActivate();
		this.mcFrusion._visible = true;
		this.mcMask._visible = true;
	}


	/*
		Function: onDeactivate
	*/
	public function onDeactivate() : Void
	{
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::onDeactivate" ) : undefined;

		super.onDeactivate();
		this.mcFrusion._visible = false;
		this.mcMask._visible = false;
	}


/*------------------------------------------------------------------------------------
 * Frusion States modifiers 
 *------------------------------------------------------------------------------------*/


	/*
		Function: onReadyToClose
		When the client os ready to close, 
		the FrusionManager sends this message		
	*/
	public function onReadyToClose() : Void
	{
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::onReadyToClose" ) : undefined;
   		super.onReadyToClose();
	}
	

    /*
		Function: setDepth

	*/
	public function setDepth(depth) : Void
	{
		super.setDepth(depth);
        
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::setDepth" ) : undefined;
		this.mcFrusion.swapDepths(this.baseDepth + Depths.frusionSlot.main);
		this.mcMask.swapDepths(this.baseDepth + Depths.frusionSlot.mask);
	}


	/*
		Function: updateSize
	*/
	public function updateSize() : Void
	{
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::updateSize" ) : undefined;
        
		this.mcFrusion._x = _global.main.cornerX + ((_global.mcw - _global.main.cornerX) - this.gd.width) / 2;
		this.mcFrusion._y = _global.main.cornerY + ((_global.mch - _global.main.cornerY) - this.gd.height) / 2;

        /*
        var coco = Standard.getWinStyle();
        var info =
        {
			outline:1,
			inline:2,	
			curve:10,
			color:
            {
				main:		coco.global.color[0].main,
				inline:		coco.global.color[0].shade,
				outline:	coco.global.color[0].darkest
			}
		}   
        var p = { x:this.mcFrusion._x , y:this.mcFrusion._y, w:this.gd.width, h:this.gd.height};
        this.mcSkin.clear();
		FEMC.drawCustomSquare( this.mcSkin, p, info, true )
        */

        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::updateSize : x" + this.mcFrusion._x ) : undefined;
        this.DEBUG ? _global.debug("frusion.frutiparc.InternalFrusion::updateSize : y" + this.mcFrusion._y ) : undefined;
        
		this.mcMask._x = this.mcFrusion._x;
		this.mcMask._y = this.mcFrusion._y;
        
   		//_global.frusionMng.updateFrusionCoordinates( this.mcFrusion._x, this.mcFrusion._y );
	}

}
