/*
	$Id: FPFrusionSlot.as,v 1.20 2004/01/09 15:16:27 fnicaise Exp $
*/


import frusion.gamedisc.GameDisc;
import frusion.client.FrusionLoader;
import frusion.Context;

/*
	Class: FPFrusionSlot
*/
class frusion.frutiparc.FPFrusionSlot extends Slot
{


/*------------------------------------------------------------------------------------
 * DEBUG FLAG
 *------------------------------------------------------------------------------------*/


    public var DEBUG : Boolean = true;
	
	
/*------------------------------------------------------------------------------------
 * Public members
 *------------------------------------------------------------------------------------*/


	public var mc:MovieClip;
	public var debug : Function;
    public var created : Boolean = false;  
    public var connected : Boolean = false;  


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/

    private var onPauseSignalSent : Boolean = false;
    private var closeSignalSent	: Boolean = false;
	private var gd : frusion.gamedisc.GameDisc;
	private var frusionServer : frusion.server.FrusionServer; 
    private var gameStarted : Boolean = false;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/
	
	
	/*
		Function: FPFrusionSlot
		Constructor.
		
		Parameters: 
		- gd : GameDisc : the gamedisc which contains all relevant information
	*/
	public function FPFrusionSlot(gd : GameDisc, x, b, z )
	{
		this.gd = gd;
	}
	
	
	/*
		Function: init
		This method looks the game attributes and decides where the game shall be loaded
		either in a frame or in a popup or in the frutiparc swf.
		This method is called by a slotlist when the game is put in the frusion  
	*/
	public function init( slotList, depth : Number, flGo : Boolean ) : Void
	{
		super.init(slotList,depth,flGo);
		this.title = Lang.fv( Context.FRUSION );
	}


/*------------------------------------------------------------------------------------
 * Frusion UI Events
 *------------------------------------------------------------------------------------*/

    
	/*
		Function: registerStartGame
		Tell frutiparc that a game was started and thus 
        ensure we burn the gamedisc once used
	*/
    public function registerStartGame() : Void
    {
		this.gameStarted = true;
        _global.me.status.setInternal( this.gd.swfName ); 
    }

    
	/*
		Function: registerConnection
		Tell frutiparc that a connection is open
	*/
    public function registerConnection() : Void
    {
        this.DEBUG ? _global.debug( "FPFrusionSlot::registerConnection" ) : undefined;
        this.connected = true;
    }

    
    /*
        Function: createSlot
        Creates slot in frutiparc
    */
    public function createSlot() : Void
    {
        this.created = true;
    }
    
    
	/*
		Function: onActivate
	*/
	public function onActivate()
	{
		super.onActivate();

        // send pause signal to game
        //if(	this.onPauseSignalSent && this.connected )
        _global.frusionMng.changeFrusionState( Context.FRUSION_PAUSE_OFF_STATE );
    	_global.topDesktop.disable();
        _global.wallPaper.hideImage();
        _global.me.status.setInternal( this.gd.swfName ); 
        this.mc.activate();
	}


	/*
		Function: onDeactivate

        Slot is deactivated
	*/
	public function onDeactivate() : Void
	{
		super.onDeactivate();

        // send pause signal to game
        /*
        if(	!this.onPauseSignalSent && this.connected )
        {
            //_global.debug( "onDeactivate first-------------------------------------------------" );
            this.onPauseSignalSent = true; 
            _global.frusionMng.changeFrusionState( Context.FRUSION_PAUSE_STATE );
        } 
        else
        {
            //_global.debug( "onDeactivate after" );
            _global.frusionMng.changeFrusionState( Context.FRUSION_PAUSE_STATE );
        }    
        */
        
        _global.me.status.unsetInternal( this.gd.swfName ); 
        _global.frusionMng.changeFrusionState( Context.FRUSION_PAUSE_ON_STATE );
    	_global.topDesktop.enable();
        _global.wallPaper.showImage();
        this.mc.deactivate();

	}


	public function onWarning()
	{
		this.mc.warning();
		super.onWarning();
	}

	
	public function onStopWarning()
	{
		this.mc.stopWarning();
		super.onStopWarning();
	}


/*------------------------------------------------------------------------------------
 * Frusion States modifiers 
 *------------------------------------------------------------------------------------*/


	/*
		Function: close
	*/
	public function close() : Void
	{	
		// try to close connection
        if( this.closeSignalSent ) return;
        
        //XXX TEST EJECT BUTTON
        /*
        if(	this.gameStarted || this.connected )
            this.closeSignalSent = true;
        */

        //XXX TEST EJECT BUTTON
        if(	this.connected )
            this.closeSignalSent = true;
        
		_global.frusionMng.changeFrusionState( Context.FRUSION_CLOSE_STATE );
	}


	/*
		Function: onReadyToClose
		When the client os ready to close, 
		the FrusionManager sends this message		
	*/
	public function onReadyToClose() : Void
	{
        this.DEBUG ? _global.debug( "onReadyToClose" ) : undefined;

        //if(	this.gameStarted )
        _global.me.status.unsetInternal( this.gd.swfName );
        
		// stop gamedisc and make it blow up ?
		if( this.gd.discType < Context.GAMEDISC_WHITE && this.gameStarted )
			_global.main.frusion.stopDisc( Context.FRUSION_BURSTDISC );
		else
			_global.main.frusion.stopDisc( Context.FRUSION_RELEASEDISC );

        _global.fileMng.frusionOff();		

        this.finalize();
        
        // delete all non obfuscated game information
        this.DEBUG ? _global.debug( "DESTROYING non obfuscated packages" ) : undefined;
        delete _global[this.gd.swfName];
        _global[this.gd.swfName] = null;
        delete _global.asml;
        _global.asml = null;
        delete _global.Std;
        _global.Std = null;


        // delete all ASML information
        this.DEBUG ? _global.debug( "DESTROYING obfuscated packages" ) : undefined;
        // ["153&"] > asml
        delete _global["153&"];
        _global["153&"] = null;
        // ["3&!$"] > Std
        delete _global["3&!$"];
        _global["3&!$"] = null;
        
        _global.topDesktop.enable();
        _global.wallPaper.showImage();
        
		super.close();
	}
	
	
	/*
		Function: getMenu

        Frusion menu displayed when clicking on the slot
	*/
	public function getMenu()  
	{
        if( this.created )
        {
    		return [
    			{title: Lang.fv( Context.FERMER ), action: {onRelease: [{obj:this, method:Context.TRYTOCLOSE}]}}
    		];
        }
	}

	
	/*
		Function: setDepth
	*/
	public function setDepth(depth)
	{
		super.setDepth(depth);
	}


	/*
		Function: onStageResize
	*/
	public function onStageResize()
	{
		this.updateSize();
	}


	/*
		Function: updateSize

        Abstract method
	*/
	public function updateSize(){}
	public function finalize(){}

	
	/*
		Function: getIconLabel
	*/
	public function getIconLabel() : String
	{
		return Context.SLOTFRUSION;
	}
	
}
