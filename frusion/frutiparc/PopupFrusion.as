/*
 *	$Id: PopupFrusion.as,v 1.20 2004/08/02 16:41:57 fnicaise Exp $
 */


import frusion.gamedisc.GameDisc;
import frusion.client.FrusionLoader;
import frusion.Context;
import frusion.util.FrusionParams;


/*
	Class: PopupFrusion
*/
class frusion.frutiparc.PopupFrusion extends frusion.frutiparc.FPFrusionSlot
{


/*------------------------------------------------------------------------------------
 * Private members
 *------------------------------------------------------------------------------------*/


	private var mcFrusion:MovieClip;
    private var x : Number;
    private var b : Number;
    private var y : Number;


/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/
	
	
	/*
		Function: PopupFrusion
		Constructor.
	*/
	public function PopupFrusion( gd : GameDisc, x, b, y )
	{
        super( gd, x, b, y );
        this.x = x;
        this.b = b;
        this.y = y;
	}
	
	
	/*
		Function: init
		This method looks the game attributes and decides where the game shall be loaded
		either in a frame or in a popup or in the frutiparc swf.
		This method is called by a slotlist when the game is put in the frusion  
	*/
	public function init( slotList, depth : Number, flGo : Boolean ) : Void
	{
        super.init(slotList,depth,flGo);

    	getURL("javascript:fp_goURLNewWin('" + _global.baseURL + "frusion/?sid="+_root.sid
        	+ "&frutipass=" + random( 1000000 ) 
            + "&gaspard=" + this.b
        	+ "&w="+ this.gd.width
            + "&irma=" + this.x
        	+ "&h="+ this.gd.height
            + "&tourneboule=" + this.y
            + "&gameId=" + random( 1000000 )
            + "&gromelin=" + random( 1000000 )
        	+ "&playMode="+ this.gd.playMode
        	+ "',2,"
        	+ this.gd.width +"," 
        	+ this.gd.height + ")"
    	    ,"");

                        
		// TODO: faire l'image "le jeu s'est ouvert dans une nouvelle fenetre"
		var mcName = Context.FRUSION_SLOT+FEString.uniqId();
		this.slotList.mc.attachMovie(
            Context.FRUSION_POPUP, 
            mcName, 
            this.baseDepth + Depths.frusionSlot.main, 
            {gamedisc:this.gd}
            );
                    
		// update size
		this.updateSize();
	}


/*------------------------------------------------------------------------------------
 * Frusion UI Events
 *------------------------------------------------------------------------------------*/


	/*
		Function: onActivate
	*/
	public function onActivate()
	{
		this.mcFrusion._visible = true;
		//super.onActivate();
	}


	/*
		Function: onDeactivate
	*/
	public function onDeactivate() : Void
	{
		this.mcFrusion._visible = false;
		//super.onDeactivate();
	}


/*------------------------------------------------------------------------------------
 * Frusion States modifiers 
 *------------------------------------------------------------------------------------*/

    
	/*
		Function: onReadyToClose
		When the client os ready to close, 
		the FrusionManager sends this message		
	*/
	public function onReadyToClose() : Void
	{
		this.mcFrusion.removeMovieClip();
        getURL("javascript:fp_closePopupFrusion()"); 
   		super.onReadyToClose();
	}
	

    /*
		Function: setDepth
	*/
	public function setDepth(depth)
	{
		super.setDepth(depth);
		this.mcFrusion.swapDepths(this.baseDepth + Depths.frusionSlot.main);
	}	
    
    
	/*
		Function: updateSize
	*/
	public function updateSize()
	{
		this.mcFrusion._x = _global.main.cornerX + ((_global.mcw - _global.main.cornerX) - this.gd.width) / 2;
		this.mcFrusion._y = _global.main.cornerY + ((_global.mch - _global.main.cornerY) - this.gd.height) / 2;
	}

}
