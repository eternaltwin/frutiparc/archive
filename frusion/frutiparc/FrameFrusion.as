/*
 *	$Id: FrameFrusion.as,v 1.15 2004/08/02 16:41:57 fnicaise Exp $
 */


import frusion.gamedisc.GameDisc;
import frusion.client.FrusionLoader;
import frusion.Context;

/*
	Class: FrameFrusion
*/
class frusion.frutiparc.FrameFrusion extends frusion.frutiparc.FPFrusionSlot
{
	

    private var x : Number;
    private var b : Number;
    private var y : Number;

/*------------------------------------------------------------------------------------
 * Public methods
 *------------------------------------------------------------------------------------*/
	
	
	/*
		Function: FrameFrusion
		Constructor.
	*/
	public function FrameFrusion(gd : GameDisc, x, b, y )
	{
        super( gd, x, b, y );
        this.x = x;
        this.b = b;
        this.y = y;
   	}
	
	
	/*
		Function: init
		This method looks the game attributes and decides where the game shall be loaded
		either in a frame or in a popup or in the frutiparc swf.
		This method is called by a slotlist when the game is put in the frusion  
	*/
	public function init( slotList, depth : Number, flGo : Boolean ) : Void
	{
		super.init(slotList,depth,flGo);
        
    	getURL("javascript:fp_goURLResize('" + _global.baseURL + "frusion/?sid="+_root.sid
        	+ "&frutipass=" + random( 1000000 ) 
            + "&gaspard=" + this.b
        	+ "&w="+ this.gd.width
            + "&irma=" + this.x
        	+ "&h="+ this.gd.height
            + "&tourneboule=" + this.y
            + "&gameId=" + random( 1000000 )
            + "&gromelin=" + random( 1000000 )
        	+ "&playMode="+ this.gd.playMode
        	+ "',2,"
        	+ this.gd.width +"," 
        	+ this.gd.height + ")"
    	    ,"");
                    
		// update size
		this.updateSize();
	}


/*------------------------------------------------------------------------------------
 * Frusion UI Events
 *------------------------------------------------------------------------------------*/

	
	/*
		Function: onActivate
	*/
	public function onActivate()
	{
		getURL("javascript:fp_resizeMe(2)","");
		super.onActivate();
	}


	/*
		Function: onDeactivate
	*/
	public function onDeactivate() : Void
	{
		getURL("javascript:fp_resizeMe(0)","");
		super.onDeactivate();
	}


/*------------------------------------------------------------------------------------
 * Frusion States modifiers 
 *------------------------------------------------------------------------------------*/


	/*
		Function: onReadyToClose
		When the client os ready to close, 
		the FrusionManager sends this message		
	*/
	public function onReadyToClose() : Void
	{
		getURL("javascript:fp_closeFrame(2)","");
   		super.onReadyToClose();
	}


}
