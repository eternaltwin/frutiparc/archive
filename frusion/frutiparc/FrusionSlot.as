/*
	$Id: FrusionSlot.as,v 1.1 2003/10/25 14:43:14 pperidont Exp $
*/


/*
	Class: FrusionSlot
	
	This class represents a MovieClip context for the loaders
*/
class frusion.frutiparc.FrusionSlot extends MovieClip
{
	
	private var fl : frusion.client.FrusionLoader;

	/*
		cleans the FrusionLoader
	*/
    public function finalize() : Void
    {
        this.fl.finalize();
        delete this.fl;
        this.fl = null;
    }   


	/*
		Function: init
		
		*Inits the FrusionLoaders.*
	*/
	public function init( playMode : Number ) : Void
	{		          
   		// Determines the play mode	
		if( playMode == frusion.Context.FRUSION_MULTIPLAYER_LOADER )
			this.fl = new frusion.client.MultiLoader( _global.swfURL, this  );
		else if( playMode == frusion.Context.FRUSION_SINGLEPLAYER_LOADER )		
			this.fl = new frusion.client.SingleLoader( _global.swfURL, this );
		else if( playMode == frusion.Context.FRUSION_PREVIEW_LOADER )		
			this.fl = new frusion.client.PreviewLoader( _global.swfURL, this );
	}

}   
// EOF

