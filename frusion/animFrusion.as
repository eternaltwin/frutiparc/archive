mcw = 400
mch = 300

function initLoading(){
	//_root._alpha=50
	var margin = 6
	var side = 60 * (100/_xscale)
	
	size = 0;
	timer = 0;
	rot = 0;
	crot = 0;
	
	max = mch - (side+margin*3)
	
	this.bar._x = margin
	this.bar._y = mch-margin
	this.bar._xscale = side;
	this.bar._yscale = side;

	this.logo._x = margin
	this.logo._y = margin
	this.logo._xscale = side;
	this.logo._yscale = side;
	
	this.logo.turner.s0.gotoAndStop(1)
	this.logo.turner.s1.gotoAndStop(2)
	this.logo.turner.s2.gotoAndStop(3)
	
	
	this.fieldPercent._x = margin
	this.fieldPercent._xscale = side;
	this.fieldPercent._yscale = side;
	
	//this.loader.animComplete()
	
}

function updateBar(nb){
	//_root._alpha=50
	ratio = nb/100
}

function update(){
	//_root.test+="size("+size+") ratio("+ratio+")\n"

	// BAR
	var cible = (max*ratio)
	size = size*0.8 + cible*0.2;
	this.fieldPercent.text = String(Math.round((size/max)*100)+"%")
	this.fieldPercent._y = mch-(margin+size+4);
	this.bar._yscale = size;
	if( Math.abs(size-cible) < 0.5 ){
		//_root._alpha = 10
		this.loader.animComplete()
	}

	// LOGO
	timer++
	if(timer>16){
		timer = 0;
		crot+=120 // = (rot+120)%360
	}
	rot = rot*0.6 + crot*0.4
	this.logo.turner._rotation = rot
	//
	for(var i=0; i<3; i++){
		this.logo.turner["s"+i]._rotation = -this.logo.turner._rotation
	}
}






