/*
	
	Title: Utilisation du service FrutiScore
	
	*Ce manuel comprend l'ensemble de la documentation utile pour utiliser le service FrutiScore, l'outil de sauvegarde des scores.*
*/
/*
    Symbol: Introduction

    Tous les jeux qui proposent un syst�me de score impl�mentent le syst�me FrutiScore. Ce service propose les options suivantes :
    - R�cup�ration des modes de jeux
    - Sauvegarde des scores
    - r�cup�ration des scores et classements
    - appel au serveur au d�marrage et � la fin d'une partie

    *Ce service n'est pas impl�ment� par d�faut par le client FrusionClient.* Aussi est-il indispensable de l'inclure manuellement pour les
    jeux � score. Une autre solution est de se servir du client GameClient qui propose en standard les supports FrusionClient, FrutiCard et
    FrutiScore (cf etape au choix game client dans le menu du tutorial). En g�n�ral c'est l'option la plus simple et g�n�ralement
    conseill�e, car tous les m�canismes sont directement pr�ts � l'emploi.

    Cependant si vous souhaitez tout de m�me impl�menter vous m�me FrutiScore suivez le guide... :)
*/
/*
    Symbol: Ajout du service � votre client

    Reprenons l'exemple de classe maClasseDeService de la section pr�c�dente (doc FrusionClient). 
    
    En premier lieu, ajoutons un membre FrutiScore priv� � notre classe.
    
    >   private var frutiScore : frusion.service.FrutiScore ;

    
    Ensuite ajoutons les lignes de code nec�ssaires � la fin du constructeur.

	>	public function maClasseDeService()
	>	{
    >		this.frusionClient = new frusion.client.FrusionClient() ;
	>		this.frusionClient.addListener(this);
	>
	>		this.frusionClient.registerReadyCallback( new Callback(this,"onServiceConnect") ) ;
    >		this.frusionClient.registerPauseCallback( new Callback(this,"onFrutiparcPause")) ;
    >       
    >       ...    
    >       ... Code d'init des callbacks, etc...   
    >       ...    
    >       ...    
    >       
    >       // Manager de score FrutiScore
    >       this.frutiScore = new frusion.service.FrutiScore( this.frusionClient ) ;
    >       this.frutiScore.addListener(this);
    >       
    >   }

    Dans les lignes pr�c�dentes, on peut remarquer que le service FrutiScore prend le FrusionClient en param�tres. Ceci lui permet en fait
    de dialoguer directement avec ce dernier. 
    
    Ensuite notez l'ajout d'un listener sur notre classe qui permettra de r�cup�rer les informations en provenance du serveur via le
    FrutiScore dans notre classe. Pour r�cup�rer les informations en provenance de FrutiScore il suffira d'ajouter les diff�rents callbacks
    comme indiqu�s dans la doc de la classe (voir doc frusion.service.FrutiScore). 

    Pour info, la liste des callbacks importants � impl�menter est la suivante :
    - onSaveScore
    - onEndGame
    - onStartGame
    - onListModes
*/
/*
    Symbol: R�cup�rer les modes de jeu

    Avant de d�marrer, vous voudrez certainement r�cup�rer les diff�rents modes accessibles au joueur pour votre jeu. A cette fin, il suffit de faire l'appel
    suivant : 

    > this.frutiScore.listModes( this.gameDisc.gameId );

    Dans cete exemple, vous noterez le passage d'un objet appel� gameDisc, objet dont nous n'avons pas encore parl�. 

    Le GameDisc est la structure de donn�es qui stocke toutes les informatiosn relatives au jeu et � ses fichiers de d�pendance (mp3, swf,
    etc...). Par d�faut le client FrusionClient s'occuppe � votre place de r�cup�rer ce GameDisc. Mais nous allons l'ajouter � notre client
    pour avoir plus de facilit�s � l'exploiter.

    Ajoutons un membre gameDisc � notre classe :
    
    > private var gameDisc : frusion.gamedisc.GameDisc;

    Pour r�cup�rer le gameDisc de la FrusionClient ajoutons simplement la ligne suivante dans le constructeur de notre classe :
    
    > this.gameDisc = this.frusionClient.gameDisc;


    Cette parenth�se �tant ferm�e, nous devons maintenant r�cup�rer les modes de jeu gr�ce au callback dont nous avons d�j� parl�
    pr�ce�demment, � savoir *onListMode*.

    Les modes de jeu �tant stock�s sous forme de tableau, nous allons cr�er un nouveau membre de type Array � notre classe :

    > private var gameModes : Array;

    Il ne reste plus qu'� r�cup�rer les modes � l'aide de notre impl�mentation du callback (ce mod�le est pris sur le code du GameClient) :

    >public function onListModes(nodeTxt : String) : Void 
    >{
    >    var node : XML = new XML(nodeTxt) ;
    >    var modeStr : String = node.firstChild.attributes.md ;
    >    var modeList : Array ;
    >
    >    if ( node.firstChild.attributes.k != undefined )
    >    {
    >       // afficher un message d'erreur
    >       return;
    >    }
    >
    >    // Extraction de la liste brute
    >    if ( modeStr==undefined || modeStr=="" )
    >       modeList = new Array() ;
    >    else
    >       modeList = modeStr.split(":") ;
    >
    >    // Mise � jour des gameModes
    >    this.gameModes = new Array() ;
    >    for (var i=0;i<modeList.length;i++)
    >       this.gameModes[ parseInt(modeList[i], 10) ] = true ;
    >
    >}

    Pas grand chose � noter sur le code pr�c�dent, on r�cup�re les informations du flux xml et on les transforme en tableau qui contient nos
    modes. Remarquez cependant en
    d�but de fonction la gestion des erreurs comment�es. A la r�ception de tout message xml, il faut impl�menter un test d'erreur sur
    l'attribut k qui est un drapeau pour signaler une erreur.
*/

