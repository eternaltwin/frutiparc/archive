/*
	
	Title: Manuel
	
	*Ce manuel comprend l'ensemble de la documentation utile pour utiliser le framework de la frusion
	ou impl�menter de nouveaux services et m�canismes.*
		
	- Il peut proposer des examples de code source comment�s, des sch�mas si besoin, et autres indications
	pour aider le d�veloppeur � se servir du package sans a priori avoir besoin de demander d'aide.
	- Il est probable que malgr� la maintenance effectu�e sur ce manuel et la documentation du code source 
	lui m�me, il subsiste des erreurs. Ces erreurs si elles sont rep�r�es doivent �tre modifi�es de suite
	de fa�on � ne conserver au final qu'une documentation exempte de toutes erreurs et utilisable par tous
	sans perte de temps. 

*/
/*	
	Section: Architecture du framework...
	
	Le framework frusion a �t� cr�� pour r�pondre � plusieurs besoins:
	- g�rer les communications client-serveur entre le jeu et frutiparc de fa�on transparente 
	- proposer un environnement standard, que le jeux soit charg� dans Frutiparc ou non
	- proposer aux d�veloppeurs un ensemble de m�cansimes faciles � utiliser et pr�ts � l'emploi 
	en cachant au besoin la compl�xit� du syst�me gr�ce � des classes et/ou des interfaces
*/
/*
	Package: Package frusion
	
	*Ce package comprend les classes de base de chargement de la frusion.*
*/
/*
	Symbol: Context
	
	*Cette classe conserve un ensemble de membres statiques globales au projet frusion*
*/
/*
	Symbol: FakeFrusionSlot
	
	*Cette classe �mule les actions d'un slot de frusion frutiparc pour faire tourner les jeux 
	avec le 'faux' serveur de la frusion.*	
*/
/*
	Symbol: FakeManager
	
	*Cette classe �mule les actions d'un manager de frusion frutiparc pour faire tourner les jeux 
	avec le 'faux' serveur de la frusion.*
*/
/*
	Symbol: FrusionManager
	
	*Cette classe est le point de d�part de la frusion c�t� serveur.*
	
	- Elle cr�� le slot de la frusion
	r�cup�re le gameDisc et une connexion vers l'instance unique su serveur <frusion.server.FrusionServer>.
*/ 
/*
	Package: Package frusion.client
	
	*Ce package comprend l'ensemble des classes et interfaces destin�es 
	� g�rer la communication avec les jeux*
*/
/*
	Symbol: FrusionClient
	
	*La classe FrusionClient s'occcupe de toutes les communications avec le serveur.*
	
	- En tant que LocalConnection, elle dialogue avec son homologue c�t� serveur: <FrusionServer>.	 
	- Elle envoie des signaux � <FrusionServer> et se charge de r�cup�rer tous les signaux en provenance de celui-ci, 
	qu'il s'agisse de documents XML ou d'�v�nements en provenance du serveur. 
*/ 
/*
	Symbol: FrusionLoader
	
	*Chargement des swf d'un jeu single player.*
	 
	- La classe FrusionLoader s'occcupe du chargement du swf du jeu, du swf de l'animation de la frusion
	ainsi que de la barre de chargement.  
*/ 
/*
	Symbol: MultiLoader

	*Chargement des swf d'un jeu multi player.*

	- Tout comme la classe <FrusionLoader>, la classe MultiLoader s'occcupe du chargement du swf du jeu, du swf de l'animation de la frusion
	ainsi que de la barre de chargement. 
	- En plus de ces fichiers elle se charge de charger l'interface 
	de cr�ation de parties multijoueurs: FrutiConnect (issu du projet �ponyme).
*/ 
/*
	Symbol: MultiManager
	
	*Interface de cr�ation de parties pour tous les jeux multi-joueurs.*
	
	- Cette interface propose un m�canisme commun � tous les jeux multijoueurs pour lesquels 
	la cr�ation de parties passe par l'interface de cr�ation de parties multijoueurs: FrutiConnect 
	(issu du projet �ponyme). 
	- Cette interface permet d'avoir un protocole de dialogue commun pour tous les Manager de jeux
	multijoueurs. Chaque jeu multijoueur qui a besoin de communiquer avec FrutiConnect devrait 
	avoir une classe manager impl�mentant MultiManager.   
*/
/*
	Package: Package frusion.gameclient
	
	*Ce package comprend un ensemble de classes pour g�r�r diff�rents type de jeux sans avoir � 
	utiliser des classes de bases telles que <FrusionClient> et des classes de services telles que <FrutiScore>.  
	Les classes de ce package permettent au d�veloppeur de rapidement incorporer les m�canismes de la frusion
	dans leur jeu.*
	   	
*/
/*
	Symbol: GameClient
	
	*Classe de connection jeu-FrusionClient-services* 
	
	- Cette classe que le joueur doit �tendre avec une classe de son cru, permet l'interfa�age automatique
	avec le service de jeu FrutiScore. Elle offre �galement la r�cup�ration de la FrutiCard en standard.
	
	 - *cf manuel ci dessus pour des exemples d'utilisation* 
*/
/*
	Symbol: RankingResult
	
	*Structure de stockage des r�sultats (classement) d'une partie. Ni plus ni moins une structure pour les r�sultats.*
	
	- Structure:
    > si (int) - subId du score
    > op (int) - ancienne position (vaut 0 si inexistante)
    > os (int) - ancien score
    > p (int) - meilleure position
    > s (int) - meilleur score
    > r (int) - id du ranking concern�
    > rn (String) - nom du ranking concern�	
*/
/*
	Package: Package frusion.gamedisc
	
	*Ce package contient les classes relatives au chargement 
	et au stockage des informations issues d'un disque de jeu de frutiparc.* 
*/	
/*
	Symbol: GameDisc
	
	*Structure de stockage des informations issus d'un GameDisc de Frutiparc.*
	
	- Structure:
	>discType : Number - type de disque	- 0=> noir, 1=> blanc
	>swfName : String		
	>swfId : String - nom court du jeu 
	>gameId : String - id du jeu tel qu'il est repr�sent� en base de donn�es
	>width : Number - largeur du jeu � charger
	>height : Number - hauteur du jeu � charger
	>mode : String - mode d'ouverture : i=>interne e=> externe
	>files : Object - Table de hachage contenant des structures de type GameFile
	>playMode : String - type de jeu : solo / multijoueurs 
	>size : Number - taille du swf du jeu	
*/
/*
	Symbol: GameFile
	
	*Structure de stockage des informations d'un fichier swf charg�*
	
	- Structure:
	>id : String - id du swf charg� au format MD5
	>size : Number - taille du fichier charg�	
*/
/*
	Symbol: GameDiscLoader
	
	*R�cup�ration par HTTP des informations d'un disque de jeu*
	
	- Le GameDiscLoader se charge de charger les informations du disque par requ�te HTTP au serveur 
	- il parse le xml r�el et cr�e le <GameDisc> correpsondant aux informations re�ues.	
*/
/*
	Package: Package frusion.gfx
	
	*Ce package contient les classes relatives aux �l�ments graphiques de chargement de jeu  
	de la frusion.* 
*/	
/*
	Symbol: FrusionAnim
	
	*La classe FrusionAnim se charge de charger l'animation du logo de la frusion*
*/
/*
	Symbol: LoadingBar
	
	*La classe LoadingBar affiche une barre de chargement � l'�cran pendant le chargement du jeu
	au d�marrage de la frusion*
*/
/*
	Package: Package frusion.server
	
	*Ce package contient les classes et interfaces utiles � la communication avec le serveur.* 
*/	
/*
	Symbol: FrusionServer
	
	*Communication avec le serveur XML. Pont avec le client <FrusionClient>*
	
	- Cette classe de type LocalConnection a pour responsabilit� de communiquer les informations en 
	provenance du serveur XML vers le client <frusion.client.FrusionClient> et de communiquer les requ�tes de ce dernier 
	vers le serveur XML.
	
	- elle s'occupe du processus d'identification de l'utilisateur
	- elle ouvre, maintient ou ferme le connexion vers le serveur XML 
	
	*Deux types d'informations sont v�hicul�es par FrusionServer vers le client:*
	- r�sultats XML
	- signaux d'�v�nements en provenance de Frutiparc 
		
*/
/*
	Symbol: XMLServer
	
	*classe de type XMLSocket qui transmets et re�oit des messages XML*
*/
/*
	Symbol: XMLCommand
	
	*Classe contenant deux m�thodes statiques pour cr�er facilement des messages XML avant envoi au serveur*
*/
/*
	Package: Package frusion.util
	
	*Ce package contient un ensemble de classes utilitaires.* 
*/
/*
	Symbol: AnimFileLoader
	
	*Une classe de type FileLoader pour charger l'animation de logo de la frusion
	et r�cup�rer les diff�rentes �tats de chargement*
*/
/*
	Symbol: FrutiConnectFileLoader
	
	*Une classe de type FileLoader pour charger l'interface de cr�ation de parties pour les jeux multijoueurs.*
*/
/*
	Symbol: GameFileLoader
	
	*Une classe de type FileLoader pour charger le jeu.*
*/
/*
	Package: Package frusion.service
	
	*Ce package contient un ensemble de classes de gestion des services.*
	
	Chaque classe de gestion de services poss�de un ensemble de m�canismes communs:
	- une r�f�rence vers l'objet FrusionClient utilis� par le jeu
	- une table de hachage qui contient un ensemble de paires nom_de_commande / code_commande
	pour envoyer des commandes server  
	- une table de hachage qui contient un ensemble de paires code_commande / nom_de_commande 
	pour dispatcher les signaux de r�ception vers les bonnes m�thodes de la classe de service.
*/
/*
	Symbol: FrutiCard
	
	*Gestion du service FrutiCard*
*/
/*
	Symbol: FrutiChat
	
	*Gestion du service FrutiChat*
*/
/*
	Symbol: FrutiGrapiz
	
	*Gestion du service FrutiGrapis relatif au jeu Grapiz*
*/
/*
	Symbol: FrutiScore
	
	*Gestion du service FrutiScore commun aux jeux � classement par score*	
*/
/*
	Symbol: ScoreParameter
	
	*Structure de stockage des param�tres de score sous la form paire cl�/valeur*
	
	- Structure:
	>subId : Number;
	>value : Number;
*/
