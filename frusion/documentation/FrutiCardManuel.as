/*
	
	Title: Utilisation de la FrutiCard
	
	*Ce manuel comprend l'ensemble de la documentation utile pour utiliser la FrutiCard, l'outil de sauvegarde des jeux.*
*/
/*
    Symbol: Introduction

    Chaque jeu peut impl�menter un m�canisme de sauvegarde de certaines donn�es, pr�f�rences, etc... Une fois sauvegard�es, ces donn�es sont
    facilement accessible et modfiables pour le jeu. La FrutiCard est le moyen de transf�rer et r�cup�rer ces donn�es. Elle se pr�sente sous
    la forme d'un service inclus de base dans la FrusionClient. 

    Par d�faut, au chargement du jeu, la FrusionClient s'occupe � votre place de r�cup�rer les informations de la FrutiCard et de les
    stocker pour vous. 
*/
/*
    Symbol: R�cup�ration de la FrutiCard

    L'utilisation de la FrutiCard est tr�s simple. D�j� pr�sente dans la FrusionClient, vous n'avez pas besoin de la r�impl�menter dans
    votre jeu. Ainsi en reprenant l'exemple de classe maClasseDeService de la section pr�c�dente (doc FrusionClient) dans lequel nous avions
    un membre priv� frusionClient :

	>	private var frusionClient : FrusionClient;

    Pour acc�der � la FrutiCard, il suffit juste d'utiliser le membre de FrusionClient comme indiqu� ci-dessous :

    >   // acceder � la FrutiCard
    >   this.frusionClient.frutiCard // appeler les m�thodes ou membres dont on a besoin....
    
*/
/*
    Symbol: R�cup�ration des donn�es de la FrutiCard

    Les donn�es de la FrutiCard sont automatiquement r�cup�r�es par le client FrusionClient une fois l'identification r�alis�es. Le
    m�canisme fonctionne comme une boite noire. Pour r�cup�rer les informations au d�marrage il suffit d'appeler la FrutiCard dans le
    callback de connection *onServiceConnect* ( pour rappel, plusieurs callbacks sont impl�ment�s par d�faut dans maClasseDeService,
    onServiceConnect est l'un deux).

    En reprenant notre exemple et en mettant � jour le code on obtient:
    >   // r�cup�ration des slots
	>       public function onServiceConnect() : Void 
	>	    {
	>		    // r�cup�re les donn�es
    >           var slots : Array = frusionClient.frutiCard.slots ;
	>	    }

    A noter, la FrutiCard offre *n* emplacements.
*/
/*
    Symbol: Sauvegarde des donn�es de la FrutiCard
    
    Pour sauvegarder des informations sur un slot, il suffit de donner le num�ro du slot et les donn�es � sauvegarder.
    Par exemple :
    
    >   // sauvegarde sur le serveur des infos sur le slot 0 (index=0) d'un objet g�n�rique
    >   var mesdonnees : Object;
    >   frusionClient.frutiCard.updateSlot( 0, mesdonnees ) ;

    A noter : 
    On n'attend pas de callback lors de la sauvegarde des slots.
*/
/*
    Symbol: Exemple complet de gestion de slots 

    Cet exemple est repris du jeu BurningKiwi. 

    >   // exemple d'initialisation d'un tableau de stockage des slots
    >   var SLOT_PREFS: Number = 0;
    >   var SLOT_PUBLIC: Number = 1;
    >   var frutiSlots : Array = new Array() ;
    >
    >   // on d�cide d'avoir deux slots par exemple...
    >   frutiSlots[SLOT_PREFS] = new Object() ;
    >   frutiSlots[SLOT_PUBLIC] = new Object() ;
    >
    >   // exemple de r�cup�ration des de tous les slots une fois connect�
    >   frutiSlots = frusionClient.frutiCard.slots ;
    >
    >   // exemple de sauvegarde des informations des pr�f�rences dans l'objet local
    >   // on notera que chacun est libre de choisir le format de son slot
    >   frutiSlots[SLOT_PREFS].mus = musicON ;
    >   frutiSlots[SLOT_PREFS].snd = soundsON ;
    >   frutiSlots[SLOT_PREFS].det = qualitySetting ;
    >   frutiSlots[SLOT_PREFS].bar = panelON ;
    >   frutiSlots[SLOT_PREFS].ctr = controls ;
    >
    >   // sauvegarde sur le serveur des infos sur le slot des pr�f�rences (index=0)
    >   frusionClient.frutiCard.updateSlot( SLOT_PREFS, frutiSlots[SLOT_PREFS] ) ;
    >
    >   // exemple de sauvegarde d'un second slot
    >   frutiSlots[SLOT_PUBLIC].ts = trackStats ;
    >   frusionClient.frutiCard.updateSlot( SLOT_PUBLIC, frutiSlots[SLOT_PUBLIC] ) ;
    >
*/
