/*
    Title: Mettre un nouveau jeu sur frutiparc

   	*Ce manuel comprend l'ensemble de la documentation utile pour configurer son environement, de fa�on � pouvoir utiliser le framework.*
*/    
/*
		Section: Installation du jeu gr�ce au module d'administration
		
		*A la fin de cette section, votre jeu sera install� et pr�t � fonctionner sur FP2 !*
*/
/*    
    Symbol: Etape 1 - Entrer les infos du jeu 
    
    Ci-suivent les instructions pour mettre en ligne un jeu sur frutiparc

    - Finir le jeu ;)
    - aller sur http://admin.frutiparc.com ( http://admin.beta.frutiparc.com/ sur un serveur de d�v )
    - se logger avec son identifiant frutiparc et non pas motion twin
    - aller dans la rubrique jeu et cliquer sur le lien jeux
    - cliquer sur le lien en bas � gauche : ajouter
    - entrer le nom complet du jeu dans le champ nom, par exemple, { Motion Ball 2 }
    - entrer le nom du disque dans le champ disc. Ce nom servira de clef plus tard donc il vaut mieux opter pour un nom court, par exemple, { mball } et s'assurer qu'il n'est pas d�j� utilis�
    - entrer les propri�t�s du jeu sous la forme w=n; h=n; m=l o� w repr�sente la largeur du jeu, h la hauteur et m le mode d'ouverture interne(m=i) ou externe (m=e), par exemple, { w=420;h=500;m=e }
    - Disques max : entrer le nombre de disques maximum du jeu courant qu'un joueur peut recevoir dans une journ�e ou cocher illimit� s'il ne doit pas y avoir de limite
    - entrer la description du jeu dans le champ Desc (m�me si cette description n'est affich�e nul part pour le moment, elle le sera surement � terme)
    - cliquer sur valider et sauter de joie si le message obtenu est le suivant : Jeu ajout� avec succ�s, yopla! (enfin sans le yopla) Sinon pleurer sa r... ou sa
      m...
*/
/*
    
    Symbol: Etape 2 - Ajouter le swf du jeu  
    
    Ci-suivent les instructions pour ajouter le swf du jeu sur frutiparc.

    PRECONDITIONS:
    - avoir compil� le swf de son jeu
    - �tre retourn� sur le menu principal de l'interface d'admin
    
    SUITE DES AVENTURES:
    - aller dans le menu jeu
    - cliquer sur {Fichiers de jeux disponibles (Ajout SWF)}
    - dans la belle page pleine de MD5 cliquer en bas � gauche sur... Ajouter, bravo ! (enfin lorsque le jeu vient d'�tre ajout�, la page est pas si pleine que �a...)
    - dans la page nouvellement ouverte : Ajouter un fichier de jeu
    - dans la combo jeu choisir... le jeu ! lala!
    - laisser le champ nom � index pour r�f�rencer le swf principal de votre jeu (celui qui est charg� par la frusion au d�part). Les autres noms corespondent � ceux qui seront demand�s par le jeu � la m�thode getFileInfos, n'importe quelle chaine de caract�re fonctionne (�viter les caract�res sp�ciaux quand m�me...)
    - mettre un commentaire si besoin qui permet d'identifier la version
    - dans le champ fichier choisir le fichier en cliquant sur le bouton choose � droite et chercher son gentil swf
    - ensuite cocher la jolie case: oui, j'ai prot�g� mon swf 
    - si on souhaite que ce swf soit directement utilis� comme fichier courant sur le jeu et pour tous les types de disques (noir, gris, blanc et rouge) cocher �galement "appliquer .... " (on pourra choisir en d�tail par la suite en cliquant sur "Courrants" dans la liste des jeux).
    
*/
/*
    Symbol: Etape 3 - G�rer les modes du jeu

    Un jeu peut disposer de plusieurs modes. Chacun des modes est accessible depuis certain type de disque uniquement (g�n�ralement le mode challenge est accessible depuis les diques noirs, gris et blanc, et les modes suppl�mentaires depuis un dique blanc uniquement).
    Ci-suivent les instructions pour d�finir les modes de jeu accessible depuis chaque type de disque.
    
    SUITE DES OPERATIONS:
    - retourner sur la liste des jeux de l'interface d'admin
    - cliquer sur Modes sur la ligne du jeu qu'on est en train d'ajouter
    - dans la belle page Modes de jeu de {mon jeu} cliquer sur Ajouter en bas � gauche
    - indiquer le num�ro du mode � autoriser (ce num�ro est utilis� dans votre jeu)
    - indiquer le type de disque pour lequel autoriser ce mode
    - valider et zoulpla-boum ce mode est maintenant autoris� !
    
    FIN DES OPERATIONS:
    - maintenant que les modes sont d�finis, on peut ajouter les gameScores
    
*/
/*
    Symbol: Etape 4 - G�rer les gameScore

    Un gameScore corespond � un "type de score" dans un mode d'un jeu, par exemple le "temps total obtenu pour la course Banana Derby en mode Arcade du jeu Burning Kiwi". On �tabli des classements sur les scores d'un m�me gameScore.
    
    SUITE DES OPERATIONS:
    - aller sur la liste des modes du jeu qui nous interesse (cf. etape 3)
    - cliquer sur le lien "gameScore associ�s" d'une ligne corespondant au mode pour lequel on veut ajouter un gameScore
    - une fois sur la page "GameScore du mode n�{mon mode} du jeu {mon jeu} cliquer sur le petit bouton Ajouter en bas � gauche (vous trouvez pas �a un peu lassant qu'il soit toujours au m�me endroit ?)
    - l'id secondaire est l'identifiant qui sera utilis� dans le jeu, il doit �tre unique pour ce couple jeu / mode.
    - le nom est un nom qui permet juste de s'y retrouver dans l'admin
    - le type est entre autre utilis� � l'affichage du score
    - les sp�cifications d�finissent les colonnes suppl�mentaires qui seront affich�es avec un classement de ce gameScore, lire les informations � propos du format en haut de la page
    - une fois que tout est remplit, on valide !
    
*/
/*
    Symbol: Etape 5 - G�rer les m�dailles

    Avant d'ajouter une r�gle de cr�ation de classement on va d�finir quelle sont les m�dailles qui seront disponibles pour ce jeu.
    Il ne peut y avoir qu'un seul proprietaire de chaque m�daille par jour (enfin un pour chacune des valeurs : or, argent et bronze) et donc un seul classement associ� chaque jour � cette m�daille.
    
    SUITE DES OPERATIONS:
    - on retourne au sommaire de l'admin
    - dans la cat�gorie Jeux, choisir m�daille
    - comme d'hab.. le petit "Ajouter" qui se balade en bas � gauche
    - choisir le titre de la m�daille (il sera surement affich� sur le site)
    - choisir le jeu
    - valider
    - et... ah bah non c'est d�j� fini !
    
*/
/*
    Symbol: Etape 6 - D�finir les r�gles de cr�ation de classements

    Le but de cette �tape est d'avoir tous les soirs � 23h un classement (ou plusieurs) cr�� pour notre jeu.
    Une r�gle de cr�ation de clasements contient trois grandes cat�gories :
    - Les conditions n�cessaire � ce qu'elle soit execut�e
    - La description du classement qui sera cr��
    - Les actions diverses qui seront execut�es si cette r�gle est execut�e
    
    REJOINDRE LA PAGE POUR AJOUTER UNE REGLE DE CREATION DE CLASSEMENT:
    - on retourne au sommaire de l'admin
    - on choisit Cr�ation automatique de classements dans la section Jeux
    - on clique sur le chtit ajouter en bas � gauche comme d'hab
    
    CONDITIONS D'EXECUTION:
		Les r�gles sont appliqu�es selon les dates de validit� (la date de validit� du classement � cr�er est utilis�e, et non pas la date d'execution du script de cr�ation, donc si on veut cr�er un classement � partir du 12 janvier, mettre que la r�gle est valide � partir du 12 janvier � 00h00).
		Elle sont appliqu�es chaque jour, selon les valeurs de l'identifiant de distribution et du coeficient de distribution : 
		  - *Une seule r�gle* pour un Id de distribution donn� 
      
      _Coef � 100 (ou plus)_ : C'est le cas classique, cette r�gle est simplement appliqu�e tous les jours 
      
      _Coef inf�rieur � 100_ : Cette r�gle est appliqu�e avec une probabilit� de taux_de_distribution / 100 
      
      - *Plusieurs r�gles* avec le m�me Id de distribution : 
      
    	Une et une seule des r�gles est appliqu�e chaque jour, en al�atoire. Le co�f permet de d�finir la probabilit� d'apparition des r�gles les une par rapport aux autres (chaque r�gle a donc une probabilite coef / somme de tous les coef d'�tre choisie) 
    
    DESCRIPTION DU CLASSEMENT A CREER:
    - le gameScore est le gameScore pour lequel ce classement est effectif
    - le nom est le nom du classement qui sera affich� aux joueurs
    - l'ordre indique si les premiers du classement sont ceux qui ont le plus gros (descendant) ou le plus petit score (ascendant)
    - le commentaire permet juste de s'y retrouver sur l'admin
    
    DONNES ASSOCIEES A CETTE REGLE:
    Si la r�gle est execut�e: 
    - le XML du jour qu'on d�finit ici sera appliqu� pour le JEU (jeu corespondant au gamescore d�fini) pour la journ�e de validit� du classement
    - � la fin de la journ�e de validit� du classement, la m�daille indiqu�e ici sera attribu�e en fonction des trois premiers au classement cr��
    
*/
/*
    Symbol: Etape 7 - Ajouter un jeu dans la boutique

    Bient�t...
    
*/
/*
		Section: Astuces pour l'utilisation de la frusion durant le d�veloppement
*/
/*
    Symbol: Astuce - Customizer un CD pour pouvoir utiliser son jeu

    Ci-suivent les instructions pour customizer un CD pour pouvoir tester son jeu.
    
    PRECONDITIONS:
    - aller sur beta.frutiparc.com
    - se mettre un CD sur le bureau pour le modifier ult�rieurement
    
    SUITE DES OPERATIONS:
    - se logger sur le serveur hq avec son login et pass motion twin
    - aller � l'adresse de mysql http://hq.motion-twin.com/mysql
    - une fois sur la page de garde choisir la base de donn�es frutiparc
    - choisir table ff_file
    - lancer la requ�te SQL suivante dans l'onglet SQL : SELECT * FROM `ff_file` WHERE user_id=10268 and type='disc' o� on changera le user
      id par le sien
    - dans les r�sultats (donc les disques) chercher l'enregistrement qui poss�de un parent_id = 0 (=sur le bureau)
    - modifier cet enregistrement
    - se positionner sur le champ desc et modifier les valeurs avec le type de disque � 0(noir), 1 (gris) ou 2(blanc) puis, � la ligne, le nom court du jeu (nom du disque pr�cedemment cit�e dans une aventure ant�rieure)
    
    FIN DES OPERATIONS:
    - tester le jeu sur beta.frutiparc.com avec le cd qui est sur le bureau et sauter de joie!
    
*/
