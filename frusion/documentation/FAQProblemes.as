/*
	
	Title: FAQ Problemes
	
	*Ce manuel comprend l'ensemble de la documentation utile pour r�soudre les diff�rents probl�mes rencontr�s
	en cours de d�veloppement.*

    Responsable du projet en cas de probl�mes, divergences d'opinions et cassage de t�te en dernier recours: Fran�ois Nicaise alias je-te-
    prends-�-Tobal2-quand-tu-veux-petit-scarab�e 
			
*/
/*
	Package: F.A.Q. Phase de D�veloppement
*/
/*
    Symbol: Question:

	*Quand je compile, je n'ai pas d'erreurs de compilation...*
*/
/*
    Symbol: R�ponse:

	*Ce probl�me peut facilement �tre r�solu en ajoutant quelques erreurs deci-dela. Mais en soi, ce n'est finalement un tr�s gros
    probl�me... Par contre le probl�me viendrait plut�t de ta petite t�te, qui aurait besoin d'un s�ance de TOBAL2 histoire de remettre les
    id�es en place...*
*/
/*
	Package: F.A.Q. Phase de tests

    *Ensemble des questions-r�ponses pour les probl�mes rencontr�s lors du lancement du jeu dans frutiparc*
    
    Le m�canisme de v�rification de la frusion au lancement est simple: 
        - mettre les diff�rentes fen�tres de debug sous forme de slot
        - ins�rer le CD dans la frusion
        - v�rifier l'apparition du slot frusion
        - v�rifier que l'anim frusion se joue
        - v�rifier que le jeu ou l'interface de gestion des jeux multijoueurs se charge
*/
/*
    Symbol: Question:

	*Quand j'ins�re le CD jeu dans la frusion, le slot s'ouvre mais rien ne se passe...*
*/
/*
    Symbol: R�ponse:

	*le fichier anim_frusion.swf n'est pas trouv� sur le serveur...*
    
    Il faut en recompiler une version :
        - R�cup�rer le fichier sur CVS frutiparc/frusion/anim_frusion.fla
*/
/*
    Symbol: Question:

	*Une fois l'anim frusion enti�rement charg�e ou quand je clique sur l'anim frusion en fin de chargement, rien ne se passe...*
*/
/*
    Symbol: R�ponse:
    
    *le fichier du jeu n'est pas charg�*

    Plusieurs options :
        - v�rifier l'existence du fichier sur le serveur (avec le nom MD5)
        - v�rifier que le nom fichier ne se termine pas par le type .swf (8296c090356600f6537ce03aa86329a9.swf au lieu de
          8296c090356600f6537ce03aa86329a9 par exemple) 
        - v�rifier l'existence d'un fichier 'index' dans l'admin des jeux de frutiparc (http://admin.beta.frutiparc.com)  
*/
/*
    Symbol: Question:

    *Le port du jeu est introuvable...*
*/
/*
    Symbol: R�ponse:

    Chaque jeu utilise un port qui lui est attribu� suivant ses besoins. Le port du jeu est stock� dans un fichier xml � la base du serveur
    web dans le dossier www/xml sous le nom services.xml. les informations sont stock�es sous la forme d'une node :'  <service name="monjeu"
    port="2001"/>'. Or le jeu ne se chargera pas si le port n'est pas d�fini ou mal entr� dans ce fichier.

    Aussi plusieurs options :
        - v�rifier que la ligne avec le nom de votre jeu existe bien, si ce n'est pas le cas en ajouter une qui contient le nom court du jeu
          et le num�ro du port requis (demander � l'administrateur le num�ro du port si vous ne le connaissez pas)
        - v�rifier le nom court utilis� pour le jeu (attribut name dans la node). Ce dernier doit �tre le m�me que dans l'admin des jeux de frutiparc (http://admin.beta.frutiparc.com) 
        - v�rifier le num�ro de port
    Par ailleurs, chaque jeu utilise un port qui lui est attribu� suivant ses besoins. Le port du jeu est stock� dans un fichier xml � la base du serveur
    web dans le dossier www/xml sous le nom services.xml. les informations sont stock�es sous la forme d'une node :'  <service name="monjeu"
    port="2001"/>'. Or le jeu ne se chargera pas si le port n'est pas d�fini ou mal entr� dans ce fichier.

    Aussi plusieurs options :
        - v�rifier que la ligne avec le nom de votre jeu existe bien, si ce n'est pas le cas en ajouter une qui contient le nom court du jeu
          et le num�ro du port requis (demander � l'administrateur le num�ro du port si vous ne le connaissez pas)
        - v�rifier le nom court utilis� pour le jeu (attribut name dans la node). Ce dernier doit �tre le m�me que dans l'admin des jeux de frutiparc (http://admin.beta.frutiparc.com) 
        - v�rifier le num�ro de port

    Si tout est correct, il se peut que le port ne soit pas ouvert sur le serveur, auquel cas il suffit de demander � l'administrateur de
    l'ouvrir et de faire de nouveaux tests.

    Si les probl�mes persistent, pensez � rev�rifier votre code :
        - assurez vous que vous compilez votre jeu avec les derni�res versions des librairies si vous ne travaillez pas avec les headers
        
    En dernier recours, contactez-moi en criant tr�s fort � l'aide et en me suppliant aussi (les ch�ques ne sont pas accept�s. merci)
        

    Si tout est correct, il se peut que le port ne soit pas ouvert sur le serveur, auquel cas il suffit de demander � l'administrateur de
    l'ouvrir et de faire de nouveaux tests.

    Si les probl�mes persistent, pensez � rev�rifier votre code :
        - assurez vous que vous compilez votre jeu avec les derni�res versions des librairies si vous ne travaillez pas avec les headers
        
    En dernier recours, contactez-moi en criant tr�s fort � l'aide et en me suppliant aussi (les ch�ques ne sont pas accept�s. merci)
            
*/
/*
    Symbol: Question:

	*Quand je n'ins�re pas le CD jeu dans la frusion rien ne se passe...*
*/
/*
    Symbol: R�ponse:

	*Viens me voir � la sortie, je vais t'expliquer un peu la vie...*
*/

