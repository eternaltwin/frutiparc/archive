/*
	
	Title: Doc GameClient
	
	*Ce manuel comprend l'ensemble de la documentation utile pour utiliser la classe GameClient 
	dans un projet de jeu ayant besoin des services FrutiScore et FrutiCard*
	
	Doc en cours. Voir http://hq.motion-twin.com/wiki/index.php/GamaTutra pour plus d'infos
		
*/
/*	
	Section: Description du syst�me...
	
	*Le framework frusion propose la classe <frusion.gameclient.GameClient> qui permet de s'interfacer
	� la frusion de fa�on transparente:*
		
	- g�rer les communications client-serveur entre le jeu et frutiparc de fa�on transparente 
	- g�re les envois et r�ceptions de requ�tes vers les services FrutiCard et FrutiScore
	- g�re la connexion de FrusionClient et les envois et r�ceptions d'informations au serveur
*/
/*
	Package: Etape 1 - Cr�ation d'une sous-classe 
*/
/*
	Symbol: Mod�le de classe
	
	*Mod�le de classe*
	
	- Ci-dessous, un mod�le de client standard est propos� et peut servir de base de travail:
	
>class MonCLient extends frusion.gameclient.GameClient 
>{ 
>
>	private var root : MovieClip ; 
>
>	//------------------------------------------------------------------------ 
>	CONSTRUCTEUR 
>	//------------------------------------------------------------------------ 
>	public function MonClient( context : MovieClip ) { 
>		super() ; 
>		this.root = context ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	MODIFIE LE CONTEXTE DE L'OBJET 
>	//------------------------------------------------------------------------ 
>	public function changeRoot( newRoot : MovieClip ) : Void { 
>		this.root = newRoot ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	INITIALISATION ET CONNEXION AU SERVICE 
>	//------------------------------------------------------------------------ 
>	public function serviceConnect() : Void { 
>		super.serviceConnect() ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	MODES DE JEU 
>	//------------------------------------------------------------------------ 
>	public function listModes() : Void { 
>		super.listModes() ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	D�MARRE UNE PARTIE 
>	//------------------------------------------------------------------------ 
>	public function startGame( gameMode : Number ) : Void { 
>		super.startGame( gameMode ) ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	TERMINE UNE PARTIE 
>	//------------------------------------------------------------------------ 
>	public function endGame() : Void { 
>		super.endGame() ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	CALLBACK: SERVICE CONNECT 
>	//------------------------------------------------------------------------ 
>	public function onServiceConnect() : Void { 
>		super.onServiceConnect() ; 
>
>		root.vs.discType = this.gameDisc.discType ; 
>		root.frutiSlots = this.slots ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	CALLBACK: LISTMODES 
>	//------------------------------------------------------------------------ 
>	public function onListModes( nodeTxt : String ) : Void { 
>		super.onListModes( nodeTxt ) ; 
>		root.gameModes = this.gameModes ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	CALLBACK: GAME CLOSE 
>	//------------------------------------------------------------------------ 
>	public function onGameClose() : Void { 
>		super.onGameClose() ; 
>		if ( this.gameRunning ) 
>			this.endGame() ; 
>		else 
>			this.closeService() ; 
>	} 
>
>	//------------------------------------------------------------------------ 
>	CALLBACK: ENDGAME 
>	//------------------------------------------------------------------------ 
>	public function onEndGame( nodeTxt : String ) : Void { 
>		super.onEndGame(nodeTxt) ; 
>		if ( this.forceClose ) 
>			this.closeService() ; 
>	} 
>} // FIN DECLARATION CLASSE
*/
