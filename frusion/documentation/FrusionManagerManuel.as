/*
	
	Title: Doc FrusionManager
	
	*Ce manuel comprend l'ensemble de la documentation utile pour comprendre le fonctinnement
	de la classe <frusion.FrusionManager> qui est le point d'entr�e v�ritable du syst�me de la
	frusion.*									
*/
/*	
	Section: Description du syst�me...
	
	*La classe FrusionManager est une des pierres d'angle du syst�me.*
	
	*Le diagrammme de s�quences ci-dessous montre les diff�rentes �tapes du syst�me:* 
	<image:../../images/FrusionManagerSequenceDiagram.png>.	
*/
/*
	Symbol: Etape 1 - R�cup�ration des informations du disque
	
	La premi�re �tape consiste � r�cup�rer les informations du disque gr�ce 
	� l'identifiant que frutiparc transmet au FrusionManager. 
	Pour ce faire:
	- le FrusionManager utilise le GameDiscLoader, classe destin�e � la seule fin de 
	r�cup�rer les informations par requ�te HTTP.
	- une fois les donn�es r�cup�r�es, le GameDiscloader parse le xml r�sultat pour cr�er un nouvel 
	objet de type GameDisc qu'il renvoie au FrusionManager
*/
/*
	Symbol: Etape 2 - D�marrage du syst�me graphique
	
	- Le GameDisc r�cup�r�, le FrusionManager demande � la Frusion de d�marrer. Graphiquement on voit 
	le disque qui commence � tourner.
	- Ensuite, le FrusionManager demande la cr�ation d'un slot � Frutiparc.  	
*/
/*
	Symbol: Etape 3 - Intialisation du serveur Frusion
	
	- La derni�re �tape consiste � r�cup�rer une instance existante du serveur.
	- Une fois l'instance r�cup�r�e elle est initialis�e   	
*/