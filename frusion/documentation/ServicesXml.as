/*
	
	Title: Configuration des ports
	
	*Ce manuel comprend l'ensemble de la documentation utile pour allouer un port � un jeu.*
*/
/*
    Symbol: Introduction

    Certains ports du serveur sont reli�s � des services de CBee++. Chaque jeu qui communique avec CBee++ doit utiliser un de ces ports. 
    Pour savoir lequel le jeu doit utiliser, un fichier xml, services.xml dans le r�pertoire www/xml stocke ces derniers. sous la forme
    suivante:

    ><?xml version="1.0"?>
    ><services>
    >   <service name="frutichat"   port="2000"/>
    >   <service name="frutiscore"  port="2001"/>
    >   <service name="grapiz"      port="2010"/>
    >   <service name="jamajama"    port="2008"/>
    >   <service name="bandas"      port="2011"/>
    >   <service name="bkiwi"      port="2001"/>
    ></services>
*/
/*
    Symbol: description d'une node

    Une node est compos�e de deux attributs.

    L'attribut 'name' stocke le nom cour du jeu, le m�me que celui qui est entr� dans l'interface d'admin de frutiparc. C'est notre cl� pour
    r�cup�rer le port. Pour rappel, quand on ajoute un jeu � partir de l'interface d'administration, on donne le nom 'normal' du jeu ainsi
    que son nom long.

    L'attribut port sert � stocker le num�ro de port dont le jeu � besoin. La liste des ports est disponible aupr�s de l'administrateur.
*/
/*
    Symbol: utilisation

    Il suffit d'ajouter une ligne dans le fichier pour que le port soit automatiquement assign� au jeu dans la frusion. Suivre le mod�le
    suivant: 
    >   <service name="nom_court"      port="numero_port"/>
*/
