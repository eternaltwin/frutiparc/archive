/*
	
	Title: Environnement de d�veloppement
	
	*Ce manuel comprend l'ensemble de la documentation utile pour configurer son environement, de fa�on � pouvoir utiliser le framework.*
*/
/*
	Package: Pr�-requis
*/
/*
    Symbol: Environnement complet:

	*Par environnement complet on comprend l'ensemble des fichiers du framework et des librairies globales pour compiler le projet.*

    Utilisation :
        - r�cup�rer sur le serveur CVS le dossier frutiparc sur le disque local
        - r�cup�rer sur le serveur CVS le dossier common_ext sur le disque local
        - r�cup�rer sur le serveur CVS le dossier frutiengine sur le disque local
        - une fois r�cup�r�s, mettre � jour les classpaths du projet pour les inclure � la compilation
    
    *>C'est l'environnement privil�gi� pour int�grer le framework et tester en profondeur son projet.*
    
*/
/*
    Symbol: Environnement restreint:

	*Par environnement restreint on comprend le minimum de fichiers n�cessaires pour pouvoir travailler.*

    Etant donn� que la frusion et frutiparc int�grent d�j� toutes les classes dont on a besoin, il suffit de r�cup�rer les en-t�tes ou headers des classes
    du framework pour pouvoir compiler. Attention, toutefois, l'environnement restreint n'est pas conseill� pour un d�marrage d'utilisation
    du framework, l'impl�mentation des m�thodes des diff�rentes classes du framework n'�tant pas fournies. 
    
    Son principal b�n�fice est alors d'offrir des temps de compilation tr�s int�ressants et ne pas se soucier des modifications
    dans l'impl�mentation des m�thodes de classe.

    Utilisation :
        - r�cup�rer sur le serveur CVS le dossier frutiparc/headers sur le disque local
        - une fois r�cup�r�, mettre � jour le classpath du projet pour l'inclure � la compilation

    *>C'est l'environnement privil�gi� en fin de projet, quand tout a �t� test� et valid� au niveau de l'int�gration. *

*/

