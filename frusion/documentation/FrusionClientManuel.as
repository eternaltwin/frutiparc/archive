/*
	
	Title: Doc FrusionClient
	
	*Ce manuel comprend l'ensemble de la documentation utile pour utiliser la classe FrusionClient
	dans tout projet de jeu.*
			
*/
/*	
	Section: Description du syst�me...
	
	*Le framework frusion propose la classe <frusion.client.FrusionClient> qui permet de g�rer les 
	communication client-serveur*
	
	Cette classe peut �tre utilis�e par tout jeu. A noter:		
	- si votre jeu utilise FrutiScore / FrutiCard, utilisez plut�t les m�canismes li�s � la 
	classe <frusion.gameclient.GameClient> 	
	- En terme d'utilisation pour un nouveau service, il est recommand� de cr�er une classe sur le
	m�me mod�le que les classes de services d�j� pr�sentes dans le package frusion.service.
	
	*Le diagrammme de s�quences ci-dessous montre les diff�rentes �tapes du syst�me:* 
	<image:../../images/FrusionClientSequenceDiagram.png>.	
	
*/
/*
	Package: Etape 1 - Initialisation du client Frusion 
*/
/*
	Symbol: Cr�ation d'un nouveau client
	
	*La d�marche � suivre pour cr�er un nouveau client est tr�s simple :*
    >frusionClient = new frusion.client.FrusionClient() ;
*/
/*

    Symbol: Enregistrement des Callbacks principaux
    
    *Il existe 4 callbacks principaux � enregistrer :*
    - le callback d'�tat *READY* qui est appel� quand la connexion au serveur est �tablie avec succ�s
    et que l'utilisateur est identifi�  
    - le callback d'�v�nement *PAUSE* qui est appel� quand Frutiparc met le 
    jeu en pause (perte de focus de slot par exemple) 
    - le callback d'�v�nement *CLOSE* qui est appel� quand Frutiparc d�cide de fermer le 
    jeu (appel� � la fermeture de slot par exemple) 
    - le callback d'�v�nement *RESET* qui est appel� quand la Frusion de Frutiparc est remise � z�ro
    
    *Code :*
    >frusionClient.registerReadyCallback( new util.Callback(this,"onServiceConnect") ) ;
    >frusionClient.registerPauseCallback( new util.Callback(this,"onFrutiparcPause")) ;
    >frusionClient.registerCloseCallback( new util.Callback(this,"onFrutiparcClose")) ;
    >frusionClient.registerResetCallback( new util.Callback(this,"onFrutiparcReset")) ;
*/    
/*

    Symbol: Connection au service correspondant

	*Une fois les princpaux callbacks enregistr�s, il suffit de se connecter aux ports du service demand� :*
    >frusionClient.getService( scoreServicePort ); // avec scoreServicePort = 2001 par exemple 
*/
/*
	Package: Etape 2 - Enregistrement des commandes et callbacks correspondants  
*/
/*
	Symbol: Cr�ation de la liste des commandes

	On enregistre la liste des commandes � passer au serveur dans un objet de type Object
	en utilisant un m�canisme paire nom_de_commande / code_de_commande.
	
	Cet objet, appel� commandList dans l'exemple ci-dessous, va permettre l'utilisation des noms
	entiers des commandes plut�t que les code, ceci � des fins de lisibilit� et facilit� de 
	compr�hension du code :  
	
	>	// exemple de commandes
	>	commandList : = new Object();
	>	commandList.maCommandeA 		= "mca";
	>	commandList.maCommandeB 		= "mcb";
*/
/*
	Symbol: Cr�ation de la liste des renvois 

	On enregistre la liste des renvois ou callbacks que notre frusionClient va r�cup�rer
	du server dans un objet de type Object
	en utilisant un m�canisme paire renvoi_de_commande / code_de_commande.

	*Code :*         File: __a_frusionclient  (no auto-title, documentation\FrusionClientManuel.as)

	>	// exemple de renvois de commandes
	>	callbackList = new Object();
	>	callbackList.onMaCommandeA 		= "mca";
	>	callbackList.onMaCommandeB		= "mcb";
	
	Une fois cette liste cr�e, elle est pass�e au frusionClient, qui v�rifiera � chaque r�ception 
	d'informations en provenance du serveur si celles-ci sont � nous renvoyer.
	
	*Code :*
	>	this.frusionClient.registerCallbackList( callbackList );	 
*/
/*
	Symbol: Cr�ation et envoi d'une commande avec param�tres
	
	- Pour envoyer des commandes, il suffit de d'utiliser la commande 
	> sendCommand( nomDeCommande, parametres )
	
	*exemple d'une commande avec deux param�tres:*
>	function maCommandeA( paramA : Object, paramB : Number ) : Void
>	{
>		this.frusionClient.sendCommand(
>				this.commandList.maCommandeA,
>				new Array(
>					new CommandParameter( "paramA", paramA ),
>					new CommandParameter( "paramB", paramB ) ) );
>	}

	- A noter, dans l'exemple pr�c�dent on se sert de 
	> this.commandList.maCommandeA
	au lieu de passer "mca", qui est le vrai code de commande.
	
	- L'objet <frusion.server.CommandParameter> est un container de paire cl�/valeurs utile
	pour formatter facilement un param�tre � envoyer. 
 
 	- Une fois la commande envoy�e, la r�ponse arrivera par le biais de la fonction
 	de renvoi qui poss�de la m�me code de commande "mca" soit: 
 	>onMaCommandeA 
 	comme il a �t� sp�cifi� dans l'objet callbacklist.
*/
/*
	Symbol: Cr�ation et envoi d'une commande sans param�tres
	
	- Pour envoyer une commande sans param�tres, il suffit de d'utiliser la commande 
	> sendCommand( nomDeCommande, parametres ) avec un tableau de param�tres vide.
	
	*exemple d'une commande sans param�tres:*
>	function maCommandeB() : Void
>	{
>		this.frusionClient.sendCommand(
>				this.commandList.maCommandeB,
>				new Array() );
>	}

	- A noter, dans l'exemple pr�c�dent on se sert de 
	> this.commandList.maCommandeB
	au lieu de passer "mcb", qui est le vrai code de commande.
	 
 	- Une fois la commande envoy�e, la r�ponse arrivera par le biais de la fonction
 	de renvoi qui poss�de la m�me code de commande "mcb" soit: 
 	>onMaCommandeB 
 	comme il a �t� sp�cifi� dans l'objet callbacklist.
*/
/*
	Symbol: Cr�ation et envoi d'une commande avec param�tres et texte
	
	- Dans certains cas il est n�cessaire d'envoyer une commande avec des param�tres 
	et une node xml contenant des informations. Il suffit pour cela de d'utiliser la commande 
	> sendCommandWithText( nomDeCommande, parametres, texte ).
	
	*exemple d'une commande texte avec param�tres:*
	>	function maCommandeC( paramA : Object, text : String ) : Void
	>	{
	>		this.frusionClient.sendCommandWithText(
	>				this.commandList.maCommandeC,
	>				new Array(
	>					new CommandParameter( "paramA", paramA ) ),
	>			  text);
	>   )

	- A noter, dans l'exemple pr�c�dent on se sert de 
	> this.commandList.maCommandeC
	au lieu de passer "mcc", qui est le vrai code de commande.
	 
 	- Une fois la commande envoy�e, la r�ponse arrivera par le biais de la fonction
 	de renvoi qui poss�de la m�me code de commande "mcc" soit: 
 	>onMaCommandeC 
 	comme il a �t� sp�cifi� dans l'objet callbacklist.
*/
/*
	Symbol: Cr�ation et envoi d'une commande sans param�tres et texte
	
	- Dans certains cas il est n�cessaire d'envoyer une commande sans param�tres 
	et une node xml contenant des informations. Il suffit pour cela de d'utiliser la commande 
	> sendCommandWithText( nomDeCommande, parametres, texte ).
	
	*exemple d'une commande texte sans param�tres:*
	>	function maCommandeD( texte : String ) : Void
	>	{
	>		this.frusionClient.sendCommandWithText(
	>				this.commandList.maCommandeD,
	>				null,
	>			  texte);
	>   )

	- A noter, dans l'exemple pr�c�dent on se sert de 
	> this.commandList.maCommandeD
	au lieu de passer "mcd", qui est le vrai code de commande.
	 
 	- Une fois la commande envoy�e, la r�ponse arrivera par le biais de la fonction
 	de renvoi qui poss�de la m�me code de commande "mcd" soit: 
 	>onMaCommandeD 
 	comme il a �t� sp�cifi� dans l'objet callbacklist.
*/
/*
	Symbol: Renvoi / callback de commandes
	
	- Pour r�cup�rer le r�sultat d'une commande, il suffit de cr�er une fonction ayant le m�me
	nom que celui pr�cis� and la callbackList. 
	
	En reprenant l'exemple pr�c�dent:
	>function onMaCommandeA( node : String ) : Void
	>{
	>	// (R�cup�re le xml r�sultat et fait des choses... ;)
	>}
	
	 - le xml est r�cup�r� sous forme de String � modifier en XMLNode et � parser suivant les besoins...
*/
/*
	Package: Exemple complet
	
	*Reprise de l'exemple utilis� ci-dessus:*
	
	>import frusion.client.FrusionClient ;
	>import frusion.service.ScoreParameter ;
	>import frusion.server.CommandParameter ;
	>import util.Callback;
	>
	>
	>class maClasseDeService
	>{
	>	private var frusionClient : FrusionClient;
	>	private var callbackList : Object;
	>	private var commandList : Object;
	>
	>
	>	public function maClasseDeService()
	>	{
    >		this.frusionClient = new frusion.client.FrusionClient() ;
	>		this.frusionClient.addListener(this);
	>
	>		this.frusionClient.registerReadyCallback( new Callback(this,"onServiceConnect") ) ;
    >		this.frusionClient.registerPauseCallback( new Callback(this,"onFrutiparcPause")) ;
    >		this.frusionClient.registerCloseCallback( new Callback(this,"onFrutiparcClose")) ;
    >		this.frusionClient.registerResetCallback( new Callback(this,"onFrutiparcReset")) ;
	>
	>		// creating appropriate commands
	>		this.commandList = new Object();
	>		commandList.maCommandeA 		= "mca";
	>		commandList.maCommandeB 		= "mcb";
	>		commandList.maCommandeC 		= "mcc";
	>		commandList.maCommandeD 		= "mcd";
	>
	>		// Registering callback list
	>		this.callbackList = new Object();
	>		callbackList.onMaCommandeA 		= "mca";
	>		callbackList.onMaCommandeB		= "mcb";
	>		callbackList.onMaCommandeC		= "mcc";
	>		callbackList.onMaCommandeD		= "mcd";
	>		this.frusionClient.registerCallbackList( callbackList );
	>
	>	}
	>
	>		
	>	public function maCommandeA( paramA : Object, paramB : Number ) : Void
	>	{
	>		this.frusionClient.sendCommand(
	>				this.commandList.maCommandeA,
	>				new Array(
	>					new CommandParameter( "paramA", paramA ),
	>					new CommandParameter( "paramB", paramB ) ) );
	>	}
	>
	>
	>	public function maCommandeB() : Void
	>	{
	>		this.frusionClient.sendCommand(
	>				this.commandList.maCommandeB,
	>				new Array() );
	>	}
	>
	>
	>	public function maCommandeC( paramA : Object, texte : String ) : Void
	>	{
	>		this.frusionClient.sendCommandWithText(
	>				this.commandList.maCommandeC,
	>				new Array(
	>					new CommandParameter( "paramA", paramA ) ),
	>			  texte);
	>   )
	>
	>
	>	public function maCommandeD( texte: String ) : Void
	>	{
	>		this.frusionClient.sendCommandWithText(
	>				this.commandList.maCommandeD,
	>				null,
	>			  	texte );
	>	}
	>
	>
	>	public function onMaCommandeA( node : String ) : Void
	>	{
	>		// (R�cup�re le xml r�sultat et fait des choses... ;)
	>	}
	>
	>
	>	public function onMaCommandeB( node : String ) : Void
	>	{
	>		// (R�cup�re le xml r�sultat et fait des choses... ;)
	>	}
	>
	>
	>	public function onMaCommandeC( node : String ) : Void
	>	{
	>		// (R�cup�re le xml r�sultat et fait des choses... ;)
	>	}
	>
	>
	>	public function onMaCommandeD( node : String ) : Void
	>	{
	>		// (R�cup�re le xml r�sultat et fait des choses... ;)
	>	}
	>
	>
	>   public function onServiceConnect() : Void 
	>	{
	>		// do things...
	>	}
	>
	>
	>   public function onFrutiparcPause() : Void 
	>	{
	>		// do things...
	>	}
	>
	>
	>   public function onFrutiparcClose() : Void 
	>	{
	>		// do things...
	>	}
	>
	>
	>   public function onFrutiparcReset() : Void 
	>	{
	>		// do things...
	>	}
	>
	>}
*/
