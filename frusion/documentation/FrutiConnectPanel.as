/*
	
	Title: Cr�ation des panels de jeu
	
	*Ce manuel comprend l'ensemble de la documentation utile pour cr�er un panel de cr�ation de partie dans l'interface FrutiConnect.*
*/
/*
    Symbol: Introduction

    Chaque jeu qui utilise l'interface FrutiConnect doit offrir son propre panel de cr�ation de partie. Pour ce faire, il suffit de placer
    un fichier xml contenant les informations du panel dans le r�pertoire www/xml. En g�n�ral, un fichier de panel se pr�sente sous la forme
    suivante (exemple tir� du fichier de panel de FrutiBandas):

    ><!-- d�but description panel-->
    ><p>  
    >   <!-- hauteur de ligne-->
    >   <l h="40" />  
    >
    >   <!-- combo box de s�lection de nombre de joueurs-->
    >   <l>  
    >       <t s="1" w="200">Joueurs maximum</t>
    >       <c w="80" v="nbrPlayers" d="1">2;3;4</c>
    >   </l>  
    >
    >   <!-- combo box de s�lection de temps-->
    >   <l>  
    >       <t s="1" w="200">Temps</t>
    >       <c w="80" v="time" d="1">10:00;8:00;5:00;3:00</c>
    >   </l>       
    >
    >   <!-- combo box de s�lection de taille-->
    >   <l>
    >       <t s="1" w="200">Taille plateau</t>
	>       <c w="80" v="boardSize" d="1">6;7;8</c>
    >   </l>
    ></p>
    ><!-- fin description panel-->
    
*/
/*
    Symbol: Cr�ation de fichier de panel

    Les fichiers de panels sont automatiquement charg�s dans l'interface FrutiConnect par la frusion. Pour ce faire, ils utilisent la
    convention de nommage suivante: 
    
    >nom_court_du_jeu.panel.xml.

    Ainsi le jeu multi-joueurs FrutiBandas dont le nom court est bandas aura un fichier xml stock� dans le r�pertoire www/xml nomm� 
    
    >bandas.panel.xml
*/
/*
    Symbol: Cr�ation du panel

    Une fois le fichier cr��, il s'agit de mettre les informations n�cessaires � la cr�ation de partie. Le fichier permet la description des
    champs textes, �tiquettes et combo-box utiles. Le panel est compos� de lignes, chaque ligne poss�de plusieurs attributs, les composants
    pr�c�demment cit�s sont positionn�s dans une ligne.

    Nous allons prendre l'exemple suivant : *cr�ation d'un panel comprenant une combo-box de s�lection de temps*.

    
    Tout d'abord cr�er la trame du panel:
    ><p>
    ></p>

    Ensuite cr�er une nouvelle ligne:
    ><p>
    >   <l>
    >   </l>
    ></p>

    Puis cr�er une �tiquette avec le titre 'temps' de 200 pixels de largeur dans la ligne pr�cedemment cr��e:
    ><p>
    >   <l>
    >       <t s="1" w="200">Temps</t>
    >   </l>
    ></p>
    

    Enfin cr�er la combo-box de 80 pixels avec diff�rents temps avec le premier �l�ment s�lectionn� par d�faut:
    ><p>
    >   <l>
    >       <t s="1" w="200">Temps</t>
    >       <c w="80" v="time" d="1">10:00;8:00;5:00;3:00</c>
    >   </l>
    ></p>
    

    A noter, l'attribut v est utilis� pour communiquer avec une variable dans le manager du jeu multi-joueurs. Son utilisation est d�crite dans
    le manuel correspondant � l'utilisation du framework frusion avec un jeu multi-joueurs.
*/
/*
    Symbol: D�finition des attributs xml

    Ci-suit la d�finition de certains atrributs utilisables dans le fichier xml :
    
    Conteneurs:
    >"l" = ligne 
	>"p" = panel 

    composants:
	>"t" = textField (=�tiquette)
	>"i" = inputField
	>"c" = link
    
	attributs:
	>"w" = width
	>"b" = big
	>"s" = spacer
   	>"h" = height

    Pour une liste plus exhaustive consulter la documentation du fichier *cp.Document* dans frutiparc.
 
*/
