/*
	
	Title: Doc XMLServer
	
	*Ce manuel comprend l'ensemble de la documentation utile pour comprendre le fonctionnement
	de la classe <frusion.server.XMLServer>, la classe qui g�re les envois et 
	r�ceptions de messages XML.*					
*/
/*	
	Section: Description du syst�me...
	
	*La classe XMLServer est responsables des �changes xml avec CBee.*
	
	De conception et d'utilisation tr�s simple elle ne sert qu'� transmettre et recevoir 
	du contenu XML sans se soucier de la mani�re dont ce dernier est format�, etc...
	
	*Le diagrammme ci-dessous montre le fonctionnement du syst�me:* 
	<image:../../images/XMLServerSequenceDiagram.png>.	
*/
/*
	Symbol: Syst�me push pour les envois clients
	
	*Le syst�me utilise une technologie push.*
	
	L'utilisation de ASBroadcaster permet � tous les clients de XMLServer 
	(<frusion.server.FrusionServer> au sein du framework) d'�couter le serveur XML.
	
	Celui-ci envoie les messages XML d�s r�ception sans regarder leur contenu. 
	Les clients doivent donc les transmettre aux bonnes m�thodes.
*/
