# Frutiparc Archive

This project contains an old Frutiparc archive provided for Eternaltwin.

Any project based on the code and assets of MT games must be Open Source and
Non-Commercial: use the license `AGPLv3`.
