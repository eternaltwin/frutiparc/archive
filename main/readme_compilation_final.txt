Pour compiler un main.swf correct :
'tention, faut tout lire et surtout ne rien oublier...

DANS LE FLA :
* Prendre le dernier dev.fla et l'enregistrer sous final.fla

* En frame 2 (root) il faut : #include "../init_final.as" (à la place de #include "classLoader.as")

* Dans la librarie, modifier l'url (dans le panel linkage) de /_icones/_fileIcon/iconGFX pour : http://swf.frutiparc.com/fileIcon.swf

* Ajouter les classpath vers :
	../
	../interface/
	../../frutiengine/
	../../frutiengine/frusion_internal/
	../../common_ext/class/

SUR LES FICHIERS ACTIONSCRIPT
* Modifier Path.as pour que le domaine swf soit le bon ( http://swf.frutiparc.com/ et pas http://www.beta.frutiparc.com/swf/ )
	
* Modifier frusion/Context.as de manière à avoir :
	public static var BASE_DOMAIN : String = "swf.frutiparc.com";
	public static var SENDING_DOMAIN : String = "swf.frutiparc.com";

Enfin, modifier listener/main.as :
* TRES IMPORTANT : mettre en commentaire la methode onIpDebug (des mots de passe d'admin sont dedans...)

* décommenter le addListener ver onIp, commenter celui vers onIpDebug, ce qui doit donner une fois la modif faite :
	_global.mainCnx.addListener("ip",listener.main,"onIp");
	//_global.mainCnx.addListener("ip",listener.main,"onIpDebug");


Une fois ces opérations effectuées... vient le temps de la compilation...................
Si ça a compilé sans erreur et qu'on est sur et certain de notre coup, on peut mettre en ligne.
Ne pas oublier de vérifier que ça fonctione ^^